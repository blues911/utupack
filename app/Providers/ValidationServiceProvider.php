<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Order;
use App\Models\Sample;
use App\Models\UserConsignee;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Правила валидации
        Validator::extend('user_is_active', function ($attribute, $value, $parameters, $validator)
        {
            $user = User::where(['login' => $value, 'status' => 1])->first();
            
            return ($user) ? true : false;
        });

        Validator::extend('order_items_required', function ($attribute, $value, $parameters, $validator)
        {
            $items = json_decode($value, true);

            return (!empty($items)) ? true : false;
        });

        Validator::extend('order_items_min', function ($attribute, $value, $parameters, $validator)
        {
            $min = 0;
            $minProductionVolume = settings('min_production_volume');
            $items = json_decode($value, true);

            if (!empty($items))
            {
                foreach ($items as $item)
                {
                    $min += $item['nomenclature']['area'];
                }
            }

            return ($minProductionVolume > 0 && $min < $minProductionVolume) ? false : true;
        });

        Validator::extend('address_number_required', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();

            return (empty($data['address_number'])) ? false : true;
        });

        Validator::extend('api_sizes_keys', function ($attribute, $value, $parameters, $validator)
        {
            if (isset($value['length']) && !empty($value['length'])
                && isset($value['width']) && !empty($value['width']))
            {
                return true;
            }
            else
            {
                return false;
            }
        });

        Validator::extend('order_unique_request_number', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();
            $order = null;

            if ($data['request_number'] != '' && isset($data['id']))
            {
                $order = Order::where([
                        ['id', '!=', $data['id']],
                        ['user_id', '=',  $data['user_id']],
                        ['request_number', '=',  $data['request_number']]
                    ])
                    ->first();
            }
            else if ($data['request_number'] != '' && !isset($data['id']))
            {
                $order = Order::where([
                        ['user_id', '=',  $data['user_id']],
                        ['request_number', '=', $data['request_number']]
                    ])
                    ->first();
            }

            return ($order) ? false : true;
        });

        Validator::extend('sample_unique_request_number', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();
            $sample = null;

            if ($data['request_number'] != '' && isset($data['id']))
            {
                $sample = Sample::where([
                        ['id', '!=', $data['id']],
                        ['user_id', '=',  $data['user_id']],
                        ['request_number', '=',  $data['request_number']],
                    ])
                    ->first();
            }
            else if ($data['request_number'] != '' && !isset($data['id']))
            {
                $sample = Sample::where([
                        ['user_id', '=',  $data['user_id']],
                        ['request_number', '=', $data['request_number']]
                    ])
                    ->first();
            }

            return ($sample) ? false : true;
        });

        Validator::extend('consignee_unique_title', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();
            $sample = null;

            if ($data['id'] != '')
            {
                $userConsignee = UserConsignee::where([
                        ['id', '!=', $data['id']],
                        ['user_id', '=',  $data['user_id']],
                        ['title', '=',  $data['title']]
                    ])
                    ->first();
            }
            else if ($data['id'] == '')
            {
                $userConsignee = UserConsignee::where([
                        ['user_id', '=',  $data['user_id']],
                        ['title', '=',  $data['title']]
                    ])
                    ->first();
            }

            return ($userConsignee) ? false : true;
        });

        Validator::extend('consignee_unique_inn', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();
            $sample = null;

            if ($data['id'] != '')
            {
                $userConsignee = UserConsignee::where([
                        ['id', '!=', $data['id']],
                        ['user_id', '=',  $data['user_id']],
                        ['inn', '=',  $data['inn']]
                    ])
                    ->first();
            }
            else if ($data['id'] == '')
            {
                $userConsignee = UserConsignee::where([
                        ['user_id', '=',  $data['user_id']],
                        ['inn', '=',  $data['inn']]
                    ])
                    ->first();
            }

            return ($userConsignee) ? false : true;
        });

        Validator::extend('consignee_unique_ogrn', function ($attribute, $value, $parameters, $validator)
        {
            $data = $validator->getData();
            $sample = null;

            if ($data['id'] != '')
            {
                $userConsignee = UserConsignee::where([
                        ['id', '!=', $data['id']],
                        ['user_id', '=',  $data['user_id']],
                        ['ogrn', '=',  $data['ogrn']]
                    ])
                    ->first();
            }
            else if ($data['id'] == '')
            {
                $userConsignee = UserConsignee::where([
                        ['user_id', '=',  $data['user_id']],
                        ['ogrn', '=',  $data['ogrn']]
                    ])
                    ->first();
            }

            return ($userConsignee) ? false : true;
        });

        // Сообщения для правил валидации
        Validator::replacer('user_is_active', function ($message, $attribute, $rule, $parameters)
        {
            return 'Пользователь не найден или неактивен.';
        });

        // Остальные сообщения в моделях...
    }
}
