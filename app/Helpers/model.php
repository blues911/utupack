<?php

use App\Models\Order;
use App\Models\Sample;
use App\Models\Claim;
use App\Models\Settings;

if (!function_exists('user_logged_in'))
{
    function user_logged_in()
    {
        return auth()->user() ? true : false;
    }
}

if (!function_exists('user_account_type'))
{
    function user_account_type($type)
    {
        if (is_array($type))
            return auth()->user() && in_array(auth()->user()->account_type, $type);
        else
            return auth()->user() && auth()->user()->account_type == $type;
    }
}

if (!function_exists('user_account_type_in'))
{
    function user_account_type_in($accountType, $types)
    {
        return in_array($accountType, $types);
    }
}

if (!function_exists('order_status_name'))
{
    function order_status_name($key)
    {
        return Order::$statusTypes[$key];
    }
}

if (!function_exists('order_delivery_name'))
{
    function order_delivery_name($key)
    {
        return Order::$deliveryTypes[$key];
    }
}

if (!function_exists('sample_status_name'))
{
    function sample_status_name($key)
    {
        return Sample::$statusTypes[$key];
    }
}

if (!function_exists('sample_delivery_name'))
{
    function sample_delivery_name($key)
    {
        return Sample::$deliveryTypes[$key];
    }
}

if (!function_exists('sample_making_name'))
{
    function sample_making_name($key)
    {
        return Sample::$makingTypes[$key];
    }
}

if (!function_exists('claim_status_name'))
{
    function claim_status_name($key)
    {
        return Claim::$statusTypes[$key];
    }
}

if (!function_exists('settings'))
{
    function settings($key)
    {
        $settings = Settings::where('var', $key)->first();
        return ($settings) ? $settings->value : null;
    }
}