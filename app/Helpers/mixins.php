<?php

use GuzzleHttp\Client as HttpClient;


if (!function_exists('fake_ext_id'))
{
    function fake_ext_id()
    {
        return rand(1000000000, 9999999999);
    }
}

if (!function_exists('fix_product_code'))
{
    function fix_product_code($code)
    {
        return ((int)$code * 1);
    }
}

if (!function_exists('random_hash'))
{
    function random_hash($len)
    {
        return substr(md5(mt_rand()), 0, $len);
    }
}

if (!function_exists('is_multi_array'))
{
    function is_multi_array($arr)
    {
        return is_array(current($arr));
    }
}

if (!function_exists('highlight_menu'))
{
    function highlight_menu($path)
    {
        $parseUrl = parse_url(url()->current());
        $parseUrl = explode('/', $parseUrl['path']);
        $path = explode('/', $path);

        return $parseUrl[1] == $path[0];
    }
}

if (!function_exists('http_get_file'))
{
    function http_get_file($path)
    {
        $file = null;

        $httpClient = new HttpClient();
        $res = $httpClient->get($path);

        if ($res->getStatusCode() == 200 && $res->hasHeader('Content-Type'))
        {
            $file['name'] = basename($path);
            $file['mime_type'] = $res->getHeader('Content-Type')[0];
            $file['file'] = $res->getBody()->getContents();
        }

        return $file;
    }
}

if (!function_exists('snumb_format'))
{
    function snumb_format($num)
    {
        // 15,000,000.0100 -> 15 000 000.01
        // 15,000,000.0000 -> 15 000 000

        $num = number_format($num, 2);

        if (preg_match('/(.).00/', $num))
            $num = str_replace('.00', '', $num);

        $num = str_replace(',', ' ', $num);

        return $num;
    }
}

if (!function_exists('plural'))
{
    function plural($num, $one, $three, $five)
    {
        $n = (int)$num;
        $forms = array($one, $three, $five);

        if (count($forms) == 3)
        {
            $plural = (($n % 10 == 1 && $n % 100 != 11) ? 0 : (($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20)) ? 1 : 2));
            return $forms[$plural];
        }
        else
        {
            return null;
        }
    }
}

if (!function_exists('get_request_atoms'))
{
    function get_request_atoms($str)
    {
        $atoms = [];

        if (preg_match_all('/([A-Z]+|[А-ЯЁ]+|[0-9]+)/uis', $str, $matches))
        {
            $atoms = $matches[0];
        }
    
        return $atoms;
    }
}