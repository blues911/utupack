<?php

if (!function_exists('db_datetime'))
{
    function db_datetime()
    {
        return date('Y-m-d H:i:s');
    }
}

if (!function_exists('to_db_datetime'))
{
    function to_db_datetime($date)
    {
        return $date ? date('Y-m-d H:i:s', strtotime($date)) : null;
    }
}

if (!function_exists('to_calendar_date'))
{
    function to_calendar_date($date)
    {
        return $date ? date('d.m.Y', strtotime($date)) : null;
    }
}

if (!function_exists('to_chat_date'))
{
    function to_chat_date($date)
    {
        if (!$date)
            return null;

        $monthNames = [
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        ];

        $Y = date('Y', strtotime($date));
        $m = date('m', strtotime($date));
        $d = date('d', strtotime($date));

        return (int)$d . ' ' . $monthNames[$m] . ' ' . $Y . ', ' . date('H:i', strtotime($date));
    }
}

if (!function_exists('to_agreement_date'))
{
    function to_agreement_date($date)
    {
        if (!$date)
            return null;

        $monthNames = [
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        ];

        $Y = date('Y', strtotime($date));
        $m = date('m', strtotime($date));
        $d = date('d', strtotime($date));

        return (int)$d . ' ' . $monthNames[$m] . ' ' . $Y . ' года';
    }
}