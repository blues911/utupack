<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Sample extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Статусы
     * 
     * @var array
     */
    public static $statusTypes = [
        'new' => 'Новый',
        'confirmed' => 'Подтвержден',
        'production' => 'Передан в производство',
        'ready' => 'Готов к отгрузке',
        'done' => 'Выполнен',
        'canceled' => 'Отменен'
    ];

    /**
     * Типы доставки
     * 
     * @var array
     */
    public static $deliveryTypes = [
        'delivery' => 'Доставка',
        'pickup' => 'Самовывоз'
    ];

    /**
     * Типы изготовления
     * 
     * @var array
     */
    public static $makingTypes = [
        'line' => 'Линия',
        'plotter' => 'Плоттер'
    ];

    /**
     * Правила валидации
     * 
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'buyer_ext_id' => 'required',
        'consignee_id' => 'required',
        'agreement_id' => 'required',
        'delivery_type' => 'required',
        'request_number' => 'nullable|sample_unique_request_number',
        'nomenclature_type_id' => 'required',
        'sizes.length' => 'required',
        'sizes.width' => 'required',
        'making' => 'required',
        'cardboard_type_id' => 'required',
        'cardboard_profile_id' => 'required',
        'cardboard_color_id' => 'required',
        'file' => 'filled|max:10000|mimes:jpg,jpeg,png,tiff,bmp,doc,docx,xls,xlsx,pdf,zip,rar'
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'user_id.required' => 'Не найден ID клиента.',
        'buyer_ext_id.required' => 'Не выбран «Покупатель».',
        'consignee_id.required' => 'Не выбран «Грузополучатель».',
        'agreement_id.required' => 'Не выбран «Договор».',
        'delivery_type.required' => 'Не выбран «Тип доставки».',
        'request_number.sample_unique_request_number' => '«Внутренний № образца» должен быть уникален.',
        'nomenclature_type_id.required' => 'Не выбран «Вид изделия».',
        'sizes.length.required' => 'Не указана «Длинна».',
        'sizes.width.required' => 'Не указана «Ширина».',
        'making.required' => 'Не выбрано «Изготовление».',
        'cardboard_type_id.required' => 'Не выбрана «Марка картона».',
        'cardboard_profile_id.required' => 'Не выбран «Профиль».',
        'cardboard_color_id.required' => 'Не выбран «Цвет».',
        'file.uploaded' => 'Загрузка файла не удалась.',
        'file.max' => 'Максимальный объем файла 10Mb.',
        'file.mimes' => 'Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar'
    ];

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'get' => [
            'sample_id' => 'required'
        ],
        'update' => [
            'sample_id' => 'required',
            'user_id' => 'filled',
            'buyer_ext_id' => 'filled',
            'consignee_id' => 'filled',
            'agreement_id' => 'filled',
            'delivery_type' => 'filled|in:delivery,pickup',
            'nomenclature_type_id' => 'filled',
            'sizes' => 'filled|array|api_sizes_keys',
            'making' => 'filled|in:line,plotter',
            'cardboard_type_id' => 'filled',
            'cardboard_profile_id' => 'filled',
            'cardboard_color_id' => 'filled',
            'product_code' => 'filled'
        ],
        'delete' => [
            'sample_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'sample';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Клиент
     * 
     * @return \App\Models\User
     */
    public function client()
    {
        return $this->hasOne(
                'App\Models\User',
                'id',
                'user_id'
            );
    }

    /**
     * Покупатель
     * 
     * @return \App\Models\UserAgreement
     */
    public function buyer()
    {
        return $this->hasOne(
                'App\Models\UserAgreement',
                'buyer_ext_id',
                'buyer_ext_id'
            );
    }

    /**
     * Грузополучатель
     * 
     * @return \App\Models\UserConsignee
     */
    public function consignee()
    {
        return $this->hasOne(
                'App\Models\UserConsignee',
                'id',
                'consignee_id'
            );
    }

    /**
     * Договор
     * 
     * @return \App\Models\UserAgreement
     */
    public function agreement()
    {
        return $this->hasOne(
                'App\Models\UserAgreement',
                'id',
                'agreement_id'
            );
    }

    /**
     * Вид изделия
     * 
     * @return \App\Models\NomenclatureType
     */
    public function nomenclatureType()
    {
        return $this->hasOne(
                'App\Models\NomenclatureType',
                'id',
                'nomenclature_type_id'
            );
    }

    /**
     * Картон
     * 
     * @return \App\Models\CardboardType
     */
    public function cardboardType()
    {
        return $this->hasOne(
                'App\Models\CardboardType',
                'id',
                'cardboard_type_id'
            );
    }

    /**
     * Профиль картона
     * 
     * @return \App\Models\CardboardProfile
     */
    public function cardboardProfile()
    {
        return $this->hasOne(
                'App\Models\CardboardProfile',
                'id',
                'cardboard_profile_id'
            );
    }

    /**
     * Цвет картона
     * 
     * @return \App\Models\CardboardColor
     */
    public function cardboardColor()
    {
        return $this->hasOne(
                'App\Models\CardboardColor',
                'id',
                'cardboard_color_id'
            );
    }

    /**
     * Файл с чертежом
     * 
     * @return \App\Models\File
     */
    public function file()
    {
        return $this->hasOne(
                'App\Models\File',
                'object_id',
                'id'
            )
            ->where('object_type', 'sample');
    }

    /**
     * Чат
     * 
     * @return \App\Models\Chat
     */
    public function chat()
    {
        return $this->hasMany(
                'App\Models\Chat',
                'object_id',
                'id'
            )
            ->where('object_type', 'sample')
            ->orderBy('date', 'desc');
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
                'client',
                'buyer',
                'consignee',
                'agreement',
                'nomenclatureType',
                'cardboardType',
                'cardboardProfile',
                'cardboardColor',
                'file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'chat.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->find($id);
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with([
                'client',
                'buyer',
                'consignee',
                'agreement',
                'nomenclatureType',
                'cardboardType',
                'cardboardProfile',
                'cardboardColor',
                'file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'chat.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Список с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    public static function allForAccountType($filters)
    {
        $query = self::prepareForAccountType($filters);
        
        return $query
            ->skip($filters['offset'])
            ->take($filters['limit'])
            ->orderBy('date', 'desc')
            ->get();
    }

    /**
     * Кол-во с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    public static function countForAccountType($filters)
    {
        $query = self::prepareForAccountType($filters);
        
        return $query->count();
    }

    /**
     * Подготовительный запрос с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    protected static function prepareForAccountType($filters)
    {
        $query = self::with([
            'client',
            'consignee',
            'agreement',
            'nomenclatureType',
            'cardboardType',
            'cardboardProfile',
            'cardboardColor'
        ])
        ->withCount(['chat' => function($query) {
            if (user_account_type('admin'))
                $query->where('read_by_admin', 0);
            if (user_account_type('manager'))
                $query->where('read_by_manager', 0);
            if (user_account_type('client'))
                $query->where('read_by_client', 0);
        }]);

        // Определение аккаунта
        if (user_account_type('client'))
            $query->where('user_id', Auth::user()->id);

        if (user_account_type('manager'))
        {
            $query->whereIn('user_id', function($query) {
                $query->select('id')
                    ->from('user')
                    ->where('manager_id', Auth::user()->id);
            });
        }
        
        // Фильтры
        if ($filters['date_range'] != '')
        {
            $row = explode(' ', str_replace('/\s(.*)\s/', ' ', $filters['date_range']));
            $query->whereBetween('date', [to_db_datetime($row[0]), to_db_datetime($row[2])]);
        }

        if ($filters['status'] != '')
            $query->where('status', $filters['status']);

        if ($filters['client'] != '')
            $query->where('user_id', $filters['client']);

        return $query;
    }

    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['sample_id'] = $this->ext_id;
        $data['user_id'] = $this->client->ext_id;
        $data['buyer_id'] = $this->buyer->buyer_ext_id;
        $data['consignee_id'] = $this->consignee->ext_id;
        $data['agreement_id'] = $this->agreement->ext_id;
        $data['request_number'] = $this->request_number;
        $data['delivery_type'] = $this->delivery_type;
        $data['making'] = $this->making;
        $data['sizes'] = json_decode($this->sizes, true);
        $data['cnt'] = $this->cnt;
        $data['comment'] = $this->comment;
        $data['status'] = $this->status;
        $data['date'] = $this->date;
        $data['nomenclature_type_id'] = $this->nomenclatureType->ext_id;
        $data['cardboard_type_id'] = $this->cardboardType->ext_id;
        $data['cardboard_profile_id'] = $this->cardboardProfile->ext_id;
        $data['cardboard_color_id'] = $this->cardboardColor->ext_id;
        $data['file'] = ($this->file) ? url('shared/read-file/' . $this->file->hash) : null;
        $data['product_code'] = $this->product_code;

        return $data;
    }
}