<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'settings';
}
