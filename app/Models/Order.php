<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\OrderItem;
use App\Models\File;

class Order extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Статусы
     * 
     * @var array
     */
    public static $statusTypes = [
        'new' => 'Новая',
        'confirmed' => 'Подтверждена',
        'production' => 'Передана в производство',
        'ready' => 'Готова к отгрузке',
        'done' => 'Выполнена',
        'canceled' => 'Отменена'
    ];

    /**
     * Типы доставки
     * 
     * @var array
     */
    public static $deliveryTypes = [
        'delivery' => 'Доставка',
        'pickup' => 'Самовывоз'
    ];

    /**
     * Варианты даты
     * 
     * @var array
     */
    public static $dateTypes = [
        // 'date' => 'По дате заявки',
        'desired_date' => 'По дате отгрузки',
        'ready_date' => 'По дате доставки',
    ];

    /**
     * Правила валидации
     * 
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'buyer_ext_id' => 'required',
        'consignee_id' => 'required',
        'agreement_id' => 'required',
        'delivery_type' => 'required',
        'desired_date' => 'required',
        'request_number' => 'nullable|order_unique_request_number',
        'items' => 'order_items_required|order_items_min',
        'file' => 'filled|max:10000|mimes:jpg,jpeg,png,tiff,bmp,doc,docx,xls,xlsx,pdf,zip,rar'
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'user_id.required' => 'Не найден ID клиента.',
        'buyer_ext_id.required' => 'Не выбран «Покупатель».',
        'consignee_id.required' => 'Не выбран «Грузополучатель».',
        'agreement_id.required' => 'Не выбран «Договор».',
        'delivery_type.required' => 'Не выбран «Тип доставки».',
        'desired_date.required' => 'Отсутствует «Дата отгрузки/доставки».',
        'request_number.order_unique_request_number' => '«Внутренний № заявки» должен быть уникален.',
        'items.order_items_required' => 'Отсутствуют «Позиции».',
        'items.order_items_min' => 'Объем заявки меньше необходимого.',
        'file.uploaded' => 'Загрузка файла не удалась.',
        'file.max' => 'Максимальный объем файла 10Mb.',
        'file.mimes' => 'Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar.'
    ];

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'get' => [
            'order_id' => 'required'
        ],
        'update' => [
            'order_id' => 'required',
            'buyer_id' => 'filled',
            'consignee_id' => 'filled',
            'agreement_id' => 'filled',
            'delivery_type' => 'filled|in:delivery,pickup',
            'desired_date' => 'filled',
            'status' => 'filled|in:new,confirmed,production,ready,done,canceled',
            'product_code' => 'filled'
        ],
        'delete' => [
            'order_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'order';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Клиент заявки
     * 
     * @return \App\Models\User
     */
    public function client()
    {
        return $this->hasOne(
                'App\Models\User',
                'id',
                'user_id'
            );
    }

    /**
     * Покупатель
     * 
     * @return \App\Models\UserAgreement
     */
    public function buyer()
    {
        return $this->hasOne(
                'App\Models\UserAgreement',
                'buyer_ext_id',
                'buyer_ext_id'
            );
    }

    /**
     * Грузополучатель
     * 
     * @return \App\Models\UserConsignee
     */
    public function consignee()
    {
        return $this->hasOne(
                'App\Models\UserConsignee',
                'id',
                'consignee_id'
            );
    }

    /**
     * Договор
     * 
     * @return \App\Models\UserAgreement
     */
    public function agreement()
    {
        return $this->hasOne(
                'App\Models\UserAgreement',
                'id',
                'agreement_id'
            );
    }

    /**
     * Позиции
     * 
     * @return \App\Models\OrderItem
     */
    public function items()
    {
        return $this->hasMany(
                'App\Models\OrderItem',
                'order_id',
                'id'
            );
    }

    /**
     * Доверенность для водителя
     * 
     * @return \App\Models\File
     */
    public function file()
    {
        return $this->hasOne(
                'App\Models\File',
                'object_id',
                'id'
            )
            ->where('object_type', 'order');
    }

    /**
     * Чат
     * 
     * @return \App\Models\File
     */
    public function chat()
    {
        return $this->hasMany(
                'App\Models\Chat',
                'object_id',
                'id'
            )
            ->where('object_type', 'order')
            ->orderBy('date', 'desc');
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
                'client',
                'buyer',
                'consignee',
                'agreement',
                'items.nomenclature.type',
                'items.nomenclature.cardboardType',
                'items.nomenclature.cardboardProfile',
                'items.nomenclature.cardboardColor',
                'file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'chat.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->find($id);
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with([
                'client',
                'buyer',
                'consignee',
                'agreement',
                'items.nomenclature.type',
                'items.nomenclature.cardboardType',
                'items.nomenclature.cardboardProfile',
                'items.nomenclature.cardboardColor',
                'file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'chat.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Список для претензии клиента
     * 
     * @param int $userId
     * @return mixed
     */
    public static function allForClaim($userId)
    {
        return self::with([
                'client',
                'items.nomenclature'
            ])
            ->where('user_id', $userId)
            ->where('status', '!=', 'canceled')
            ->whereNotNull('ext_id')
            ->get();
    }

    /**
     * Список с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    public static function allForAccountType($filters)
    {
        $query = self::prepareForAccountType($filters);
        
        return $query->skip($filters['offset'])
            ->take($filters['limit'])
            ->orderBy('date', 'desc')
            ->get();
    }

    /**
     * Кол-во с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    public static function countForAccountType($filters)
    {
        $query = self::prepareForAccountType($filters);
        
        return $query->count();
    }
    
    /**
     * Подготовительный запрос с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    protected static function prepareForAccountType($filters)
    {
        $query = self::with([
            'client',
            'items.nomenclature.type',
            'items.nomenclature.cardboardType',
            'items.nomenclature.cardboardProfile',
            'items.nomenclature.cardboardColor',
        ])
        ->withCount(['chat' => function($query) {
            if (user_account_type('admin'))
                $query->where('read_by_admin', 0);
            if (user_account_type('manager'))
                $query->where('read_by_manager', 0);
            if (user_account_type('client'))
                $query->where('read_by_client', 0);
        }]);

        // Определение аккаунта
        if (user_account_type('client'))
            $query->where('user_id', Auth::user()->id);

        if (user_account_type('manager'))
        {
            $query->whereIn('user_id', function($query) {
                $query->select('id')
                    ->from('user')
                    ->where('manager_id', Auth::user()->id);
            });
        }

        // Фильтры
        if ($filters['date_range'] != '')
        {
            $row = explode(' ', str_replace('/\s(.*)\s/', ' ', $filters['date_range']));
            $query->whereBetween('date', [to_db_datetime($row[0]), to_db_datetime($row[2])]);
        }

        if ($filters['status'] != '')
            $query->where('status', $filters['status']);

        if ($filters['client'] != '')
            $query->where('user_id', $filters['client']);

        if ($filters['by_date'] != '')
            $query->orderBy($filters['by_date'], 'desc');



        return $query;
    }
    
    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['order_id'] = $this->ext_id;
        $data['user_id'] = $this->client->ext_id;
        $data['buyer_id'] = $this->buyer->buyer_ext_id;
        $data['consignee_id'] = $this->consignee->ext_id;
        $data['agreement_id'] = $this->agreement->ext_id;
        $data['request_number'] = $this->request_number;
        $data['delivery_type'] = $this->delivery_type;
        $data['desired_date'] = $this->desired_date;
        $data['ready_date'] = $this->ready_date;
        $data['delivery_date'] = $this->delivery_date;
        $data['comment'] = $this->comment;
        $data['status'] = $this->status;
        $data['date'] = $this->date;
        $data['driver_attorney_file'] = ($this->file) ? url('shared/read-file/' . $this->file->hash) : null;
        $data['product_code'] = $this->product_code;

        foreach ($this->items as $item)
        {
            $data['items'][] = [
                'nomenclature_id' => $item->nomenclature->ext_id,
                'address' => $item->address,
                'address_number' => $item->address_number,
                'cnt' => $item->cnt,
                'cnt_rule' => $item->cnt_rule,
                'produced' => $item->produced,
                'comment' => $item->comment,
                'amount' => $item->amount,
            ];
        }

        return $data;
    }
}
