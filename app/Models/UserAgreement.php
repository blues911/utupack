<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserAgreement extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'create' => [
            'agreement_id' => 'required',
            'user_id' => 'required',
            'buyer_id' => 'required',
            'buyer_title' => 'required',
            'number' => 'required',
            'date' => 'required',
            'status' => 'required'
        ],
        'update' => [
            'agreement_id' => 'required',
            'user_id' => 'filled',
            'buyer_id' => 'filled',
            'buyer_title' => 'filled',
            'number' => 'filled',
            'date' => 'filled',
            'min_execution' => 'filled',
            'info' => 'filled',
            'status' => 'filled'
        ],
        'delete' => [
            'agreement_id' => 'required'
        ],
        'delete_all' => [
            'user_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'user_agreement';

    /**
     * Договора покупателя
     * 
     * @return \App\Models\UserAgreement
     */
    public function buyerAgreements()
    {
        return $this->hasMany(
                'App\Models\UserAgreement',
                'buyer_ext_id',
                'buyer_ext_id'
            );
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with('buyerAgreements')
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Список покупателей для клиента
     * 
     * @return mixed
     */
    public static function allBuyersForClient($userId)
    {
        return self::with(['buyerAgreements' => function($query){
                $query->where('status', 1);
            }])
            ->where('user_id', $userId)
            ->where('status', 1)
            ->groupBy('buyer_ext_id')
            ->get();
    }
}
