<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'get' => [
            'user_id' => 'required'
        ],
        'create' => [
            'user_id' => 'required',
            'account_type' => 'required|in:client,manager,admin',
            'status' => 'required',
            'name' => 'required',
            'login' => 'required',
            'password' => 'required',
            'email' => 'required',
            'manager.user_id' => 'required_if:account_type,client',
            'manager.phone' => 'required_if:account_type,client',
            'manager.photo' => 'required_if:account_type,client',
        ],
        'update' => [
            'user_id' => 'required',
            'account_type' => 'filled|in:client,manager,admin',
            'status' => 'filled',
            'name' => 'filled',
            'login' => 'filled',
            'password' => 'filled',
            'email' => 'filled',
            'manager.user_id' => 'filled',
            'manager.phone' => 'filled',
            'manager.email' => 'filled',
        ],
        'delete' => [
            'user_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'required_if' => 'Поле обязательно для заполнения, кода тип аккаунта client.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'user';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Менеджер клиента
     * 
     * @return \App\Models\User
     */
    public function clientManager()
    {
        return $this->hasOne(
                'App\Models\User',
                'id',
                'manager_id'
            );
    }

    /**
     * Договора клиента
     * 
     * @return \App\Models\UserAgreement
     */
    public function clientAgreements()
    {
        return $this->hasMany(
                'App\Models\UserAgreement',
                'user_id',
                'id'
            );
    }

    /**
     * Грузополучатели клиента
     * 
     * @return \App\Models\UserConsignee
     */
    public function clientConsignees()
    {
        return $this->hasMany(
                'App\Models\UserConsignee',
                'user_id',
                'id'
            );
    }

    /**
     * Грузополучатели клиента
     * 
     * @return \App\Models\UserNomenclature
     */
    public function clientNomenclatures()
    {
        return $this->hasMany(
                'App\Models\UserNomenclature',
                'user_id',
                'id'
            );
    }

    /**
     * Заявки клиента
     * 
     * @return \App\Models\Order
     */
    public function clientOrders()
    {
        return $this->hasMany(
                'App\Models\Order',
                'user_id',
                'id'
            );
    }

    /**
     * Образцы клиента
     * 
     * @return \App\Models\Sample
     */
    public function clientSamples()
    {
        return $this->hasMany(
                'App\Models\Sample',
                'user_id',
                'id'
            );
    }

    /**
     * Претензии клиента
     * 
     * @return \App\Models\Claim
     */
    public function clientClaims()
    {
        return $this->hasManyThrough(
                'App\Models\Claim',
                'App\Models\Order',
                'user_id',
                'order_id',
                'id',
                'id'
            );
    }

    /**
     * Фото менеджера
     * 
     * @return \App\Models\File
     */
    public function file()
    {
        return $this->hasOne(
                'App\Models\File',
                'object_id',
                'id'
            )
            ->where('object_type', 'user');
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
                'clientManager.file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                }
            ])
            ->find($id);
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with([
                'clientManager.file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'file' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                }
            ])
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Найти по id и account_type = client
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneByClientId($id)
    {
        return self::with([
              'clientManager.file' => function($query) {
                  $query->select('id', 'object_id', 'name', 'hash');
              },
              'clientAgreements',
              'clientConsignees',
              'clientNomenclatures',
              'clientOrders',
              'clientSamples',
              'clientClaims'
          ])
          ->where('account_type', 'client')
          ->find($id);
    }

    /**
     * Список всех клиентов
     * 
     * @return mixed
     */
    public static function allClients()
    {
        $user = Auth::user();

        switch ($user->account_type)
        {
            case 'admin':
                return self::where('account_type', 'client')
                    ->where('status', 1)
                    ->get();
                break;

            case 'manager':
                return self::where('manager_id', $user->id)
                    ->where('account_type', 'client')
                    ->where('status', 1)
                    ->get();
                break;

            default:
                return null;
        }
    }

    /**
     * Подготовка данных для внутреннего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['user_id'] = $this->ext_id;
        $data['account_type'] = $this->account_type;
        $data['status'] = $this->status;
        $data['name'] = $this->name;
        $data['login'] = $this->login;
        
        if ($this->account_type == 'client' && $this->clientManager)
        {
            $data['manager'] = [];
            $data['manager']['user_id'] = $this->clientManager->ext_id;
            $data['manager']['phone'] = $this->clientManager->phone;
            $data['manager']['email'] = $this->clientManager->email;
            $data['manager']['photo'] = url('shared/read-file/' . $this->clientManager->file->hash);
        }

        return $data;
    }
}
