<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiLog extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'api_log';
    
    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Атрибуты, которые можно присвоить массово
     * 
     * @var string
     */
    protected $fillable = [
        'type',
        'url',
        'request',
        'response',
        'exception',
        'time_elapsed',
        'date'
    ];
}