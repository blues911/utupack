<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrderItem extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Правила валидации
     *
     * @var array
     */
    public static $rules = [
        'id' => 'required',
        'nomenclature_id' => 'required',
        'address' => 'required',
        'address_number' => 'address_number_required',
        'cnt' => 'required',
        'cnt_rule' => 'required'
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'nomenclature_id.required' => 'Не выбрана «Товарная номенклатура».',
        'address.required' => 'Не указан «Адрес доставки».',
        'address_number.address_number_required' => 'Не указан «Номер точки выгрузки».',
        'cnt.required' => 'Не выбрано «Количество».',
        'cnt_rule.required' => 'Не указана «Кол-во шт.».'
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'order_item';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Заявка
     * 
     * @return \App\Models\Order
     */
    public function order()
    {
        return $this->belongsTo(
                'App\Models\Order',
                'order_id'
            );
    }

    /**
     * Вид изделия
     * 
     * @return \App\Models\UserNomenclature
     */
    public function nomenclature()
    {
        return $this->belongsTo(
                'App\Models\UserNomenclature',
                'nomenclature_id'
            );
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
              'nomenclature.type',
              'nomenclature.cardboardType',
              'nomenclature.cardboardProfile',
              'nomenclature.cardboardColor'
            ])
            ->find($id);
    }

    /**
     * Найти по id заявки и id изделия
     * 
     * @param int $orderId
     * @param int $nomenclatureId
     * @return mixed
     */
    public static function oneByOrderIdNomenclatureId($orderId, $nomenclatureId)
    {
        return self::where('order_id', $orderId)
            ->where('nomenclature_id', $nomenclatureId)
            ->first();
    }

    /**
     * Список позиций для заявки
     * 
     * @param array $params
     * @return mixed
     */
    public static function allForOrder($params)
    {
        return self::with([
                'nomenclature.type',
                'nomenclature.cardboardType',
                'nomenclature.cardboardProfile',
                'nomenclature.cardboardColor'
            ])
            ->where('order_id', $params['order_id'])
            ->get();
    }

    /**
     * Список уникальных адресов для позиции
     * 
     * @param int $userId
     * @return array
     */
    public static function allAddressesForClient($userId)
    {
        $result = [];

        $list = self::with('order')
            ->whereHas('order', function($query) use($userId) {
                $query->where('order.user_id', $userId);
            })
            ->select('address')
            ->distinct()
            ->get();

        if (!empty($list))
        {
            foreach ($list as $item)
            {
                $result[] = $item->address;
            }
        }

        return $result;
    }
}
