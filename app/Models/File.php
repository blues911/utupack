<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'file';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Найти по hash
     * 
     * @param string $hash
     */
    public static function oneByHash($hash)
    {
        return self::where('hash', $hash)->first();
    }

    /**
     * Найти по object_type и object_id
     * 
     * @param string $objectType
     * @param int $objectId
     */
    public static function oneByObjectData($objectType, $objectId)
    {
        return self::where([
                'object_type' => $objectType,
                'object_id' =>  $objectId
            ])
            ->first();
    }
}
