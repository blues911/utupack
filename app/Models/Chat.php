<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Order;
use App\Models\Sample;
use App\Models\Claim;

class Chat extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации
     * 
     * @var array
     */
    public static $rules = [
        'object_id' => 'required',
        'object_type' => 'required',
        'message' => 'required',
        'files.*' => 'filled|max:10000|mimes:jpg,jpeg,png,tiff,bmp,doc,docx,xls,xlsx,pdf,zip,rar'
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'message.required' => 'Не указан текст сообщения.',
        'files.*.uploaded' => 'Загрузка файлов не удалась.',
        'files.*.max' => 'Максимальный объем файла 10Mb.',
        'file.*.mimes' => 'Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar.'
    ];

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'add' => [
            'message_id' => 'required',
            'object_type' => 'required|in:order,sample,claim',
            'object_id' => 'required',
            'user_id' => 'required',
            'date' => 'required',
            'message' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'chat';
    
    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Пользователь (client, manage, admin)
     * 
     * @return \App\Models\User
     */
    public function user()
    {
        return $this->hasOne(
                'App\Models\User',
                'id',
                'user_id'
            );
    }

    /**
     * Прикрепленный файлы
     * 
     * @return \App\Models\User
     */
    public function files()
    {
        return $this->hasMany(
                'App\Models\File',
                'object_id',
                'id'
            )
            ->where('object_type', 'chat');
    }

    /**
     * Заявка
     * 
     * @return \App\Models\Order
     */
    public function order()
    {
        return $this->hasOne(
                'App\Models\Order',
                'id',
                'object_id'
            );
    }

    /**
     * Образец
     * 
     * @return \App\Models\Sample
     */
    public function sample()
    {
        return $this->hasOne(
                'App\Models\Sample',
                'id',
                'object_id'
            );
    }

    /**
     * Претензия
     * 
     * @return \App\Models\Claim
     */
    public function claim()
    {
        return $this->hasOne(
                'App\Models\Claim',
                'id',
                'object_id'
            );
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
                'user',
                'files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->find($id);
    }

    /**
     * Найти по ext_id
     * 
     * @param string $id
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with([
                'user',
                'files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Список непрочитанных с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param int $id
     * @return mixed
     */
    public static function unreadForAccountType($params)
    {
        $query = self::with([
                  'user',
                  'files' => function($query) {
                      $query->select('id', 'object_id', 'name', 'hash');
                  },
              ])
              ->where('object_type', $params['object_type'])
              ->where('object_id', $params['object_id']);
        
        if (user_account_type('admin'))
            $query->where('read_by_admin', 0);
        if (user_account_type('manager'))
            $query->where('read_by_manager', 0);
        if (user_account_type('client'))
            $query->where('read_by_client', 0);

        return $query->get();
    }

    /**
     * Общее кол-во непрочитанных с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param int $id
     * @return mixed
     */
    public static function countUnreadForMenu($objectType)
    {
        $query = self::where('object_type', $objectType);

        if (user_account_type('admin'))
            $query->where('read_by_admin', 0);

        if (user_account_type('manager'))
        {
            $query->whereIn('object_id', function($query) use($objectType) {

                    if ($objectType == 'claim')
                    {
                        $query->select('claim.id')
                            ->from('claim')
                            ->join('order', 'order.id', '=', 'claim.order_id')
                            ->whereIn('order.user_id', function($query) {
                                  $query->select('id')
                                      ->from('user')
                                      ->where('manager_id', Auth::user()->id);
                            });
                    }
                    else
                    {
                        $query->select('id')
                            ->from($objectType)
                            ->whereIn('user_id', function($query) {
                                  $query->select('id')
                                      ->from('user')
                                      ->where('manager_id', Auth::user()->id);
                            });
                    }
                })
                ->where('read_by_manager', 0);
        }

        if (user_account_type('client'))
        {
            $query->whereIn('object_id', function($query) use($objectType) {

                    if ($objectType == 'claim')
                    {
                        $query->select('claim.id')
                            ->from('claim')
                            ->join('order', 'order.id', '=', 'claim.order_id')
                            ->where('order.user_id', Auth::user()->id);
                    }
                    else
                    {
                        $query->select('id')
                            ->from($objectType)
                            ->where('user_id', Auth::user()->id);
                    }
                })
                ->where('read_by_client', 0);
        }

        return $query->count();
    }

    /**
     * Кол-во непрочитанных c учетом интервала для email
     * 
     * @param int $userId
     * @param string $accountType
     * @param string $objectType
     * @return mixed
     */
    public static function countUnreadForMail($objectType, $userId, $accountType)
    {
        $dateInterval = date('Y-m-d H:i:s', strtotime('-5 minutes'));

        if ($accountType == 'admin')
        {
            return self::selectRaw('object_type, object_id, count(*) as total')
                ->where('read_by_admin', 0)
                ->where('date', '>=', $dateInterval)
                ->where('object_type', $objectType)
                ->groupBy('object_type')
                ->groupBy('object_id')
                ->get();
        }

        if ($accountType == 'manager')
        {
            $query = self::selectRaw('object_type, object_id, count(*) as total')
                ->where('read_by_manager', 0)
                ->where('date', '>=', $dateInterval)
                ->where('object_type', $objectType);
            
            if ($objectType == 'claim')
            {
                $query->whereIn('object_id', function($query) use($userId) {
                    $query->select('claim.id')
                        ->from('claim')
                        ->join('order', 'order.id', '=', 'claim.order_id')
                        ->whereIn('order.user_id', function($query) use($userId) {
                              $query->select('id')
                                  ->from('user')
                                  ->where('manager_id', $userId);
                        });
                });
            }
            else
            {
                $query->whereIn('object_id', function($query) use($objectType, $userId) {
                    $query->select('id')
                        ->from($objectType)
                        ->whereIn('user_id', function($query) use($userId) {
                              $query->select('id')
                                  ->from('user')
                                  ->where('manager_id', $userId);
                    });
                });
            }

            return $query->groupBy('object_type')
                ->groupBy('object_id')
                ->get();
        }

        if ($accountType == 'client')
        {
            $query = self::selectRaw('object_type, object_id, count(*) as total')
                ->where('read_by_client', 0)
                ->where('date', '>=', $dateInterval)
                ->where('object_type', $objectType);

            if ($objectType == 'claim')
            {
                $query->whereIn('object_id', function($query) use($userId) {
                    $query->select('claim.id')
                        ->from('claim')
                        ->join('order', 'order.id', '=', 'claim.order_id')
                        ->where('order.user_id', $userId);
                });
            }
            else
            {
                $query->whereIn('object_id', function($query) use($objectType, $userId) {
                    $query->select('id')
                        ->from($objectType)
                        ->where('user_id', $userId);
                });
            }

            return $query->groupBy('object_type')
                ->groupBy('object_id')
                ->get();
        }

        return null;
    }

    /**
     * Непрочитанные c учетом интервала для email
     * 
     * @param int $userId
     * @param string $accountType
     * @param string $objectType
     * @return mixed
     */
    public static function getUnreadForMail($objectType, $userId, $accountType)
    {
        $dateInterval = date('Y-m-d H:i:s', strtotime('-5 minutes'));

        if ($accountType == 'admin')
        {
            return self::with(['user', $objectType])
                ->where('read_by_admin', 0)
                ->where('date', '>=', $dateInterval)
                ->where('object_type', $objectType)
                ->get();
        }

        if ($accountType == 'manager')
        {
            $query = self::with(['user', $objectType])
                ->where('read_by_manager', 0)
                ->where('date', '>=', $dateInterval)
                ->where('object_type', $objectType);
            
            if ($objectType == 'claim')
            {
                $query->whereIn('object_id', function($query) use($userId) {
                    $query->select('claim.id')
                        ->from('claim')
                        ->join('order', 'order.id', '=', 'claim.order_id')
                        ->whereIn('order.user_id', function($query) use($userId) {
                              $query->select('id')
                                  ->from('user')
                                  ->where('manager_id', $userId);
                        });
                });
            }
            else
            {
                $query->whereIn('object_id', function($query) use($objectType, $userId) {
                    $query->select('id')
                        ->from($objectType)
                        ->whereIn('user_id', function($query) use($userId) {
                              $query->select('id')
                                  ->from('user')
                                  ->where('manager_id', $userId);
                    });
                });
            }

            return $query->get();
        }

        if ($accountType == 'client')
        {
            $query = self::with(['user', $objectType])
                ->where('read_by_client', 0)
                ->where('date', '>=', $dateInterval)
                ->where('object_type', $objectType);

            if ($objectType == 'claim')
            {
                $query->whereIn('object_id', function($query) use($userId) {
                    $query->select('claim.id')
                        ->from('claim')
                        ->join('order', 'order.id', '=', 'claim.order_id')
                        ->where('order.user_id', $userId);
                });
            }
            else
            {
                $query->whereIn('object_id', function($query) use($objectType, $userId) {
                    $query->select('id')
                        ->from($objectType)
                        ->where('user_id', $userId);
                });
            }

            return $query->get();
        }

        return null;
    }

    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['message_id'] = $this->ext_id;
        $data['object_type'] = $this->object_type;

        if ($this->object_type == 'order')
        {
            $order = Order::find($this->object_id);
            $data['object_id'] = $order->ext_id;
        }
        else if ($this->object_type == 'sample')
        {
            $sample = Sample::find($this->object_id);
            $data['object_id'] = $sample->ext_id;
        }
        else if ($this->object_type == 'claim')
        {
            $claim = Claim::find($this->object_id);
            $data['object_id'] = $claim->ext_id;
        }

        $data['user_id'] = $this->user->ext_id;
        $data['date'] = $this->date;
        $data['is_client'] = ($this->user->account_type == 'client') ? 1 : 0;
        $data['message'] = $this->message;

        $files = [];

        if ($this->files()->count() > 0)
        {
            foreach ($this->files as $file)
            {
                $files[] = url('shared/read-file/' . $file->hash);
            }
        }

        $data['files'] = json_encode($files);

        return $data;
    }
}