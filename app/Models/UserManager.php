<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserManager extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'user_manager';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Данные аккаунта менеджера
     * 
     * @return \App\Models\User
     */
    public function manager()
    {
        return $this->belongsTo(
                'App\Models\User',
                'user_id'
            );
    }
}
