<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserNomenclature extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'create' => [
            'nomenclature_id' => 'required',
            'user_id' => 'required',
            'nomenclature_type_id' => 'required',
            'cardboard_type_id' => 'required',
            'cardboard_profile_id' => 'required',
            'cardboard_color_id' => 'required',
            'title' => 'required',
            'area' => 'required',
            'product_code' => 'required',
            'status' => 'required',
        ],
        'update' => [
            'nomenclature_id' => 'required',
            'user_id' => 'filled',
            'nomenclature_type_id' => 'filled',
            'cardboard_type_id' => 'filled',
            'cardboard_profile_id' => 'filled',
            'cardboard_color_id' => 'filled',
            'title' => 'filled',
            'sku' => 'filled',
            'area' => 'filled',
            'product_code' => 'filled',
            'status' => 'filled',
        ],
        'delete' => [
            'nomenclature_id' => 'required'
        ],
        'delete_all' => [
            'user_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     *
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'user_nomenclature';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * Тип товарной номенклатуры
     * 
     * @return \App\Models\NomenclatureType
     */
    public function type()
    {
        return $this->hasOne(
                'App\Models\NomenclatureType',
                'id',
                'nomenclature_type_id'
            );
    }

    /**
     * Тип картона
     * 
     * @return \App\Models\CardboardType
     */
    public function cardboardType()
    {
        return $this->hasOne(
                'App\Models\CardboardType',
                'id',
                'cardboard_type_id'
            );
    }

    /**
     * Профиль картона
     * 
     * @return \App\Models\CardboardProfile
     */
    public function cardboardProfile()
    {
        return $this->hasOne(
                'App\Models\CardboardProfile',
                'id',
                'cardboard_profile_id'
            );
    }

    /**
     * Цвет картона
     * 
     * @return \App\Models\CardboardColor
     */
    public function cardboardColor()
    {
        return $this->hasOne(
                'App\Models\CardboardColor',
                'id',
                'cardboard_color_id'
            );
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with(
                'type',
                'cardboardType',
                'cardboardProfile',
                'cardboardColor'
            )
            ->where('ext_id', $extId)
            ->first();
    }
    
    /**
     * Список товарной номенклатуры для клиента
     * 
     * @return mixed
     */
    public static function allForClient($userId)
    {
        return self::with(
                'type',
                'cardboardType',
                'cardboardProfile',
                'cardboardColor'
            )
            ->where('user_id', '=', $userId)
            ->get();
    }

    /**
     * Фильтр товарной номенклатуры для клиента
     * 
     * @return mixed
     */
    public static function filterForClient($params)
    {
        $query = self::with(
                'type',
                'cardboardType',
                'cardboardProfile',
                'cardboardColor'
            )
            ->where('user_id', '=', $params['user_id'])
            ->where('status', '=', 1);

        if (!empty($params['nomenclature_search']))
        {
            $search = get_request_atoms($params['nomenclature_search']);
            $search = '%' . implode('%', $search) . '%';

            $query->where(function($q) use($search) {
                $q->where('title', 'like', $search)
                    ->orWhere('sku', 'like', $search);
            });
        }
        
        if ($params['nomenclature_print'] != '')
            $query->where('print', '=', $params['nomenclature_print']);

        if (!empty($params['cardboard_type_id']))
            $query->where('cardboard_type_id', '=', $params['cardboard_type_id']);

        if (!empty($params['cardboard_profile_id']))
            $query->where('cardboard_profile_id', '=', $params['cardboard_profile_id']);

        if (!empty($params['cardboard_color_id']))
            $query->where('cardboard_color_id', '=', $params['cardboard_color_id']);

        return $query->get();
    }
}
