<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardboardProfile extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'get' => [
            'cardboard_profile_id' => 'required'
        ],
        'create' => [
            'cardboard_profile_id' => 'required',
            'title' => 'required',
        ],
        'update' => [
            'cardboard_profile_id' => 'required',
            'title' => 'filled',
        ],
        'delete' => [
            'cardboard_profile_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'cardboard_profile';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * Тип изделия
     * 
     * @return \App\Models\UserNomenclature
     */
    public function nomenclature()
    {
        return $this->belongsTo(
                'App\Models\UserNomenclature',
                'cardboard_profile_id'
            );
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::where('ext_id', $extId)->first();
    }

    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['ext_id'] = $this->ext_id;
        $data['title'] = $this->title;

        return $data;
    }
}
