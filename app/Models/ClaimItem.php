<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClaimItem extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации
     * 
     * @var array
     */
    public static $rules = [
        'nomenclature_id' => 'required',
        'claim' => 'required',
        'cnt' => 'required',
        'files.*' => 'filled|max:10000|mimes:jpg,jpeg,png,tiff,bmp,doc,docx,xls,xlsx,pdf,zip,rar'
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'nomenclature_id.required' => 'Не выбрано «Изделие».',
        'claim.required' => 'Не заполнено поле «Претензия или проблема с изделием».',
        'cnt.required' => 'Не указано «Кол-во проблемных позиций».',
        'files.*.uploaded' => 'Загрузка файлов не удалась.',
        'files.*.max' => 'Максимальный объем файла 10Mb.',
        'file.*.mimes' => 'Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar'
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'claim_item';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Претензия
     * 
     * @return \App\Models\Claim
     */
    public function claim()
    {
        return $this->hasOne(
                'App\Models\Claim',
                'id',
                'claim_id'
            );
    }

    /**
     * Изделие
     * 
     * @return \App\Models\UserNomenclature
     */
    public function nomenclature()
    {
        return $this->hasOne(
                'App\Models\UserNomenclature',
                'id',
                'nomenclature_id'
            );
    }

    /**
     * Файлы у позиции
     * 
     * @return \App\Models\File
     */
    public function files()
    {
        return $this->hasMany(
                'App\Models\File',
                'object_id',
                'id'
            )
            ->where('object_type', 'claim_item');
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
              'nomenclature'
            ])
            ->find($id);
    }

    /**
     * Найти по id претензии и id изделия
     * 
     * @param int $orderId
     * @param int $nomenclatureId
     * @return mixed
     */
    public static function oneByClaimIdNomenclatureId($claimId, $nomenclatureId)
    {
        return self::where([
                'claim_id' => $claimId,
                'nomenclature_id' => $nomenclatureId
            ])
            ->first();
    }
}