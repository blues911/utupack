<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NomenclatureType extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'get' => [
            'nomenclature_type_id' => 'required'
        ],
        'create' => [
            'nomenclature_type_id' => 'required',
            'title' => 'required',
        ],
        'update' => [
            'nomenclature_type_id' => 'required',
            'title' => 'filled',
        ],
        'delete' => [
            'nomenclature_type_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'nomenclature_type';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * Товарная номенклатура
     * 
     * @return \App\Models\User
     */
    public function nomenclature()
    {
        return $this->belongsTo(
                'App\Models\UserNomenclature',
                'nomenclature_type_id'
            );
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::where('ext_id', $extId)->first();
    }

    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['ext_id'] = $this->ext_id;
        $data['title'] = $this->title;

        return $data;
    }
}
