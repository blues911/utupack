<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Claim extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Статусы
     * 
     * @var array
     */
    public static $statusTypes = [
        'new' => 'Новая',
        'review' => 'В работе',
        'solved' => 'Есть решение',
        'canceled' => 'Отменена'
    ];

    /**
     * Правила валидации
     * 
     * @var array
     */
    public static $rules = [
        'order_id' => 'required',
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'order_id.required' => 'Не выбрана «Заявка».',
    ];

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'get' => [
            'claim_id' => 'required'
        ],
        'update' => [
            'claim_id' => 'required',
            'order_id' => 'filled',
            'status' => 'filled|in:new,review,solved,canceled',
            'date' => 'filled',
            'product_code' => 'filled',
            'common_claim' => 'filled',
            'common_decision' => 'filled',
        ],
        'delete' => [
            'claim_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     * 
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'claim';

    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Заявка
     * 
     * @return \App\Models\Order
     */
    public function order()
    {
        return $this->hasOne(
                'App\Models\Order',
                'id',
                'order_id'
            );
    }

    /**
     * Позиции
     * 
     * @return \App\Models\ClaimItem
     */
    public function items()
    {
        return $this->hasMany(
                'App\Models\ClaimItem',
                'claim_id',
                'id'
            );
    }

    /**
     * Чат
     * 
     * @return \App\Models\Chat
     */
    public function chat()
    {
        return $this->hasMany(
                'App\Models\Chat',
                'object_id',
                'id'
            )
            ->where('object_type', 'claim')
            ->orderBy('date', 'desc');
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with([
                'order.client',
                'items.nomenclature',
                'items.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'chat.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->find($id);
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with([
                'order.client',
                'items.nomenclature',
                'items.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
                'chat.files' => function($query) {
                    $query->select('id', 'object_id', 'name', 'hash');
                },
            ])
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Список с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    public static function allForAccountType($filters)
    {
        $query = self::prepareForAccountType($filters);
        
        return $query
            ->skip($filters['offset'])
            ->take($filters['limit'])
            ->orderBy('date', 'desc')
            ->get();
    }

    /**
     * Кол-во с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    public static function countForAccountType($filters)
    {
        $query = self::prepareForAccountType($filters);
        
        return $query->count();
    }
    
    /**
     * Подготовительный запрос с учетом типа аккаунта пользователя (ЛК)
     * 
     * @param array $filters
     * @return mixed
     */
    protected static function prepareForAccountType($filters)
    {
        $query = self::with([
            'order.client',
            'items'
        ])
        ->withCount(['chat' => function($query) {
            if (user_account_type('admin'))
                $query->where('read_by_admin', 0);
            if (user_account_type('manager'))
                $query->where('read_by_manager', 0);
            if (user_account_type('client'))
                $query->where('read_by_client', 0);
        }]);

        // Определение аккаунта
        if (user_account_type('client'))
        {
            $query->whereHas('order', function($query) {
                $query->where('user_id', Auth::user()->id);
            });
        }

        if (user_account_type('manager'))
        {
            $query->whereHas('order', function($query) {
                $query->whereIn('user_id', function($query) {
                    $query->select('id')
                        ->from('user')
                        ->where('manager_id', Auth::user()->id);
                });
            });
        }

        // Фильтры
        if ($filters['date_range'] != '')
        {
            $row = explode(' ', str_replace('/\s(.*)\s/', ' ', $filters['date_range']));
            $query->whereBetween('date', [to_db_datetime($row[0]), to_db_datetime($row[2])]);
        }

        if ($filters['status'] != '')
            $query->where('status', $filters['status']);

        if ($filters['client'] != '')
        {
            $query->whereHas('order', function($query) use($filters) {
                $query->where('user_id', $filters['client']);
            });
        }

        return $query;
    }
    
    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['claim_id'] = $this->ext_id;
        $data['order_id'] = $this->order->ext_id;
        $data['common_claim'] = $this->common_claim;
        $data['common_decision'] = $this->common_decision;
        $data['status'] = $this->status;
        $data['date'] = $this->date;
        $data['product_code'] = $this->product_code;

        $data['items'] = [];

        if ($this->items()->count() > 0)
        {
            foreach ($this->items as $item)
            {
                $files = [];

                if ($item->files()->count() > 0)
                {
                    foreach ($item->files as $file)
                    {
                        $files[] = url('shared/read-file/' . $file->hash);
                    }
                }

                $data['items'][] = [
                    'nomenclature_id' => $item->nomenclature->ext_id,
                    'claim' => $item->claim,
                    'cnt' => $item->cnt,
                    'decision' => $item->decision,
                    'files' => json_encode($files),
                ];
            }
        }

        return $data;
    }
}