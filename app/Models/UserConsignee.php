<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserConsignee extends Model
{
    /**
     * Базовые поля даты
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Правила валидации
     * 
     * @var array
     */
    public static $rules = [
        'title' => 'required|consignee_unique_title',
        'inn' => 'required|consignee_unique_inn',
        'ogrn' => 'required|consignee_unique_ogrn',
    ];

    /**
     * Сообщения для правил валидации
     * 
     * @var array
     */
    public static $rulesMessages = [
        'title.required' => 'Не заполено «Наименование юридического лица».',
        'title.consignee_unique_title' => '«Наименование юридического лица» должно быть уникальным.',
        'inn.required' => 'Не заполен «ИНН».',
        'inn.consignee_unique_inn' => '«ИНН» должен быть уникальным.',
        'ogrn.required' => 'Не заполен «ОГРН/ОГРНИП».',
        'ogrn.consignee_unique_ogrn' => '«ОГРН/ОГРНИП» должен быть уникальным.'
    ];

    /**
     * Правила валидации api
     * 
     * @var array
     */
    public static $apiRules = [
        'create' => [
            'consignee_id' => 'required',
            'user_id' => 'required',
            'title' => 'required',
            'inn' => 'required',
            'ogrn' => 'required'
        ],
        'update' => [
            'consignee_id' => 'required',
            'user_id' => 'filled',
            'title' => 'filled',
            'inn' => 'filled',
            'ogrn' => 'filled'
        ],
        'delete' => [
            'consignee_id' => 'required'
        ],
        'delete_all' => [
            'user_id' => 'required'
        ],
    ];

    /**
     * Сообщения для правил валидации api
     *
     * @var array
     */
    public static $apiRulesMessages = [
        'required' => 'Поле обязательно для заполнения.',
        'filled' => 'Поле обязательно для заполнения.',
    ];

    /**
     * Название таблицы
     * 
     * @var string
     */
    protected $table = 'user_consignee';
    
    /**
     * Первичный ключ
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Клиент
     * 
     * @return \App\Models\User
     */
    public function client()
    {
        return $this->hasOne(
                'App\Models\User',
                'id',
                'user_id'
            );
    }

    /**
     * Найти по id
     * 
     * @param int $id
     * @return mixed
     */
    public static function oneById($id)
    {
        return self::with('client')->find($id);
    }

    /**
     * Найти по ext_id
     * 
     * @param string $extId
     * @return mixed
     */
    public static function oneByExtId($extId)
    {
        return self::with('client')
            ->where('ext_id', $extId)
            ->first();
    }

    /**
     * Список грузополучателей для клиента
     * 
     * @return mixed
     */
    public static function allForClient($userId)
    {
        return self::where('user_id', $userId)->get();
    }
    
    /**
     * Подготовка данных для внешнего api
     * 
     * @return array
     */
    public function serrializeForApi()
    {
        $data = [];

        $data['consignee_id'] = $this->ext_id;
        $data['user_id'] = $this->client->ext_id;
        $data['title'] = $this->title;
        $data['inn'] = $this->inn;
        $data['ogrn'] = $this->ogrn;

        return $data;
    }
}
