<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TmpUpdateDBData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmp:update_db_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update DB data (one-time script)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ins = 0;
        $upd = 0;

        $items = DB::table('test_2')->get();

        foreach ($items as $item)
        {
            $old = DB::table('test_1')
                ->where('ext_id', $item->ext_id)
                ->first();

            if (!empty($old))
            {
                DB::table('test_1')
                    ->where('ext_id', $old->ext_id)
                    ->update([
                        'title' => $item->title
                    ]);

                $upd += 1;
            }
            else
            {
                DB::table('test_1')->insert([
                    'ext_id' => $item->ext_id,
                    'title' => $item->title
                ]);

                $ins += 1;
            }
        }

        echo 'ins: ' . $ins . PHP_EOL;
        echo 'upd: ' . $upd . PHP_EOL;
    }
}
