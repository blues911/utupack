<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\Chat;
use App\Models\User;
use App\Mail\UnreadChatMail;

class CheckUnreadChat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat:check_unread';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check unread chat messages.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $managerIds = [];
        $usersWithManager = User::where('manager_id', '!=', null)
            ->groupBy('manager_id')
            ->get();

        foreach ($usersWithManager as $userWithManager)
        {
            $managerIds[] = $userWithManager->manager_id;
        }

        // Выбор пользователей типа client или id которых в manager_id
        $users = User::where('status', 1)
            ->where('email', '!=', null)
            ->where('account_type', 'client')
            ->orWhere(function($query) use($managerIds) {
                $query->where('status', 1)
                    ->where('email', '!=', null)
                    ->whereIn('id', $managerIds);
            })
            ->get();

        if ($users)
        {
            foreach ($users as $user)
            {
                $result = [];

                $ordersChat = Chat::getUnreadForMail('order', $user->id, $user->account_type);
                $samplesChat = Chat::getUnreadForMail('sample', $user->id, $user->account_type);
                $claimsChat = Chat::getUnreadForMail('claim', $user->id, $user->account_type);

                if ($ordersChat->isNotEmpty())
                {
                    $result['orders_chat'] = [];
                    foreach ($ordersChat as $orderChat)
                    {
                        $result['orders_chat'][$orderChat->object_id]['order_id'] = $orderChat->order->id;
                        $result['orders_chat'][$orderChat->object_id]['order_number'] = $orderChat->order->product_code;
                        $result['orders_chat'][$orderChat->object_id]['chat'][] = [
                            'message' => $orderChat->message,
                            'author' => $orderChat->user->name,
                            'date' => $orderChat->date
                        ];
                    }
                }

                if ($samplesChat->isNotEmpty())
                {
                    $result['samples_chat'] = [];
                    foreach ($samplesChat as $sampleChat)
                    {
                        $result['samples_chat'][$sampleChat->object_id]['sample_id'] = $sampleChat->sample->id;
                        $result['samples_chat'][$sampleChat->object_id]['sample_number'] = $sampleChat->sample->product_code;
                        $result['samples_chat'][$sampleChat->object_id]['chat'][] = [
                            'message' => $sampleChat->message,
                            'author' => $sampleChat->user->name,
                            'date' => $sampleChat->date
                        ];
                    }
                }

                if ($claimsChat->isNotEmpty())
                {
                    $result['claims_chat'] = [];
                    foreach ($claimsChat as $claimChat)
                    {
                        $result['claims_chat'][$claimChat->object_id]['claim_id'] = $claimChat->claim->id;
                        $result['claims_chat'][$claimChat->object_id]['claim_number'] = $claimChat->claim->product_code;
                        $result['claims_chat'][$claimChat->object_id]['chat'][] = [
                            'message' => $claimChat->message,
                            'author' => $claimChat->user->name,
                            'date' => $claimChat->date
                        ];
                    }
                }

                if (!empty($result)) Mail::to($user->email)->queue(new UnreadChatMail($result));
            }
        }
    }
}
