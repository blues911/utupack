<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Models\Order;
use App\Models\Sample;
use App\Models\Claim;
use App\Mail\NewOrderMail;
use App\Mail\NewSampleMail;
use App\Mail\NewClaimMail;

class TestMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:mail {email} {numbs}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test mail (order, sample, claim)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Example: php artisan test:mail test@mail.com 1

        $email = $this->argument('email');
        $numbs = explode(',', $this->argument('numbs'));

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            echo 'Invalid email format' . PHP_EOL;
            die;
        }

        if (in_array(1, $numbs))
        {
            $order = Order::first();

            if ($order)
            {
                $order = Order::oneById($order->id);
                Mail::to($email)->queue(new NewOrderMail($order));
            }
        }

        if (in_array(2, $numbs))
        {
            $sample = Sample::first();

            if ($sample)
            {
                $sample = Sample::oneById($sample->id);
                Mail::to($email)->queue(new NewSampleMail($sample));
            }
        }

        if (in_array(3, $numbs))
        {
            $claim = Claim::first();

            if ($claim)
            {
                $claim = Claim::oneById($claim->id);
                Mail::to($email)->queue(new NewClaimMail($claim));
            }
        }
    }
}
