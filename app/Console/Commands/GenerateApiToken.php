<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class GenerateApiToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:generate_token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate api token.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists($this->laravel->environmentFilePath()))
        {
            $token = Str::random(36);

            file_put_contents($this->laravel->environmentFilePath(), preg_replace(
                '/^INT_API_TOKEN(.*)/m',
                'INT_API_TOKEN='.$token,
                file_get_contents($this->laravel->environmentFilePath())
            ));

            echo "Api token set successfully.\n";
        }
        else
        {
            echo "Config file not found.\n";
        }

    }
}
