<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Chat;

class Menu
{
    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(){}

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // Меню в Личном Кабинете
        $menu[] = [
            'title' => 'Заявки',
            'path' => 'order/list',
            'icon' => 'orders',
            'chat_messages' => Chat::countUnreadForMenu('order')
        ];

        $menu[] = [
            'title' => 'Образцы',
            'path' => 'sample/list',
            'icon' => 'samples',
            'chat_messages' => Chat::countUnreadForMenu('sample')
        ];

        $menu[] = [
            'title' => 'Претензии',
            'path' => 'claim/list',
            'icon' => 'claims',
            'chat_messages' => Chat::countUnreadForMenu('claim')
        ];

        if (user_account_type('client'))
        {
            $menu[] = [
                'title' => 'Профиль',
                'path' => 'profile',
                'icon' => 'profile'
            ];

            $view->with('manager', auth()->user()->clientManager);
        }
        else if (user_account_type('manager'))
        {
            $view->with('manager', auth()->user());
        }

        $view->with('menu', $menu);
    }
}