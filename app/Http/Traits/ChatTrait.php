<?php

namespace App\Http\Traits;

use App\Models\Chat;

trait ChatTrait
{
    /**
     * Прочитать сообщения в чате.
     *
     * @param string $objectType
     * @param int $objectId
     * @return void
     */
    public function readChat($objectType, $objectId)
    {
        $chatItems = Chat::unreadForAccountType([
            'object_type' => $objectType,
            'object_id' => $objectId
        ]);

        if (!empty($chatItems))
        {
            foreach ($chatItems as $chatItem)
            {
                $chat = Chat::find($chatItem->id);

                if (user_account_type('admin'))
                    $chat->read_by_admin = 1;
                if (user_account_type('manager'))
                    $chat->read_by_manager = 1;
                if (user_account_type('client'))
                    $chat->read_by_client = 1;

                $chat->save();
            }
        }
    }
}