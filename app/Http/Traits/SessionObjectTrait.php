<?php

namespace App\Http\Traits;

/**
 * Интерфейс для запоминания объекта.
 */
trait SessionObjectTrait
{
    /**
     * Ключи сессии.
     * 
     * @var array
     */
    private $sessionKeys = [
        'order_client' => 'order_client_id',
        'sample_client' => 'sample_client_id',
        'claim_client' => 'claim_client_id',
        'order_draft' => 'order_draft_id',
        'sample_draft' => 'sample_draft_id',
    ];

    /**
     * Запомнить id объекта.
     * 
     * @param string $alias
     * @param int $id
     * @return void
     */
    public function putSessionObject($alias, $id)
    {
        session()->put($this->sessionKeys[$alias], $id);
    }

    /**
     * Удалить id объекта.
     *
     * @param string $alias
     * @return void
     */
    public function forgetSessionObject($alias)
    {
        if (session()->get($this->sessionKeys[$alias]))
        {
            session()->forget($this->sessionKeys[$alias]);
        }
    }

    /**
     * Получить id объекта.
     *
     * @param string $alias
     * @return mixed
     */
    public function getSessionObject($alias)
    {
        return session()->get($this->sessionKeys[$alias]);
    }
}