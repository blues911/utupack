<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\File;

class SharedController extends Controller
{
    /**
     * Скачать файл или открыть для просмотра.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param string $hash
     * @return resource
     */
    public function readFile(Request $request, $hash)
    {
        $f = File::oneByHash($hash);
        
        if ($request->query('open') == 1)
        {
            header('Content-Description: File Display');
            header('Content-Type: ' . $f->mime_type);
            header('Content-Length: ' . strlen($f->file));
            header('Expires: 0');
            header('Cache-Control: no-cache');
            header('Pragma: public');
            echo $f->file;
            exit;
        }
        else
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'. $f->name .'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($f->file));
            echo $f->file;
            exit;
        }
    }

    /**
     * Пользовательское соглашение.
     *
     * @return \Illuminate\Http\Response
     */
    public function termsOfuse()
    {
        return view('terms_of_use');
    }

    /**
     * Заглушка для api.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function fakeApi(Request $request)
    {
        // TODO: удалить после теста
        
        $resp = [];
        $method = $request->method;
        $params = $request->params;

        if (key($params) == 'order_id')
        {
            if (!$params['order_id'])
            {
                $resp['order_id'] = fake_ext_id();
                $resp['product_code'] = fake_ext_id();
            }
            else
            {
                $resp['order_id'] = $params['order_id'];
                $resp['product_code'] = $params['product_code'];
            }
        }
        else if (key($params) == 'sample_id')
        {
            if (!$params['sample_id'])
            {
                $resp['sample_id'] = fake_ext_id();
                $resp['product_code'] = fake_ext_id();
            }
            else
            {
                $resp['sample_id'] = $params['sample_id'];
                $resp['product_code'] = $params['product_code'];
            }
        }
        else if (key($params) == 'claim_id')
        {
            if (!$params['claim_id'])
            {
                $resp['claim_id'] = fake_ext_id();
                $resp['product_code'] = fake_ext_id();
            }
            else
            {
                $resp['claim_id'] = $params['claim_id'];
                $resp['product_code'] = $params['product_code'];
            }
        }
        else if (key($params) == 'consignee_id')
        {
            $resp['consignee_id'] = (!$params['consignee_id']) ? fake_ext_id() : $params['consignee_id'];
        }
        else if (key($params) == 'message_id')
        {
            $resp['message_id'] = (!$params['message_id']) ? fake_ext_id() : $params['message_id'];
        }

        return response()->json($resp, 200);
    }
}