<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\File;
use App\Jobs\ExtChatAddJob;

class ChatController extends Controller
{
    /**
     * Добавить сообщение.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxStore(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            Chat::$rules,
            Chat::$rulesMessages
        );
        
        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        $chat = new Chat();

        $chat->object_type = $request->object_type;
        $chat->object_id = $request->object_id;
        $chat->user_id = Auth::user()->id;
        $chat->message = $request->message;
        $chat->date = db_datetime();

        if (user_account_type('admin')) $chat->read_by_admin = 1;
        if (user_account_type('manager')) $chat->read_by_manager = 1;
        if (user_account_type('client')) $chat->read_by_client = 1;

        $chat->save();

        if ($request->hasFile('files'))
        {
            foreach ($request->file('files') as $requestFile)
            {
                $file = new File();

                $file->object_id = $chat->id;
                $file->object_type = 'chat';
                $file->file = file_get_contents($requestFile);
                $file->name = $requestFile->getClientOriginalName();
                $file->mime_type = $requestFile->getClientMimeType();
                $file->hash = random_hash(32);

                $file->save();
            }
        }

        ExtChatAddJob::dispatch($chat->id);

        return response()->json([
            'html' => view('_chat_messages', [
                'chat_items' => [Chat::oneById($chat->id)],
                'is_ajax' => true
            ])->render()
        ], 200);
    }

    /**
     * Получить новые непрочитанные сообщения.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxGetUnread(Request $request)
    {
        $params = $request->except('_token');
        $chatItems = Chat::unreadForAccountType($params);

        if (!empty($chatItems))
        {
            foreach ($chatItems as $chatItem)
            {
                $chat = Chat::find($chatItem->id);

                if (user_account_type('admin'))
                    $chat->read_by_admin = 1;
                if (user_account_type('manager'))
                    $chat->read_by_manager = 1;
                if (user_account_type('client'))
                    $chat->read_by_client = 1;

                $chat->save();
            }
        }

        return response()->json([
            'html' => view('_chat_messages', [
                'chat_items' => $chatItems,
                'is_ajax' => true
            ])->render()
        ], 200);

        return response()->json($chat, 200);
    }
}