<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Traits\SessionObjectTrait;
use App\Http\Traits\ChatTrait;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserAgreement;
use App\Models\UserConsignee;
use App\Models\UserNomenclature;
use App\Models\CardboardType;
use App\Models\CardboardProfile;
use App\Models\CardboardColor;
use App\Models\File;
use App\Jobs\ExtOrderCreateJob;
use App\Jobs\ExtOrderUpdateJob;
use App\Jobs\ExtOrderDeleteJob;

class OrderController extends Controller
{
    use SessionObjectTrait,
        ChatTrait;

    /**
     * Лимит на кол-во заявок в запросе.
     * 
     * @var int
     */
    protected $ordersLimit = 20;

    /**
     * Список заявок.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->forgetSessionObject('order_client');
        $this->forgetSessionObject('order_draft');

        $params = [];

        $params['clients'] = User::allClients();
        $params['status_types'] = Order::$statusTypes;
        $params['date_types'] = Order::$dateTypes;

        $params['filters'] = [];
        $params['filters']['offset'] = 0;
        $params['filters']['limit'] = $this->ordersLimit;
        $params['filters']['date_range'] = '';
        $params['filters']['status'] = '';
        $params['filters']['by_date']= '';
        $params['filters']['client']= '';

        if (!empty($request->query()))
        {
            $params['filters']['date_range'] = $request->query('date_range');
            $params['filters']['status'] = $request->query('status');
            $params['filters']['by_date']= $request->query('by_date');
            
            if (user_account_type(['manager', 'admin']))
                $params['filters']['client']= $request->query('client');
        }

        $orders = Order::allForAccountType($params['filters']);
        $ordersTotal = Order::countForAccountType($params['filters']);
        
        return view('order.index', [
            'params' => $params,
            'orders' => $orders,
            'orders_total' => $ordersTotal
        ]);
    }

    /**
     * Форма создания заявки.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = Auth::user();

        if (user_account_type(['manager', 'admin']))
            $client = User::findOrFail($this->getSessionObject('order_client'));

        $params = [];

        $params['client'] = $client;
        $params['status_types'] = Order::$statusTypes;
        $params['buyers'] = UserAgreement::allBuyersForClient($client->id);
        $params['consignees'] = UserConsignee::allForClient($client->id);
        $params['addresses'] = OrderItem::allAddressesForClient($client->id);
        $params['cardboard_types'] = CardboardType::all();
        $params['cardboard_profiles'] = CardboardProfile::all();
        $params['cardboard_colors'] = CardboardColor::all();
        $params['is_order_draft'] = false;
        $params['view_mode'] = false;

        $order = false;

        if ($this->getSessionObject('order_draft'))
        {
            $order = Order::oneById($this->getSessionObject('order_draft'));

            if (!is_null($order->desired_date))
                $order->desired_date = to_calendar_date($order->desired_date);

            if (!is_null($order->ready_date))
                $order->ready_date = to_calendar_date($order->ready_date);

            $params['is_order_draft'] = true;
        }

        return view('order.form', [
            'params' => $params,
            'order' => $order
        ]);
    }

    /**
     * Создать заявку.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxStore(Request $request)
    {
        $order = new Order();

        $order->user_id = $request->user_id;
        $order->buyer_ext_id = $request->buyer_ext_id;
        $order->consignee_id = $request->consignee_id;
        $order->agreement_id = $request->agreement_id;
        $order->request_number = ($request->request_number) ?: null;
        $order->delivery_type = $request->delivery_type;

        if ($request->has('desired_date') && $request->filled('desired_date'))
            $order->desired_date = to_db_datetime($request->desired_date);
        else
            $order->desired_date = null;

        $order->comment = $request->comment ?: null;

        if ($request->has('status'))
            $order->status = $request->status;

        $order->date = db_datetime();

        $order->save();

        $items = json_decode($request->items, true);

        foreach ($items as $item)
        {
            $orderItem = new OrderItem();

            $orderItem->order_id = $order->id;
            $orderItem->nomenclature_id = $item['nomenclature_id'];
            $orderItem->address = $item['address'] ?: null;
            $orderItem->address_number = $item['address_number'] ?: null;
            $orderItem->cnt = $item['cnt'] ?: 0;
            $orderItem->cnt_rule = $item['cnt_rule'] ?: 0;
            $orderItem->comment = $item['comment'] ?: null;

            $orderItem->save();
        }

        if ($request->hasFile('file'))
        {
            $orderFile = $request->file('file');

            $file = new File();

            $file->object_id = $order->id;
            $file->object_type = 'order';
            $file->file = file_get_contents($orderFile);
            $file->name = $orderFile->getClientOriginalName();
            $file->mime_type = $orderFile->getClientMimeType();
            $file->hash = random_hash(32);

            $file->save();
        }

        ExtOrderCreateJob::dispatch($order->id);

        $this->forgetSessionObject('order_client');
        $this->forgetSessionObject('order_draft');

        return response()->json([
            'id' => $order->id
        ], 200);
    }

    /**
     * Форма редактирования заявки.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::oneById($id);

        if (!$order
            || $order && user_account_type('client') && $order->user_id != auth()->user()->id
            || $order && user_account_type('manager') && $order->client->manager_id != auth()->user()->id
            || $order && $order->status != 'new' && user_account_type('client'))
        {
            abort(404);
        }
            
        if ($order->ext_id == null)
            return redirect('order/show/' . $order->id);

        if (!is_null($order->desired_date))
            $order->desired_date = to_calendar_date($order->desired_date);

        if (!is_null($order->ready_date))
            $order->ready_date = to_calendar_date($order->ready_date);

        $this->readChat('order', $id);
        
        $params = [];

        $params['client'] = $order->client;
        $params['status_types'] = Order::$statusTypes;
        $params['buyers'] = UserAgreement::allBuyersForClient($order->user_id);
        $params['consignees'] = UserConsignee::allForClient($order->user_id);
        $params['addresses'] = OrderItem::allAddressesForClient($order->user_id);
        $params['cardboard_types'] = CardboardType::all();
        $params['cardboard_profiles'] = CardboardProfile::all();
        $params['cardboard_colors'] = CardboardColor::all();
        $params['is_order_draft'] = false;
        $params['view_mode'] = false;

        return view('order.form', [
            'params' => $params,
            'order' => $order
        ]);
    }

    /**
     * Обновить заявку.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function ajaxUpdate(Request $request, $id)
    {
        $order = Order::find($id);

        $order->buyer_ext_id = $request->buyer_ext_id;
        $order->consignee_id = $request->consignee_id;
        $order->agreement_id = $request->agreement_id;
        $order->request_number = $request->request_number ?: null;

        if ($request->has('desired_date') && $request->filled('desired_date'))
            $order->desired_date = to_db_datetime($request->desired_date);
        else
            $order->desired_date = null;

        $order->comment = $request->comment ?: null;

        if ($request->has('status'))
            $order->status = $request->status;

        $order->save();

        $items = json_decode($request->items, true);

        foreach ($items as $item)
        {
            $orderItem = OrderItem::find($item['id']);

            if ($orderItem)
            {
                $orderItem->order_id = $order->id;
                $orderItem->nomenclature_id = $item['nomenclature_id'];
                $orderItem->address = $item['address'] ?: null;
                $orderItem->address_number = $item['address_number'] ?: null;
                $orderItem->cnt = $item['cnt'] ?: 0;
                $orderItem->cnt_rule = $item['cnt_rule'] ?: 0;
                $orderItem->comment = $item['comment'] ?: null;
            }
            else
            {
                $orderItem = new OrderItem();

                $orderItem->order_id = $order->id;
                $orderItem->nomenclature_id = $item['nomenclature_id'];
                $orderItem->address = $item['address'] ?: null;
                $orderItem->address_number = $item['address_number'] ?: null;
                $orderItem->cnt = $item['cnt'] ?: 0;
                $orderItem->cnt_rule = $item['cnt_rule'] ?: 0;
                $orderItem->comment = $item['comment'] ?: null;
            }

            $orderItem->save();
        }

        $itemsDeletedIds = json_decode($request->items_deleted, true);

        if (!empty($itemsDeletedIds))
        {
            foreach ($itemsDeletedIds as $itemDeletedId)
            {
                $orderItem = OrderItem::oneById($itemDeletedId);
                $orderItem->delete();
            }
        }

        if ($request->has('delete_file') && $request->filled('delete_file'))
        {
            $file = File::oneByHash($request->delete_file);

            if ($file) $file->delete();
        }

        if ($request->hasFile('file'))
        {
            $orderFile = $request->file('file');

            $file = File::oneByObjectData('order', $order->id);

            if ($file) $file->delete();

            $file = new File();

            $file->object_id = $order->id;
            $file->object_type = 'order';
            $file->file = file_get_contents($orderFile);
            $file->name = $orderFile->getClientOriginalName();
            $file->mime_type = $orderFile->getClientMimeType();
            $file->hash = random_hash(32);
            
            $file->save();
        }

        ExtOrderUpdateJob::dispatch($order->id);

        return response()->json([
            'id' => $order->id
        ], 200);
    }

    /**
     * Просмотр заявки.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::oneById($id);

        if (!$order
            || $order && user_account_type('client') && $order->user_id != auth()->user()->id
            || $order && user_account_type('manager') && $order->client->manager_id != auth()->user()->id)
        {
            abort(404);
        }

        if (!is_null($order->desired_date))
            $order->desired_date = to_calendar_date($order->desired_date);

        if (!is_null($order->ready_date))
            $order->ready_date = to_calendar_date($order->ready_date);

        $this->readChat('order', $id);

        $params = [];

        $params['client'] = $order->client;
        $params['status_types'] = Order::$statusTypes;
        $params['buyers'] = UserAgreement::allBuyersForClient($order->user_id);
        $params['consignees'] = UserConsignee::allForClient($order->user_id);
        $params['addresses'] = OrderItem::allAddressesForClient($order->user_id);
        $params['cardboard_types'] = CardboardType::all();
        $params['cardboard_profiles'] = CardboardProfile::all();
        $params['cardboard_colors'] = CardboardColor::all();
        $params['is_order_draft'] = false;
        $params['view_mode'] = true;

        return view('order.form', [
            'params' => $params,
            'order' => $order
        ]);
    }

    /**
     * Копировать заявку.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function copy(Request $request, $id)
    {
        $order = Order::oneById($id);

        if (user_account_type(['manager', 'admin']))
            $this->putSessionObject('order_client', $order->user_id);
        
        $this->putSessionObject('order_draft', $order->id);

        return redirect('order/create');
    }

    /**
     * Удалить заявку.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $order = Order::oneById($id);

        foreach ($order->chat as $orderChat)
        {
            $orderChat->files()->delete();
            $orderChat->delete();
        }
        
        $order->items()->delete();
        $order->file()->delete();

        ExtOrderDeleteJob::dispatch($order->id);

        $order->delete();

        return redirect('order/list');
    }

    /**
     * Запомнить user_id для новой заявки.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxRememberClient(Request $request)
    {
        $params = $request->except('_token');
        $this->putSessionObject('order_client', $params['client_id']);
 
        return response()->json([], 200);
    }

    /**
     * Список товарной номенклатры для заявки.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxFilterNomenclatures(Request $request)
    {
        $params = $request->except('_token');
        $result = UserNomenclature::filterForClient($params);

        return response()->json($result, 200);
    }

    /**
     * Валидация заявки.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxValidateOrder(Request $request)
    {
        $result = [];

        $validator = Validator::make(
            $request->all(),
            Order::$rules,
            Order::$rulesMessages
        );
        
        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        return response()->json([], 200);
    }

    /**
     * Валидация позиции в модальном окне.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxValidateOrderItem(Request $request)
    {
        $result = [];

        $validator = Validator::make(
            $request->all(),
            OrderItem::$rules,
            OrderItem::$rulesMessages
        );

        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        return response()->json([], 200);
    }

    /**
     * Подгрузка списка заявок.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxLoadMore(Request $request)
    {
        $params = $request->except('_token');
        $params['limit'] = $this->ordersLimit;

        $orders = Order::allForAccountType($params);
        $ordersTotal = Order::countForAccountType($params);

        return response()->json([
            'html' => view('order.inc._order_items', [
                'orders' => $orders
            ])->render(),
            'orders_total' => $ordersTotal
        ], 200);
    }
}