<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Traits\SessionObjectTrait;
use App\Http\Traits\ChatTrait;
use App\Models\User;
use App\Models\Sample;
use App\Models\UserAgreement;
use App\Models\UserConsignee;
use App\Models\NomenclatureType;
use App\Models\CardboardType;
use App\Models\CardboardProfile;
use App\Models\CardboardColor;
use App\Models\File;
use App\Jobs\ExtSampleCreateJob;
use App\Jobs\ExtSampleUpdateJob;
use App\Jobs\ExtSampleDeleteJob;
use App\Mail\NewSampleMail;

class SampleController extends Controller
{
    use SessionObjectTrait,
        ChatTrait;

    /**
     * Лимит на кол-во заявок в запросе.
     * 
     * @var int
     */
    protected $samplesLimit = 20;

    /**
     * Список образцов.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->forgetSessionObject('sample_client');
        $this->forgetSessionObject('sample_draft');

        $params = [];
        $params['clients'] = User::allClients();
        $params['status_types'] = Sample::$statusTypes;

        $params['filters'] = [];
        $params['filters']['offset'] = 0;
        $params['filters']['limit'] = $this->samplesLimit;
        $params['filters']['date_range'] = '';
        $params['filters']['status'] = '';
        $params['filters']['client']= '';

        if (!empty($request->query()))
        {
            $params['filters']['date_range'] = $request->query('date_range');
            $params['filters']['status'] = $request->query('status');

            if (user_account_type(['manager', 'admin']))
                $params['filters']['client']= $request->query('client');
        }

        $samples = Sample::allForAccountType($params['filters']);
        $samplesTotal = Sample::countForAccountType($params['filters']);

        return view('sample.index', [
            'params' => $params,
            'samples' => $samples,
            'samples_total' => $samplesTotal
        ]);
    }

    /**
     * Форма создания образца.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = Auth::user();

        if (user_account_type(['manager', 'admin']))
            $client = User::findOrFail($this->getSessionObject('sample_client'));

        $params = [];
        $params['client'] = $client;
        $params['status_types'] = Sample::$statusTypes;
        $params['delivery_types'] = Sample::$deliveryTypes;
        $params['making_types'] = Sample::$makingTypes;
        $params['buyers'] = UserAgreement::allBuyersForClient($client->id);
        $params['consignees'] = UserConsignee::allForClient($client->id);
        $params['nomenclature_types'] = NomenclatureType::all();
        $params['cardboard_types'] = CardboardType::all();
        $params['cardboard_profiles'] = CardboardProfile::all();
        $params['cardboard_colors'] = CardboardColor::all();
        $params['is_sample_draft'] = false;
        $params['view_mode'] = false;

        $sample = false;

        if ($this->getSessionObject('sample_draft'))
        {
            $sample = Sample::oneById($this->getSessionObject('sample_draft'));
            $sample->sizes = json_decode($sample->sizes, true);

            $params['is_sample_draft'] = true;
        }

        return view('sample.form', [
            'params' => $params,
            'sample' => $sample
        ]);
    }

    /**
     * Создать образец.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxStore(Request $request)
    {
        $sample = new Sample();

        $sample->user_id = $request->user_id;
        $sample->buyer_ext_id = $request->buyer_ext_id;
        $sample->consignee_id = $request->consignee_id;
        $sample->agreement_id = $request->agreement_id;
        $sample->request_number = $request->request_number;
        $sample->delivery_type = $request->delivery_type;
        $sample->making = $request->making;

        $sizes = [];
        $sizes['length'] = $request->sizes['length'];
        $sizes['width'] = $request->sizes['width'];
        if (!empty($request->sizes['height'])) $sizes['height'] = $request->sizes['height'];
        $sample->sizes = json_encode($sizes);

        $sample->cnt = $request->cnt ?: 0;
        $sample->comment = $request->comment ?: '';

        if ($request->has('status'))
            $sample->status = $request->status;
        
        $sample->date = db_datetime();
        $sample->nomenclature_type_id = $request->nomenclature_type_id;
        $sample->cardboard_type_id = $request->cardboard_type_id;
        $sample->cardboard_profile_id = $request->cardboard_profile_id;
        $sample->cardboard_color_id = $request->cardboard_color_id;

        $sample->save();

        if ($request->hasFile('file'))
        {
            $sampleFile = $request->file('file');

            $file = new File();

            $file->object_id = $sample->id;
            $file->object_type = 'sample';
            $file->file = file_get_contents($sampleFile);
            $file->name = $sampleFile->getClientOriginalName();
            $file->mime_type = $sampleFile->getClientMimeType();
            $file->hash = random_hash(32);

            $file->save();
        }

        ExtSampleCreateJob::dispatch($sample->id);

        $this->forgetSessionObject('sample_client');
        $this->forgetSessionObject('sample_draft');
        
        return response()->json([
            'id' => $sample->id
        ], 200);
    }

    /**
     * Форма редактирования образца.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sample = Sample::oneById($id);

        if (!$sample
            || $sample && user_account_type('client') && $sample->user_id != auth()->user()->id
            || $sample && user_account_type('manager') && $sample->client->manager_id != auth()->user()->id
            || $sample && $sample->status != 'new' && user_account_type('client'))
        {
            abort(404);
        }
            
        if ($sample->ext_id == null)
            return redirect('sample/show/' . $sample->id);

        $sample->sizes = json_decode($sample->sizes, true);

        $this->readChat('sample', $id);
        
        $params = [];

        $params['client'] = $sample->client;
        $params['status_types'] = Sample::$statusTypes;
        $params['delivery_types'] = Sample::$deliveryTypes;
        $params['making_types'] = Sample::$makingTypes;
        $params['buyers'] = UserAgreement::allBuyersForClient($sample->user_id);
        $params['consignees'] = UserConsignee::allForClient($sample->user_id);
        $params['nomenclature_types'] = NomenclatureType::all();
        $params['cardboard_types'] = CardboardType::all();
        $params['cardboard_profiles'] = CardboardProfile::all();
        $params['cardboard_colors'] = CardboardColor::all();
        $params['is_sample_draft'] = false;
        $params['view_mode'] = false;

        return view('sample.form', [
            'params' => $params,
            'sample' => $sample
        ]);
    }

    /**
     * Обновить образец.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function ajaxUpdate(Request $request, $id)
    {
        $sample = Sample::oneById($id);

        $sample->buyer_ext_id = $request->buyer_ext_id;
        $sample->consignee_id = $request->consignee_id;
        $sample->agreement_id = $request->agreement_id;
        $sample->request_number = $request->request_number;
        $sample->delivery_type = $request->delivery_type;
        $sample->making = $request->making;

        $sizes = [];
        $sizes['length'] = $request->sizes['length'];
        $sizes['width'] = $request->sizes['width'];
        if (!empty($request->sizes['height'])) $sizes['height'] = $request->sizes['height'];
        $sample->sizes = json_encode($sizes);

        $sample->cnt = $request->cnt ?: 0;
        $sample->comment = $request->comment ?: '';

        if ($request->has('status'))
            $sample->status = $request->status;

        $sample->nomenclature_type_id = $request->nomenclature_type_id;
        $sample->cardboard_type_id = $request->cardboard_type_id;
        $sample->cardboard_profile_id = $request->cardboard_profile_id;
        $sample->cardboard_color_id = $request->cardboard_color_id;

        $sample->save();

        if ($request->has('delete_file') && $request->filled('delete_file'))
        {
            $file = File::oneByHash($request->delete_file);

            if ($file) $file->delete();
        }

        if ($request->hasFile('file'))
        {
            $sampleFile = $request->file('file');

            $file = File::oneByObjectData('sample', $sample->id);

            if ($file) $file->delete();

            $file = new File();

            $file->object_id = $sample->id;
            $file->object_type = 'sample';
            $file->file = file_get_contents($sampleFile);
            $file->name = $sampleFile->getClientOriginalName();
            $file->mime_type = $sampleFile->getClientMimeType();
            $file->hash = random_hash(32);
            
            $file->save();
        }

        // TODO: открыть когда будет готова сторона 1С
        // ExtSampleUpdateJob::dispatch($sample->id);

        return response()->json([
            'id' => $sample->id
        ], 200);
    }

    /**
     * Просмотр претензии.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sample = Sample::oneById($id);

        if (!$sample
            || $sample && user_account_type('client') && $sample->user_id != auth()->user()->id
            || $sample && user_account_type('manager') && $sample->client->manager_id != auth()->user()->id)
        {
            abort(404);
        }

        $sample->sizes = json_decode($sample->sizes, true);

        $this->readChat('sample', $id);

        $params = [];

        $params['client'] = $sample->client;
        $params['status_types'] = Sample::$statusTypes;
        $params['delivery_types'] = Sample::$deliveryTypes;
        $params['making_types'] = Sample::$makingTypes;
        $params['buyers'] = UserAgreement::allBuyersForClient($sample->user_id);
        $params['consignees'] = UserConsignee::allForClient($sample->user_id);
        $params['nomenclature_types'] = NomenclatureType::all();
        $params['cardboard_types'] = CardboardType::all();
        $params['cardboard_profiles'] = CardboardProfile::all();
        $params['cardboard_colors'] = CardboardColor::all();
        $params['is_sample_draft'] = false;
        $params['view_mode'] = true;

        return view('sample.form', [
            'params' => $params,
            'sample' => $sample
        ]);
    }

    /**
     * Копировать образец.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function copy(Request $request, $id)
    {
        $sample = Sample::oneById($id);

        if (user_account_type(['manager', 'admin']))
            $this->putSessionObject('sample_client', $sample->user_id);
        
        $this->putSessionObject('sample_draft', $sample->id);

        return redirect('sample/create');
    }

    /**
     * Удалить образец.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $sample = Sample::oneById($id);
        
        foreach ($sample->chat as $sampleChat)
        {
            $sampleChat->files()->delete();
            $sampleChat->delete();
        }
        
        $sample->file()->delete();

        // TODO: открыть когда будет готова сторона 1С
        // ExtSampleDeleteJob::dispatch($sample->id);

        $sample->delete();

        return redirect('sample/list');
    }

    /**
     * Запомнить user_id для новой заявки.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxRememberClient(Request $request)
    {
        $params = $request->except('_token');
        $this->putSessionObject('sample_client', $params['client_id']);
 
        return response()->json([], 200);
    }

    /**
     * Валидация образца.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxValidateSample(Request $request)
    {
        $result = [];

        $validator = Validator::make(
            $request->all(),
            Sample::$rules,
            Sample::$rulesMessages
        );
        
        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        return response()->json([], 200);
    }

    /**
     * Подгрузка списка образцов.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxLoadMore(Request $request)
    {
        $params = $request->except('_token');
        $params['limit'] = $this->samplesLimit;

        $samples = Sample::allForAccountType($params);
        $samplesTotal = Sample::countForAccountType($params);

        return response()->json([
            'html' => view('sample.inc._sample_items', [
                'samples' => $samples
            ])->render(),
            'samples_total' => $samplesTotal
        ], 200);
    }
}