<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\UserAgreement;
use App\Models\UserConsignee;
use App\Models\UserNomenclature;
use App\Jobs\ExtUserConsigneeCreateJob;
use App\Jobs\ExtUserConsigneeUpdateJob;
use App\Jobs\ExtUserConsigneeDeleteJob;

class ProfileController extends Controller
{
    /**
     * Информация профиля
     * 
     * @return \Illuminate\Http\Response
     */
    public function info()
    {
        $nav = 'info';

        return view('profile.info', [
            'nav' => $nav,
            'info' => auth()->user()
        ]);
    }
    /**
     * Юридические лица и договора
     * 
     * @return \Illuminate\Http\Response
     */
    public function agreements()
    {
        $nav = 'agreements';

        return view('profile.agreements', [
            'nav' => $nav,
            'agreements' => UserAgreement::where('user_id', Auth::user()->id)
                ->orderBy('status', 'desc')
                ->get()
        ]);
    }

    /**
     * Грузополучатели
     * 
     * @return \Illuminate\Http\Response
     */
    public function consignees()
    {
        $nav = 'consignees';

        return view('profile.consignees', [
            'nav' => $nav,
            'consignees' => UserConsignee::where('user_id', Auth::user()->id)->get()
        ]);
    }

    /**
     * Товарная номенклатура
     * 
     * @return \Illuminate\Http\Response
     */
    public function nomenclatures()
    {
        $nav = 'nomenclatures';

        return view('profile.nomenclatures', [
            'nav' => $nav,
            'nomenclatures' => UserNomenclature::where('user_id', Auth::user()->id)
                ->orderBy('status', 'desc')
                ->get()
        ]);
    }

    public function ajaxValidateConsignee(Request $request)
    {
        $validator = Validator::make(
            $request->except(['_token', 'action']),
            UserConsignee::$rules,
            UserConsignee::$rulesMessages
        );
        
        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->getMessages()
            ], 422);
        }

        return response()->json([], 200);
    }

    /**
     * CRUD грузополучателя
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxHandleConsignee(Request $request)
    {
        $result = [];
        $params = $request->except('_token');

        switch ($params['action'])
        {
            case 'store':

                $consignee = new UserConsignee();

                $consignee->user_id = Auth::user()->id;
                $consignee->title = $params['title'];
                $consignee->inn = $params['inn'];
                $consignee->ogrn = $params['ogrn'];

                $consignee->save();

                $result = $consignee;

                ExtUserConsigneeCreateJob::dispatch($consignee->id);
                
                break;

            case 'update':

                $consignee = UserConsignee::find($params['id']);

                $consignee->user_id = Auth::user()->id;
                $consignee->title = $params['title'];
                $consignee->inn = $params['inn'];
                $consignee->ogrn = $params['ogrn'];

                $consignee->save();

                $result = $consignee;

                ExtUserConsigneeUpdateJob::dispatch($consignee->id);

                break;

            case 'delete':
            
                $consignee = UserConsignee::find($params['id']);

                ExtUserConsigneeDeleteJob::dispatch($consignee->id);

                $consignee->delete();

                break;
        }

        return response()->json($result, 200);
    }

    /**
     * Проверить ext_id грузополучателя
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxCheckConsigneeExtId(Request $request)
    {
        $result = [];
        $params = $request->except('_token');

        $consignee = UserConsignee::find($params['consignee_id']);
        $result['ext_id'] = $consignee->ext_id;

        return response()->json($result, 200);
    }
}