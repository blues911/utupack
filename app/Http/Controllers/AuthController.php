<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Controller;
use App\Models\User;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Перенаправить после авторизации
     */
    protected $redirectTo = '/order/list';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Метод трейта \Illuminate\Foundation\Auth\AuthenticatesUsers
     * 
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('login');
    }

    /**
     * Метод трейта \Illuminate\Foundation\Auth\AuthenticatesUsers
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string|user_is_active',
            'password' => 'required|string',
        ]);
    }

    /**
     * Метод трейта \Illuminate\Foundation\Auth\AuthenticatesUsers
     * 
     * @return string
     */
    public function username()
    {
        return 'login';
    }
}