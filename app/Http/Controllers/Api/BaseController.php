<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ApiLog;

class BaseController extends Controller
{
    /**
     * Url адрес для обращения.
     * 
     * @var string
     */
    protected $logUrl = '';

    /**
     * Схема кодов с ошибками.
     * 100 Указаны не все обязательные входные параметры (ошибка схемы данных)
     * 101 Объект не найден
     * 102 Не удалось добавить объект
     * 103 Не удалось удалить объект
     * 104 Не удалось обновить объект
     * 199 Неизвестная ошибка
     * 
     * @var array
     */
    protected $errors = [
        'schema_error' => [
            'error_code' => 100,
            'error_string' => []
        ],
        'not_found' => [
            'error_code' => 101,
            'error_string' => []
        ],
        'not_created' => [
            'error_code' => 102,
            'error_string' => []
        ],
        'not_deleted' => [
            'error_code' => 103,
            'error_string' => []
        ],
        'not_updated' => [
            'error_code' => 104,
            'error_string' => []
        ],
        'unknown_error' => [
            'error_code' => 199,
            'error_string' => []
        ],
    ];

    /**
     * Ошибки для error_string.
     * 
     * @var array
     */
    protected $errorsMessages = [
        'exist' => 'Объект уже добавлен',
        'not_found' => 'Объект не найден',
        'not_created' => 'Не удалось добавить объект',
        'not_deleted' => 'Не удалось удалить объект',
        'not_updated' => 'Не удалось обновить объект',
        'unknown_error' => 'Неизвестная ошибка',
    ];

    public function __construct()
    {
        $this->middleware(function($request, $next) {

            if ($request->header('Authorization') != env('INT_API_TOKEN'))
            {
                return response('Unauthorized.', 401);
            }

            if ($request->header('Content-Type') != 'application/json')
            {
                return response('415 Unsupported Media Type.', 415);
            }

            return $next($request);
        });

        $this->logUrl = env('INT_API_URL');
    }

    /**
     * Обработчик методов и параметров запроса
     * 
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function handle(Request $request)
    {
        $response = null;
        $exception = null;
        
        $timeStart = microtime(true);

        try
        {
            if (!$request->filled(['method', 'params']))
            {
                throw new \Exception('Invalid request params');
            }

            $method = $request->method;
            $params = $request->params;

            if (!method_exists($this, $method))
            {
                throw new \Exception('Class method not exists');
            }

            $response = $this->{$method}($params);
        }
        catch (\Exception $error)
        {
            $exception = $error;
        }

        $timeEnd = microtime(true);
        $timeElapsed = $timeEnd - $timeStart;

        ApiLog::create([
            'type' => 'in',
            'url' => $this->logUrl,
            'request' => $request->getContent(),
            'response' => ($response != null) ? json_encode($response, JSON_UNESCAPED_UNICODE) : null,
            'exception' => $exception,
            'time_elapsed' => $timeElapsed,
            'date' => db_datetime()
        ]);

        if ($exception)
        {
            $this->errors['unknown_error']['error_string'] = $this->errorsMessages['unknown_error'];
            return $this->errors['unknown_error'];
        }

        return response()->json($response, 200); 
    }
}