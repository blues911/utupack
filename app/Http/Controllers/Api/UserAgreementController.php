<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use App\Models\UserAgreement;

class UserAgreementController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/agreements';
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createUserAgreement($params[$k]);
            }
        }
        else
        {
            $result = $this->createUserAgreement($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateUserAgreement($params[$k]);
            }
        }
        else
        {
            $result = $this->updateUserAgreement($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteUserAgreement($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteUserAgreement($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Создание всех договоров пользователя.
     * 
     * @param array $params
     * @return array
     */
    public function delete_all($params)
    {
        $validator = Validator::make(
            $params,
            UserAgreement::$apiRules['delete_all'],
            UserAgreement::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }
        
        $user = User::oneByExtId($params['user_id']);  
        
        if (!$user || $user->clientAgreements()->count() == 0)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $user->clientAgreements()->delete();

        return [
            'user_id' => $params['user_id']
        ];
    }

    /**
     * Создание договора.
     * 
     * @param array $params
     * @return array
     */
    private function createUserAgreement($params)
    {
        $validator = Validator::make(
            $params,
            UserAgreement::$apiRules['create'],
            UserAgreement::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userAgreement = UserAgreement::oneByExtId($params['agreement_id']);

        if ($userAgreement)
        {
            $this->errors['not_created']['error_string']['agreement_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $user = User::oneByExtId($params['user_id']);

        if (!$user)
        {
            $this->errors['not_created']['error_string']['user_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_created'];
        }

        $userAgreement = new UserAgreement();

        $userAgreement->ext_id = $params['agreement_id'];
        $userAgreement->user_id = $user->id;
        $userAgreement->buyer_ext_id = $params['buyer_id'];
        $userAgreement->buyer_title = $params['buyer_title'];
        $userAgreement->number = $params['number'];
        $userAgreement->date = to_db_datetime($params['date']);
        $userAgreement->status = ((int)$params['status'] == 1) ? 1 : 0;

        if (array_key_exists('min_execution', $params)) $userAgreement->min_execution = $params['min_execution'] ?: 0;
        if (array_key_exists('info', $params)) $userAgreement->info = $params['info'] ?: null;

        $userAgreementId = $userAgreement->save();

        return [
            'agreement_id' => $params['agreement_id']
        ];
    }

    /**
     * Обновление договора.
     * 
     * @param array $params
     * @return array
     */
    private function updateUserAgreement($params)
    {
        $validator = Validator::make(
            $params,
            UserAgreement::$apiRules['update'],
            UserAgreement::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userAgreement = UserAgreement::oneByExtId($params['agreement_id']);
        
        if (!$userAgreement)
        {
            $this->errors['not_updated']['error_string']['agreement_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('user_id', $params))
        {
            $user = User::oneByExtId($params['user_id']);

            if (!$user)
            {
                $this->errors['not_updated']['error_string']['user_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $userAgreement->user_id = $user->id;
        } 

        if (array_key_exists('buyer_id', $params)) $userAgreement->buyer_ext_id = $params['buyer_id'];
        if (array_key_exists('buyer_title', $params)) $userAgreement->buyer_title = $params['buyer_title'];
        if (array_key_exists('number', $params)) $userAgreement->number = $params['number'];
        if (array_key_exists('date', $params)) $userAgreement->date = to_db_datetime($params['date']);
        if (array_key_exists('min_execution', $params)) $userAgreement->min_execution = $params['min_execution'] ?: 0;
        if (array_key_exists('info', $params)) $userAgreement->info = $params['info'] ?: null;
        if (array_key_exists('status', $params)) $userAgreement->status = ((int)$params['status'] == 1) ? 1 : 0;


        $userAgreement->save();

        return [
            'agreement_id' => $params['agreement_id']
        ];
    }

    /**
     * Удаление договора.
     * 
     * @param array $params
     * @return array
     */
    private function deleteUserAgreement($params)
    {
        $validator = Validator::make(
            $params,
            UserAgreement::$apiRules['delete'],
            UserAgreement::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userAgreement = UserAgreement::oneByExtId($params['agreement_id']);

        if (!$userAgreement)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $userAgreement->delete();

        return [
            'agreement_id' => $params['agreement_id']
        ];
    }
}