<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\Chat;
use App\Models\User;
use App\Models\Order;
use App\Models\Sample;
use App\Models\Claim;
use App\Models\File;
use App\Jobs\ExtChatAddJob;

class ChatController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/chats';
    }

    /**
     * Метод запроса.
     * Добавление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function add($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->addChat($params[$k]);
            }
        }
        else
        {
            $result = $this->addChat($params);
        }

        return $result;
    }

    /**
     * Добавление сообщения.
     * 
     * @param array $params
     * @return array
     */
    private function addChat($params)
    {
        $validator = Validator::make(
            $params,
            Chat::$apiRules['add'],
            Chat::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $chat = Chat::oneByExtId($params['message_id']);

        if ($chat)
        {
            $this->errors['not_created']['error_string']['message_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }


        $chat = new Chat();

        $chat->ext_id = $params['message_id'];

        if ($params['object_type'] == 'order')
        {
            $order = Order::oneByExtId($params['object_id']);

            if (!$order)
            {
                $this->errors['not_created']['error_string']['object_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_created'];
            }
            
            $chat->object_type = 'order';
            $chat->object_id = $order->id;
        }
        else if ($params['object_type'] == 'sample')
        {
            $sample = Sample::oneByExtId($params['object_id']);

            if (!$sample)
            {
                $this->errors['not_created']['error_string']['object_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_created'];
            }

            $chat->object_type = 'sample';
            $chat->object_id = $sample->id;
        }
        else if ($params['object_type'] == 'claim')
        {
            $claim = Claim::oneByExtId($params['object_id']);

            if (!$claim)
            {
                $this->errors['not_created']['error_string']['object_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_created'];
            }

            $chat->object_type = 'claim';
            $chat->object_id = $claim->id;
        }

        $user = User::oneByExtId($params['user_id']);

        if (!$user)
        {
            $this->errors['not_created']['error_string']['user_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_created'];
        }

        $chat->user_id = $user->id;
        $chat->message = $params['message'];
        $chat->date = to_db_datetime($params['date']);

        if ($user->account_type == 'admin') $chat->read_by_admin = 1;
        if ($user->account_type == 'manager') $chat->read_by_manager = 1;
        if ($user->account_type == 'client') $chat->read_by_client = 1;

        $chat->save();

        if (array_key_exists('files', $params) && !empty($params['files']))
        {
            foreach ($params['files'] as $fileLink)
            {
                $httpFile = http_get_file($fileLink);

                if ($httpFile)
                {
                    $file = new File();

                    $file->object_id = $chat->id;
                    $file->object_type = 'chat';
                    $file->file = $httpFile['file'];
                    $file->name = $httpFile['name'];
                    $file->mime_type = $httpFile['mime_type'];
                    $file->hash = random_hash(32);

                    $file->save();
                }
            }
        }

        // ExtChatAddJob::dispatch($chat->id);

        return [
            'message_id' => $params['message_id']
        ];
    }
}