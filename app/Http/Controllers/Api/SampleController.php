<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use App\Models\Sample;
use App\Models\UserAgreement;
use App\Models\UserConsignee;
use App\Models\NomenclatureType;
use App\Models\CardboardType;
use App\Models\CardboardProfile;
use App\Models\CardboardColor;
use App\Models\File;
use App\Jobs\ExtSampleUpdateJob;
use App\Jobs\ExtSampleDeleteJob;

class SampleController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/samples';
    }

    /**
     * Метод запроса.
     * Получение образца.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            Sample::$apiRules['get'],
            Sample::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $sample = Sample::oneByExtId($params['sample_id']);

        if (!$sample)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $sample->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateSample($params[$k]);
            }
        }
        else
        {
            $result = $this->updateSample($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteSample($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteSample($params);
        }

        return $result;
    }

    /**
     * Обновление образца.
     * 
     * @param array $params
     * @return array
     */
    private function updateSample($params)
    {
        $validator = Validator::make(
            $params,
            Sample::$apiRules['update'],
            Sample::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $sample = Sample::oneByExtId($params['sample_id']);

        if (!$sample)
        {
            $this->errors['not_updated']['error_string']['sample_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('user_id', $params))
        {
            $user = User::oneByExtId($params['user_id']);

            if (!$user)
            {
                $this->errors['not_updated']['error_string']['user_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->user_id = $user->id;
        }

        if (array_key_exists('buyer_id', $params))
        {
            $userAgreement = UserAgreement::where('buyer_ext_id', $params['buyer_id'])->first();

            if (!$userAgreement || $userAgreement->user_id != $sample->user_id)
            {
                $this->errors['not_updated']['error_string']['buyer_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->buyer_ext_id = $userAgreement->buyer_ext_id;
        }

        if (array_key_exists('consignee_id', $params))
        {
            $userConsignee = UserConsignee::oneByExtId($params['consignee_id']);

            if (!$userConsignee || $userConsignee->user_id != $sample->user_id)
            {
                $this->errors['not_updated']['error_string']['consignee_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->consignee_id = $userConsignee->id;
        }

        if (array_key_exists('agreement_id', $params))
        {
            $userAgreement = UserAgreement::oneByExtId($params['agreement_id']);

            if (!$userAgreement || $userAgreement->user_id != $sample->user_id)
            {
                $this->errors['not_updated']['error_string']['agreement_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->agreement_id = $userAgreement->id;
        }

        if (array_key_exists('nomenclature_type_id', $params))
        {
            $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);

            if (!$nomenclatureType)
            {
                $this->errors['not_updated']['error_string']['nomenclature_type_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->nomenclature_type_id = $nomenclatureType->id;
        }

        if (array_key_exists('cardboard_type_id', $params))
        {
            $cardboardType = CardboardType::oneByExtId($params['cardboard_type_id']);

            if (!$cardboardType)
            {
                $this->errors['not_updated']['error_string']['cardboard_type_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->cardboard_type_id = $cardboardType->id;
        }

        if (array_key_exists('cardboard_profile_id', $params))
        {
            $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile_id']);

            if (!$cardboardProfile)
            {
                $this->errors['not_updated']['error_string']['cardboard_profile_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->cardboard_profile_id = $cardboardType->id;
        }

        if (array_key_exists('cardboard_color_id', $params))
        {
            $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color_id']);

            if (!$cardboardColor)
            {
                $this->errors['not_updated']['error_string']['cardboard_color_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $sample->cardboard_color_id = $cardboardColor->id;
        }

        if (array_key_exists('delivery_type', $params)) $sample->delivery_type = $params['delivery_type'];
        if (array_key_exists('request_number', $params)) $sample->request_number = $params['request_number'] ?: null;
        if (array_key_exists('sizes', $params)) $sample->sizes = json_encode($params['sizes']);
        if (array_key_exists('making', $params)) $sample->making = $params['making'];
        if (array_key_exists('cnt', $params)) $sample->cnt = $params['cnt'] ?: 0;
        if (array_key_exists('comment', $params)) $sample->comment = $params['comment'] ?: null;
        if (array_key_exists('date', $params)) $sample->date = to_db_datetime($params['date']);
        if (array_key_exists('product_code', $params)) $sample->product_code = $params['product_code'];

        if (array_key_exists('file', $params))
        {
            if (!empty($params['file']))
            {
                $httpFile = http_get_file($params['file']);

                if ($httpFile)
                {
                    if ($sample->file)
                    {
                        $file = File::oneByHash($sample->file->hash);
        
                        $file->file = $httpFile['file'];
                        $file->name = $httpFile['name'];
                        $file->mime_type = $httpFile['mime_type'];
                        $file->hash = random_hash(32);
    
                        $file->save();
                    }
                    else
                    {
                        $file = new File();
        
                        $file->object_id = $sample->id;
                        $file->object_type = 'sample';
                        $file->file = $httpFile['file'];
                        $file->name = $httpFile['name'];
                        $file->mime_type = $httpFile['mime_type'];
                        $file->hash = random_hash(32);
    
                        $file->save();
                    }
                }
                else
                {
                    $this->errors['not_updated']['error_string']['file'] = $this->errorsMessages['not_found'];
                    return $this->errors['not_updated'];
                }
            }
            else
            {
                $sample->file()->delete();
            }
        }

        $sample->save();

        // ExtSampleUpdateJob::dispatch($sample->id);

        return [
            'sample_id' => $params['sample_id']
        ];
    }

    /**
     * Удаление образца.
     * 
     * @param array $params
     * @return array
     */
    private function deleteSample($params)
    {
        $validator = Validator::make(
            $params,
            Sample::$apiRules['delete'],
            Sample::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $sample = Sample::oneByExtId($params['sample_id']);

        if (!$sample)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $sample->file()->delete();

        foreach ($sample->chat as $sampleChat)
        {
            $sampleChat->files()->delete();
            $sampleChat->delete();
        }

        // ExtSampleDeleteJob::dispatch($sample->id);
        
        $sample->delete();

        return [
            'sample_id' => $params['sample_id']
        ];
    }
}