<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use App\Models\File;
use App\Models\Chat;
use App\Jobs\ExtOrderDeleteJob;
use App\Jobs\ExtSampleDeleteJob;
use App\Jobs\ExtClaimDeleteJob;
use App\Jobs\ExtUserConsigneeDeleteJob;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/users';
    }

    /**
     * Метод запроса.
     * Получение пользователя.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            User::$apiRules['get'],
            User::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $user = User::oneByExtId($params['user_id']);

        if (!$user)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $user->serrializeForApi();
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createUser($params[$k]);
            }
        }
        else
        {
            $result = $this->createUser($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateUser($params[$k]);
            }
        }
        else
        {
            $result = $this->updateUser($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteUser($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteUser($params);
        }

        return $result;
    }

    /**
     * Создание пользователя.
     * 
     * @param array $params
     * @return array
     */
    private function createUser($params)
    {
        $validator = Validator::make(
            $params,
            User::$apiRules['create'],
            User::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $user = User::oneByExtId($params['user_id']);

        if ($user)
        {
            $this->errors['not_created']['error_string']['user_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        if ($params['account_type'] == 'client')
        {
            // Добавление типа client
            $userManager = User::oneByExtId($params['manager']['user_id']);

            if (!$userManager)
            {
                $this->errors['not_created']['error_string']['manager.user_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_created'];
            }

            if (array_key_exists('manager', $params))
            {
                if (array_key_exists('photo', $params['manager']) && !empty($params['manager']['photo']))
                {
                    $httpFile = http_get_file($params['manager']['photo']);

                    if ($httpFile)
                    {
                        if ($userManager->file)
                        {
                            $file = File::oneByHash($userManager->file->hash);
        
                            // Проверка дубликатов
                            if ($file->name != $httpFile['name'])
                            {
                                $file->file = $httpFile['file'];
                                $file->name = $httpFile['name'];
                                $file->mime_type = $httpFile['mime_type'];
                                $file->hash = random_hash(32);
                
                                $file->save();
                            }
                        }
                        else
                        {
                            $file = new File();
                          
                            $file->object_id = $userManager->id;
                            $file->object_type = 'user';
                            $file->file = $httpFile['file'];
                            $file->name = $httpFile['name'];
                            $file->mime_type = $httpFile['mime_type'];
                            $file->hash = random_hash(32);
            
                            $file->save();
                        }
                    }
                    else
                    {
                        $this->errors['not_created']['error_string']['manager.photo'] = $this->errorsMessages['not_found'];
                        return $this->errors['not_created'];
                    }
                }

                if (isset($params['manager']['phone'])) $userManager->phone = $params['manager']['phone'];
    
                $userManager->save();
            }

            $user = new User();

            $user->ext_id = $params['user_id'];
            $user->account_type = $params['account_type'];
            $user->status = $params['status'];
            $user->name = $params['name'];
            $user->login = $params['login'];
            $user->password = Hash::make($params['password']);
            $user->manager_id = $userManager->id;
            $user->email = $params['email'];

            $user->save();

            return [
                'user_id' => $params['user_id']
            ];
        }
        else
        {
            // Добавление типа manager, admin
            $user = new User();

            $user->ext_id = $params['user_id'];
            $user->account_type = $params['account_type'];
            $user->status = $params['status'];
            $user->name = $params['name'];
            $user->login = $params['login'];
            $user->password = Hash::make($params['password']);
            $user->email = $params['email'];
            
            $user->save();

            return [
                'user_id' => $params['user_id']
            ];
        }
    }

    /**
     * Обновление пользователя.
     * 
     * @param array $params
     * @return array
     */
    private function updateUser($params)
    {
        $validator = Validator::make(
            $params,
            User::$apiRules['update'],
            User::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $user = User::oneByExtId($params['user_id']);

        if (!$user)
        {
            $this->errors['not_updated']['error_string']['user_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        // Обработка $params['manager']
        if ($user->account_type == 'client' && array_key_exists('manager', $params))
        {
            if (!array_key_exists('user_id', $params['manager'])
                || array_key_exists('user_id', $params['manager'])
                && $user->clientManager->ext_id == $params['manager']['user_id'])
            {
                if (array_key_exists('photo', $params['manager']))
                {
                    if (!empty($params['manager']['photo']))
                    {
                        $httpFile = http_get_file($params['manager']['photo']);

                        if ($httpFile)
                        {
                            if ($user->clientManager->file)
                            {
                                $file = File::oneByHash($user->clientManager->file->hash);
        
                                // Проверка дубликатов
                                if ($file->name != basename($params['manager']['photo']))
                                {
                                    $file->file = $httpFile['file'];
                                    $file->name = $httpFile['name'];
                                    $file->mime_type = $httpFile['mime_type'];
                                    $file->hash = random_hash(32);
                    
                                    $file->save();
                                }
                            }
                            else
                            {
                                $file = new File();
        
                                $file->object_id = $user->clientManager->id;
                                $file->object_type = 'user';
                                $file->file = $httpFile['file'];
                                $file->name = $httpFile['name'];
                                $file->mime_type = $httpFile['mime_type'];
                                $file->hash = random_hash(32);
        
                                $file->save();
                            }
                        }
                        else
                        {
                            $this->errors['not_updated']['error_string']['manager.photo'] = $this->errorsMessages['not_found'];
                            return $this->errors['not_updated'];
                        }
                    }
                    else
                    {
                        $user->clientManager->file()->delete();
                    }
                }

                if (array_key_exists('phone', $params['manager'])) $user->clientManager->phone = $params['manager']['phone'];

                $user->clientManager->save();
            }
            else
            {
                $userManager = User::oneByExtId($params['manager']['user_id']);

                if (!$userManager)
                {
                    $this->errors['not_updated']['error_string']['manager.user_id'] = $this->errorsMessages['not_found'];
                    return $this->errors['not_updated'];
                }

                if (array_key_exists('photo', $params['manager']))
                {
                    if (!empty($params['manager']['photo']))
                    {
                        $httpFile = http_get_file($params['manager']['photo']);

                        if ($httpFile)
                        {
                            if ($userManager->file)
                            {
                                $file = File::oneByHash($userManager->file->hash);
        
                                // Проверка дубликатов
                                if ($file->name != $httpFile)
                                {
                                    $file->object_type = 'user';
                                    $file->file = $httpFile['file'];
                                    $file->name = $httpFile['name'];
                                    $file->mime_type = $httpFile['mime_type'];
                                    $file->hash = random_hash(32);
                    
                                    $file->save();
                                }
                            }
                            else
                            {
                                $file = new File();
        
                                $file->object_id = $userManager->id;
                                $file->object_type = 'user';
                                $file->file = $httpFile['file'];
                                $file->name = $httpFile['name'];
                                $file->mime_type = $httpFile['mime_type'];
                                $file->hash = random_hash(32);
                                
                                $file->save();
                            }
                        }
                        else
                        {
                            $this->errors['not_updated']['error_string']['manager.photo'] = $this->errorsMessages['not_found'];
                            return $this->errors['not_updated'];
                        }
                    }
                    else
                    {
                        $userManager->file()->delete();
                    }
                }

                if (array_key_exists('phone', $params['manager'])) $userManager->phone = $params['manager']['phone'];
                
                $userManager->save();

                $user->manager_id = $userManager->id;
            }
        }

        if (array_key_exists('account_type', $params)) $user->account_type = $params['account_type'];
        if (array_key_exists('status', $params)) $user->status = ((int)$params['status'] == 1) ? 1 : 0;
        if (array_key_exists('name', $params)) $user->name = $params['name'];
        if (array_key_exists('login', $params)) $user->login = $params['login'];
        if (array_key_exists('password', $params)) $user->password = Hash::make($params['password']);
        if (array_key_exists('email', $params)) $user->email = $params['email'];

        $user->save();
        
        return [
            'user_id' => $params['user_id']
        ];
    }

    /**
     * Удаление пользователя и все его связи.
     * 
     * @param array $params
     * @return array
     */
    private function deleteUser($params)
    {
        $validator = Validator::make(
            $params,
            User::$apiRules['delete'],
            User::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $user = User::oneByExtId($params['user_id']);
        
        if (!$user)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        if ($user->account_type == 'client')
        {
            // Получаем клиента со всеми связями
            $user = User::oneByClientId($user->id);

            if ($user->clientClaims()->count() > 0)
            {
                foreach ($user->clientClaims as $clientClaim)
                {
                    foreach ($clientClaim->chat as $claimChat)
                    {
                        $claimChat->files()->delete();
                        $claimChat->delete();
                    }

                    foreach ($clientClaim->items as $claimItem)
                    {
                        $claimItem->files()->delete();
                        $claimItem->delete();
                    }
            
                    // ExtClaimDeleteJob::dispatch($clientClaim->id);

                    $clientClaim->delete();
                }
            }

            if ($user->clientSamples()->count() > 0)
            {
                foreach ($user->clientSamples as $clientSample)
                {
                    foreach ($clientSample->chat as $sampleChat)
                    {
                        $sampleChat->files()->delete();
                        $sampleChat->delete();
                    }

                    $clientSample->file()->delete();

                    // ExtSampleDeleteJob::dispatch($clientSample->id);

                    $clientSample->delete();
                }
            }

            if ($user->clientOrders()->count() > 0)
            {
                foreach ($user->clientOrders as $clientOrder)
                {
                    foreach ($clientOrder->chat as $orderChat)
                    {
                        $orderChat->files()->delete();
                        $orderChat->delete();
                    }

                    $clientOrder->items()->delete();
                    $clientOrder->file()->delete();
            
                    // ExtOrderDeleteJob::dispatch($clientOrder->id);

                    $clientOrder->delete();
                }
            }

            $user->clientNomenclatures()->delete();
            $user->clientAgreements()->delete();

            foreach ($user->clientConsignees as $clientConsignee)
            {
                // ExtUserConsigneeDeleteJob::dispatch($clientConsignee->id);

                $clientConsignee->delete();
            }

            $user->delete();
        }
        else if ($user->account_type == 'manager')
        {
            Chat::where('user_id', $user->id)->delete();
            $user->file()->delete();
            $user->delete();
        }
        else if ($user->account_type == 'admin')
        {
            Chat::where('user_id', $user->id)->delete();
            $user->delete();
        }

        $user->delete();

        return [
            'user_id' => $params['user_id']
        ];
    }
}