<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\NomenclatureType;

class NomenclatureTypeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/nomenclature_types';
    }

    /**
     * Метод запроса.
     * Получение типа номенклатуры.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            NomenclatureType::$apiRules['get'],
            NomenclatureType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);

        if (!$nomenclatureType)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $nomenclatureType->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createNomenclatureType($params[$k]);
            }
        }
        else
        {
            $result = $this->createNomenclatureType($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateNomenclatureType($params[$k]);
            }
        }
        else
        {
            $result = $this->updateNomenclatureType($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteNomenclatureType($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteNomenclatureType($params);
        }

        return $result;
    }

    /**
     * Создание типа номенклатуры.
     * 
     * @param array $params
     * @return array
     */
    private function createNomenclatureType($params)
    {
        $validator = Validator::make(
            $params,
            NomenclatureType::$apiRules['create'],
            NomenclatureType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);

        if ($nomenclatureType)
        {
            $this->errors['not_created']['error_string']['nomenclature_type_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $nomenclatureType = new NomenclatureType();

        $nomenclatureType->ext_id = $params['nomenclature_type_id'];
        $nomenclatureType->title = $params['title'];

        $nomenclatureType->save();

        return [
            'nomenclature_type_id' => $params['nomenclature_type_id']
        ];
    }

    /**
     * Обновление типа номенклатуры.
     * 
     * @param array $params
     * @return array
     */
    private function updateNomenclatureType($params)
    {
        $validator = Validator::make(
            $params,
            NomenclatureType::$apiRules['update'],
            NomenclatureType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);

        if (!$nomenclatureType)
        {
            $this->errors['not_updated']['error_string']['nomenclature_type_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('title', $params)) $nomenclatureType->title = $params['title'];

        $nomenclatureType->save();

        return [
            'nomenclature_type_id' => $params['nomenclature_type_id']
        ];
    }

    /**
     * Удаление типа номенклатуры.
     * 
     * @param array $params
     * @return array
     */
    private function deleteNomenclatureType($params)
    {
        $validator = Validator::make(
            $params,
            NomenclatureType::$apiRules['delete'],
            NomenclatureType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);

        if (!$nomenclatureType)
        {
            $this->errors['not_deleted']['error_string']['nomenclature_type_id'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $nomenclatureType->delete();

        return [
            'nomenclature_type_id' => $params['nomenclature_type_id']
        ];
    }
}