<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\Claim;
use App\Models\ClaimItem;
use App\Models\UserNomenclature;
use App\Models\Order;
use App\Models\File;
use App\Jobs\ExtClaimUpdateJob;
use App\Jobs\ExtClaimDeleteJob;

class ClaimController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/claims';
    }

    /**
     * Метод запроса.
     * Получение претензии.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            Claim::$apiRules['get'],
            Claim::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $claim = Claim::oneByExtId($params['claim_id']);

        if (!$claim)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $claim->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateClaim($params[$k]);
            }
        }
        else
        {
            $result = $this->updateClaim($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteClaim($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteClaim($params);
        }

        return $result;
    }

    /**
     * Обновление претензии.
     * 
     * @param array $params
     * @return array
     */
    private function updateClaim($params)
    {
        $validator = Validator::make(
            $params,
            Claim::$apiRules['update'],
            Claim::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $claim = Claim::oneByExtId($params['claim_id']);

        if (!$claim)
        {
            $this->errors['not_updated']['error_string']['claim_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('order_id', $params))
        {
            $order = Order::oneByExtId($params['order_id']);

            if (!$order)
            {
                $this->errors['not_updated']['error_string']['order_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $claim->order_id = $order->id;
        }

        if (array_key_exists('common_claim', $params)) $claim->common_claim = $params['common_claim'] ?: null;
        if (array_key_exists('common_decision', $params)) $claim->common_decision = $params['common_decision'] ?: null;
        if (array_key_exists('status', $params)) $claim->status = $params['status'];
        if (array_key_exists('product_code', $params)) $claim->product_code = $params['product_code'];

        if (array_key_exists('items', $params))
        {
            // Проверка изделий
            foreach ($params['items'] as $item)
            {
                $userNomenclature = UserNomenclature::oneByExtId($item['nomenclature_id']);

                if (!$userNomenclature)
                {
                    $this->errors['not_updated']['error_string']['items.nomenclature_id'] = $this->errorsMessages['not_found'];
                    return $this->errors['not_updated'];
                }
            }

            // Удалиь позиции
            if ($claim->items)
            {
                $oldItems = $claim->items->toArray();
                $oldItemsIds = array_map(function($v) { return $v['nomenclature']['ext_id']; }, $oldItems);
                $newItemsIds = array_map(function($v) { return $v['nomenclature_id']; }, $params['items']);
                $deletedIds = array_diff($oldItemsIds, $newItemsIds);
    
                if (!empty($deletedIds))
                {
                    foreach ($deletedIds as $deletedId)
                    {
                        $userNomenclature = UserNomenclature::oneByExtId($deletedId);
                        $claimItem = ClaimItem::oneByClaimIdNomenclatureId($claim->id, $userNomenclature->id);

                        if ($claimItem)
                        {
                            $claimItem->files()->delete();
                            $claimItem->delete();
                        }
                    }
                }
            }

            // Добавить/обновить позиции
            foreach ($params['items'] as $item)
            {
                $userNomenclature = UserNomenclature::oneByExtId($item['nomenclature_id']);
                $claimItem = ClaimItem::oneByClaimIdNomenclatureId($claim->id, $userNomenclature->id);

                if ($claimItem)
                {
                    if (array_key_exists('claim', $item)) $claimItem->claim = $item['claim'] ?: null;
                    if (array_key_exists('cnt', $item)) $claimItem->cnt = $item['cnt'] ?: 0;
                    if (array_key_exists('decision', $item)) $claimItem->decision = $item['decision'] ?: null;

                    $claimItem->save();

                    if (array_key_exists('files', $item))
                    {
                        if (!empty($item['files']))
                        {
                            foreach ($item['files'] as $fileLink)
                            {
                                $httpFile = http_get_file($fileLink);

                                if ($httpFile)
                                {
                                    $file = new File();
                
                                    $file->object_id = $claimItem->id;
                                    $file->object_type = 'claim_item';
                                    $file->file = $httpFile['file'];
                                    $file->name = $httpFile['name'];
                                    $file->mime_type = $httpFile['mime_type'];
                                    $file->hash = random_hash(32);
                        
                                    $file->save();
                                }
                            }
                        }
                        else
                        {
                            $claimItem->files()->delete();
                        }
                    }
                }
                else 
                {
                    $claimItem = new ClaimItem();

                    $claimItem->claim_id = $claim->id;
                    $claimItem->nomenclature_id = $userNomenclature->id;
                    if (array_key_exists('claim', $item)) $claimItem->claim = $item['claim'] ?: null;
                    if (array_key_exists('cnt', $item)) $claimItem->cnt = $item['cnt'] ?: 0;
                    if (array_key_exists('decision', $item)) $claimItem->decision = $item['decision'] ?: null;

                    $claimItem->save();

                    if (array_key_exists('files', $item) && !empty($item['files']))
                    {
                        foreach ($item['files'] as $fileLink)
                        {
                            $httpFile = http_get_file($fileLink);

                            if ($httpFile)
                            {
                                $file = new File();
              
                                $file->object_id = $claimItem->id;
                                $file->object_type = 'claim_item';
                                $file->file = $httpFile['file'];
                                $file->name = $httpFile['name'];
                                $file->mime_type = $httpFile['mime_type'];
                                $file->hash = random_hash(32);
                    
                                $file->save();
                            }
                        }
                    }
                }
            }
        }

        $claim->save();

        // ExtClaimUpdateJob::dispatch($claim->id);
        
        return [
            'claim_id' => $params['claim_id']
        ];
    }

    /**
     * Удаление претензии.
     * 
     * @param array $params
     * @return array
     */
    private function deleteClaim($params)
    { 
        $validator = Validator::make(
            $params,
            Claim::$apiRules['delete'],
            Claim::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $claim = Claim::oneByExtId($params['claim_id']);

        if (!$claim)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        foreach ($claim->items as $claimItem)
        {
            $claimItem->files()->delete();
        }

        $claim->items()->delete();

        foreach ($claim->chat as $claimChat)
        {
            $claimChat->files()->delete();
            $claimChat->delete();
        }

        // ExtClaimDeleteJob::dispatch($claim->id);
        
        $claim->delete();

        return [
            'claim_id' => $params['claim_id']
        ];
    }
}