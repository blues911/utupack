<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\CardboardProfile;

class CardboardProfileController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/cardboard_profiles';
    }

    /**
     * Метод запроса.
     * Получение профиля картона.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            CardboardProfile::$apiRules['get'],
            CardboardProfile::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile_id']);

        if (!$cardboardProfile)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $cardboardProfile->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createCardboardProfile($params[$k]);
            }
        }
        else
        {
            $result = $this->createCardboardProfile($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateCardboardProfile($params[$k]);
            }
        }
        else
        {
            $result = $this->updateCardboardProfile($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteCardboardProfile($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteCardboardProfile($params);
        }

        return $result;
    }

    /**
     * Создание профиля картона.
     * 
     * @param array $params
     * @return array
     */
    private function createCardboardProfile($params)
    {
        $validator = Validator::make(
            $params,
            CardboardProfile::$apiRules['create'],
            CardboardProfile::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile_id']);

        if ($cardboardProfile)
        {
            $this->errors['not_created']['error_string']['cardboard_profile_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $cardboardProfile = new CardboardProfile();

        $cardboardProfile->ext_id = $params['cardboard_profile_id'];
        $cardboardProfile->title = $params['title'];

        $cardboardProfile->save();

        return [
            'cardboard_profile_id' => $params['cardboard_profile_id']
        ];
    }

    /**
     * Обновление профиля картона.
     * 
     * @param array $params
     * @return array
     */
    private function updateCardboardProfile($params)
    {
        $validator = Validator::make(
            $params,
            CardboardProfile::$apiRules['update'],
            CardboardProfile::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile_id']);

        if (!$cardboardProfile)
        {
            $this->errors['not_updated']['error_string']['cardboard_profile_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('title', $params)) $cardboardProfile->title = $params['title'];

        $cardboardProfile->save();

        return [
            'cardboard_profile_id' => $params['cardboard_profile_id']
        ];
    }

    /**
     * Удаление профиля картона.
     * 
     * @param array $params
     * @return array
     */
    private function deleteCardboardProfile($params)
    {
        $validator = Validator::make(
            $params,
            CardboardProfile::$apiRules['delete'],
            CardboardProfile::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile_id']);

        if (!$cardboardProfile)
        {
            $this->errors['not_deleted']['error_string']['cardboard_profile_id'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $cardboardProfile->delete();

        return [
            'cardboard_profile_id' => $params['cardboard_profile_id']
        ];
    }
}