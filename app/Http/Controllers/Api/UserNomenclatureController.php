<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use App\Models\UserNomenclature;
use App\Models\CardboardColor;
use App\Models\CardboardProfile;
use App\Models\CardboardType;
use App\Models\NomenclatureType;

class UserNomenclatureController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/nomenclatures';
    }
    
    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createUserNomenclature($params[$k]);
            }
        }
        else
        {
            $result = $this->createUserNomenclature($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateUserNomenclature($params[$k]);
            }
        }
        else
        {
            $result = $this->updateUserNomenclature($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteUserNomenclature($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteUserNomenclature($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Создание всех изделий пользователя.
     * 
     * @param array $params
     * @return array
     */
    public function delete_all($params)
    {
        $validator = Validator::make(
            $params,
            UserNomenclature::$apiRules['delete_all'],
            UserNomenclature::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $user = User::oneByExtId($params['user_id']);  

        if (!$user || $user->clientNomenclatures()->count() == 0)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }


        $user->clientNomenclatures()->delete();

        return [
            'user_id' => $params['user_id']
        ];
    }

    /**
     * Создание изделия (товарная номенклатура).
     * 
     * @param array $params
     * @return array
     */
    private function createUserNomenclature($params)
    {
        $validator = Validator::make(
            $params,
            UserNomenclature::$apiRules['create'],
            UserNomenclature::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userNomenclature = UserNomenclature::oneByExtId($params['nomenclature_id']);

        if ($userNomenclature)
        {
            $this->errors['not_created']['error_string']['nomenclature_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $user = User::oneByExtId($params['user_id']);
        $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);
        $cardboardType = CardboardType::oneByExtId($params['cardboard_type_id']);
        $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile_id']);
        $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color_id']);

        if (!$user
            || !$nomenclatureType
            || !$cardboardType
            || !$cardboardProfile
            || !$cardboardColor)
        {
            if (!$user) $this->errors['not_created']['error_string']['user_id'] = $this->errorsMessages['not_found'];
            if (!$nomenclatureType) $this->errors['not_created']['error_string']['nomenclature_type_id'] = $this->errorsMessages['not_found'];
            if (!$cardboardType) $this->errors['not_created']['error_string']['cardboard_type_id'] = $this->errorsMessages['not_found'];
            if (!$cardboardProfile) $this->errors['not_created']['error_string']['cardboard_profile_id'] = $this->errorsMessages['not_found'];
            if (!$cardboardColor) $this->errors['not_created']['error_string']['cardboard_color_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_created'];
        }

        $userNomenclature = new UserNomenclature();

        $userNomenclature->ext_id = $params['nomenclature_id'];
        $userNomenclature->user_id = $user->id;
        $userNomenclature->title = $params['title'];
        if (array_key_exists('sku', $params)) $userNomenclature->sku = $params['sku'];
        $userNomenclature->cardboard_type_id = $cardboardType->id;
        if (isset($cardboardProfile)) $userNomenclature->cardboard_profile_id = $cardboardProfile->id;
        $userNomenclature->cardboard_color_id = $cardboardColor->id;
        $userNomenclature->print = $params['cardboard_print'];
        if (isset($userNomenclature)) $userNomenclature->nomenclature_type_id = $nomenclatureType->id;
        $userNomenclature->area = $params['area'];
        $userNomenclature->product_code = $params['product_code'];
        $userNomenclature->status = ((int)$params['status'] == 1) ? 1 : 0;

        $userNomenclature->save();

        return [
            'nomenclature_id' => $params['nomenclature_id']
        ];
    }

    /**
     * Обновление изделия (товарная номенклатура).
     * 
     * @param array $params
     * @return array
     */
    private function updateUserNomenclature($params)
    {
        $validator = Validator::make(
            $params,
            UserNomenclature::$apiRules['update'],
            UserNomenclature::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userNomenclature = UserNomenclature::oneByExtId($params['nomenclature_id']);

        if (!$userNomenclature)
        {
            $this->errors['not_updated']['error_string']['nomenclature_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('user_id', $params))
        {
            $user = User::oneByExtId($params['user_id']);

            if (!$user)
            {
                $this->errors['not_updated']['error_string']['user_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $userNomenclature->user_id = $user->id;
        }

        if (array_key_exists('title', $params)) $userNomenclature->title = $params['title'];
        if (array_key_exists('sku', $params)) $userNomenclature->sku = $params['sku'];

        if (array_key_exists('nomenclature_type_id', $params))
        {
            $nomenclatureType = NomenclatureType::oneByExtId($params['nomenclature_type_id']);

            if (!$nomenclatureType)
            {
                $this->errors['not_updated']['error_string']['nomenclature_type_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }
            
            $userNomenclature->nomenclature_type_id = $nomenclatureType->id;
        }

        if (array_key_exists('cardboard_type', $params))
        {
            $cardboardType = CardboardType::oneByExtId($params['cardboard_type']);

            if (!$cardboardType)
            {
                $this->errors['not_updated']['error_string']['cardboard_type'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $userNomenclature->cardboard_type_id = $cardboardType->id;
        }

        if (array_key_exists('cardboard_profile', $params))
        {
            $cardboardProfile = CardboardProfile::oneByExtId($params['cardboard_profile']);

            if (!$cardboardProfile)
            {
                $this->errors['not_updated']['error_string']['cardboard_profile'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $userNomenclature->cardboard_profile_id = $cardboardProfile->id;
        }

        if (array_key_exists('cardboard_color', $params))
        {
            $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color']);

            if (!$cardboardColor)
            {
                $this->errors['not_updated']['error_string']['cardboard_color'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $userNomenclature->cardboard_color_id = $cardboardColor->id;
        }

        if (array_key_exists('cardboard_print', $params)) $userNomenclature->print = ((int)$params['cardboard_print'] == 1) ? 1 : 0;
        if (array_key_exists('area', $params)) $userNomenclature->area = $params['area'];
        if (array_key_exists('status', $params)) $userNomenclature->status = ((int)$params['status'] == 1) ? 1 : 0;

        $userNomenclature->save();

        return [
            'nomenclature_id' => $params['nomenclature_id']
        ];
    }

    /**
     * Удаление изделия (товарная номенклатура).
     * 
     * @param array $params
     * @return array
     */
    private function deleteUserNomenclature($params)
    {
        $validator = Validator::make(
            $params,
            UserNomenclature::$apiRules['delete'],
            UserNomenclature::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userNomenclature = UserNomenclature::oneByExtId($params['nomenclature_id']);

        if (!$userNomenclature)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $userNomenclature = UserNomenclature::oneByExtId($params['nomenclature_id']);
        $userNomenclature->delete();

        return [
            'nomenclature_id' => $params['nomenclature_id']
        ];
    }
}