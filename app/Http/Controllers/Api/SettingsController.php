<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\BaseController;
use App\Models\Settings;

class SettingsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/settings';
    }

    /**
     * Метод запроса.
     * Обновление настроек.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        if (array_key_exists('default_execution_time', $params))
        {
            Settings::where('var', 'default_execution_time')
                ->update([
                    'value' => $params['default_execution_time']
                ]);
            
            return [
                'result' => 'Ok'
            ];
        }

        if (array_key_exists('min_production_volume', $params))
        {
            Settings::where('var', 'min_production_volume')
                ->update([
                    'value' => $params['min_production_volume']
                ]);

            return [
                'result' => 'Ok'
            ];
        }

        $this->errors['not_updated']['error_string'] = $this->errorsMessages['not_updated'];
        return $this->errors['not_updated'];
    }
}