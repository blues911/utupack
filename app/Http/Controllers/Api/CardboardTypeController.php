<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\CardboardType;

class CardboardTypeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/cardboard_types';
    }

    /**
     * Метод запроса.
     * Получение типа картона.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            CardboardType::$apiRules['get'],
            CardboardType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardType = CardboardType::oneByExtId($params['cardboard_type_id']);

        if (!$cardboardType)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $cardboardType->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createCardboardType($params[$k]);
            }
        }
        else
        {
            $result = $this->createCardboardType($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateCardboardType($params[$k]);
            }
        }
        else
        {
            $result = $this->updateCardboardType($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteCardboardType($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteCardboardType($params);
        }

        return $result;
    }

    /**
     * Создание типа картона.
     * 
     * @param array $params
     * @return array
     */
    private function createCardboardType($params)
    {
        $validator = Validator::make(
            $params,
            CardboardType::$apiRules['create'],
            CardboardType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardType = CardboardType::oneByExtId($params['cardboard_type_id']);

        if ($cardboardType)
        {
            $this->errors['not_created']['error_string']['cardboard_type_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $cardboardType = new CardboardType();

        $cardboardType->ext_id = $params['cardboard_type_id'];
        $cardboardType->title = $params['title'];

        $cardboardType->save();

        return [
            'cardboard_type_id' => $params['cardboard_type_id']
        ];
    }

    /**
     * Обновление типа картона.
     * 
     * @param array $params
     * @return array
     */
    private function updateCardboardType($params)
    {
        $validator = Validator::make(
            $params,
            CardboardType::$apiRules['update'],
            CardboardType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardType = CardboardType::oneByExtId($params['cardboard_type_id']);

        if (!$cardboardType)
        {
            $this->errors['not_updated']['error_string']['cardboard_type_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('title', $params)) $cardboardType->title = $params['title'];

        $cardboardType->save();

        return [
            'cardboard_type_id' => $params['cardboard_type_id']
        ];
    }

    /**
     * Удаление типа картона.
     * 
     * @param array $params
     * @return array
     */
    private function deleteCardboardType($params)
    {
        $validator = Validator::make(
            $params,
            CardboardType::$apiRules['delete'],
            CardboardType::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardType = CardboardType::oneByExtId($params['cardboard_type_id']);

        if (!$cardboardType)
        {
            $this->errors['not_deleted']['error_string']['cardboard_type_id'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $cardboardType->delete();

        return [
            'cardboard_type_id' => $params['cardboard_type_id']
        ];
    }
}