<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\UserAgreement;
use App\Models\UserConsignee;
use App\Models\UserNomenclature;
use App\Models\CardboardType;
use App\Models\CardboardProfile;
use App\Models\CardboardColor;
use App\Models\File;
use App\Jobs\ExtOrderUpdateJob;
use App\Jobs\ExtOrderDeleteJob;

class OrderController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/orders';
    }

    /**
     * Метод запроса.
     * Получение заявки.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            Order::$apiRules['get'],
            Order::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $order = Order::oneByExtId($params['order_id']);

        if (!$order)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $order->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateOrder($params[$k]);
            }
        }
        else
        {
            $result = $this->updateOrder($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteOrder($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteOrder($params);
        }

        return $result;
    }

    /**
     * Обновление заявки.
     * 
     * @param array $params
     * @return array
     */
    private function updateOrder($params)
    {
        $validator = Validator::make(
            $params,
            Order::$apiRules['update'],
            Order::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $order = Order::oneByExtId($params['order_id']);

        if (!$order)
        {
            $this->errors['not_updated']['error_string']['order_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('user_id', $params))
        {
            $user = User::oneByExtId($params['user_id']);

            if (!$user)
            {
                $this->errors['not_updated']['error_string']['user_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $order->user_id = $user->id;
        }

        if (array_key_exists('buyer_id', $params))
        {
            $userAgreement = UserAgreement::where('buyer_ext_id', $params['buyer_id'])->first();

            if (!$userAgreement || $userAgreement->user_id != $order->user_id)
            {
                $this->errors['not_updated']['error_string']['buyer_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $order->buyer_ext_id = $userAgreement->buyer_ext_id;
        }

        if (array_key_exists('consignee_id', $params))
        {
            $userConsignee = UserConsignee::oneByExtId($params['consignee_id']);

            if (!$userConsignee || $userConsignee->user_id != $order->user_id)
            {
                $this->errors['not_updated']['error_string']['consignee_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $order->consignee_id = $userConsignee->id;
        }

        if (array_key_exists('agreement_id', $params))
        {
            $userAgreement = UserAgreement::oneByExtId($params['agreement_id']);

            if (!$userAgreement || $userAgreement->user_id != $order->user_id)
            {
                $this->errors['not_updated']['error_string']['agreement_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $order->agreement_id = $userAgreement->id;
        }

        if (array_key_exists('request_number', $params)) $order->request_number = $params['request_number'] ?: null;
        if (array_key_exists('delivery_type', $params)) $order->delivery_type = $params['delivery_type'];
        if (array_key_exists('desired_date', $params)) $order->desired_date = to_db_datetime($params['desired_date']);
        if (array_key_exists('ready_date', $params)) $order->ready_date = to_db_datetime($params['ready_date']);
        if (array_key_exists('delivery_date', $params)) $order->delivery_date = to_db_datetime($params['delivery_date']);
        if (array_key_exists('comment', $params)) $order->comment = $params['comment'] ?: null;
        if (array_key_exists('status', $params)) $order->status = $params['status'];
        if (array_key_exists('date', $params)) $order->date = to_db_datetime($params['date']);
        if (array_key_exists('product_code', $params)) $order->product_code = $params['product_code'];

        if (array_key_exists('driver_attorney_file', $params))
        {
            if (!empty($params['driver_attorney_file']))
            {
                $httpFile = http_get_file($params['driver_attorney_file']);

                if ($httpFile)
                {
                    if ($order->file)
                    {
                        $file = File::oneByHash($order->file->hash);

                        $file->file = $httpFile['file'];
                        $file->name = $httpFile['name'];
                        $file->mime_type = $httpFile['mime_type'];
                        $file->hash = random_hash(32);
            
                        $file->save();
                    }
                    else
                    {
                        $file = new File();
        
                        $file->object_id = $order->id;
                        $file->object_type = 'order';
                        $file->file = $httpFile['file'];
                        $file->name = $httpFile['name'];
                        $file->mime_type = $httpFile['mime_type'];
                        $file->hash = random_hash(32);
            
                        $file->save();
                    }
                }
                else
                {
                    $this->errors['not_updated']['error_string']['driver_attorney_file'] = $this->errorsMessages['not_found'];
                    return $this->errors['not_updated'];
                }
            }
            else
            {
                $order->file()->delete();
            }
        }

        if (array_key_exists('items', $params))
        {
            // Проверка изделий
            foreach ($params['items'] as $item)
            {
                $userNomenclature = UserNomenclature::oneByExtId($item['nomenclature_id']);

                if (!$userNomenclature)
                {
                    $this->errors['not_updated']['error_string']['nomenclature_id'] = $this->errorsMessages['not_found'];
                    return $this->errors['not_updated'];
                }
            }
            
            // Удалиь позиции
            if ($order->items)
            {
                $oldItems = $order->items->toArray();
                $oldItemsIds = array_map(function($v) { return $v['nomenclature']['ext_id']; }, $oldItems);
                $newItemsIds = array_map(function($v) { return $v['nomenclature_id']; }, $params['items']);
                $deletedIds = array_diff($oldItemsIds, $newItemsIds);
    
                if (!empty($deletedIds))
                {
                    foreach ($deletedIds as $deletedId)
                    {
                        $userNomenclature = UserNomenclature::oneByExtId($deletedId);

                        $orderItem = OrderItem::oneByOrderIdNomenclatureId($order->id, $userNomenclature->id);
                        $orderItem->delete();
                    }
                }
            }

            // Добавить/обновить позиции
            foreach ($params['items'] as $item)
            {
                $userNomenclature = UserNomenclature::oneByExtId($item['nomenclature_id']);
                $orderItem = OrderItem::oneByOrderIdNomenclatureId($order->id, $userNomenclature->id);

                if ($orderItem)
                {
                    if (array_key_exists('address', $item)) $orderItem->address = $item['address'] ?: null;
                    if (array_key_exists('address_number', $item)) $orderItem->address_number = $item['address_number'] ?: null;
                    if (array_key_exists('cnt', $item)) $orderItem->cnt = $item['cnt'] ?: 0;
                    if (array_key_exists('cnt_rule', $item)) $orderItem->cnt_rule = $item['cnt_rule'] ?: 0;
                    if (array_key_exists('produced', $item)) $orderItem->produced = $item['produced'] ?: 0;
                    if (array_key_exists('comment', $item)) $orderItem->comment = $item['comment'] ?: null;
                    if (array_key_exists('amount', $item)) $orderItem->amount = $item['amount'] ?: null;
        
                    $orderItem->save();
                }
                else 
                {
                    $orderItem = new OrderItem();

                    $orderItem->order_id = $order->id;
                    $orderItem->nomenclature_id = $userNomenclature->id;
                    if (array_key_exists('address', $item)) $orderItem->address = $item['address'] ?: null;
                    if (array_key_exists('address_number', $item)) $orderItem->address_number = $item['address_number'] ?: null;
                    if (array_key_exists('cnt', $item)) $orderItem->cnt = $item['cnt'] ?: 0;
                    if (array_key_exists('cnt_rule', $item)) $orderItem->cnt_rule = $item['cnt_rule'] ?: 0;
                    if (array_key_exists('produced', $item)) $orderItem->produced = $item['produced'] ?: 0;
                    if (array_key_exists('comment', $item)) $orderItem->comment = $item['comment'] ?: null;

                    $orderItem->save();
                }
            }
        }

        $order->save();

        // ExtOrderUpdateJob::dispatch($order->id);

        return [
            'order_id' => $params['order_id']
        ];
    }

    /**
     * Удаление заявки.
     * 
     * @param array $params
     * @return array
     */
    private function deleteOrder($params)
    {
        $validator = Validator::make(
            $params,
            Order::$apiRules['delete'],
            Order::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $order = Order::oneByExtId($params['order_id']);

        if (!$order)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $order->items()->delete();
        $order->file()->delete();

        foreach ($order->chat as $orderChat)
        {
            $orderChat->files()->delete();
            $orderChat->delete();
        }

        // ExtOrderDeleteJob::dispatch($order->id);
        
        $order->delete();

        return [
            'order_id' => $params['order_id']
        ];
    }
}