<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use App\Models\UserConsignee;
use App\Jobs\ExtUserConsigneeUpdateJob;
use App\Jobs\ExtUserConsigneeDeleteJob;

class UserConsigneeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/consignees';
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createUserConsignee($params[$k]);
            }
        }
        else
        {
            $result = $this->createUserConsignee($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateUserConsignee($params[$k]);
            }
        }
        else
        {
            $result = $this->updateUserConsignee($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteUserConsignee($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteUserConsignee($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Создание всех грузополучателей пользователя
     * 
     * @param array $params
     * @return array
     */
    public function delete_all($params)
    {
        $validator = Validator::make(
            $params,
            UserConsignee::$apiRules['delete_all'],
            UserConsignee::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $user = User::oneByExtId($params['user_id']);  
        
        if (!$user || $user->clientConsignees()->count() == 0)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        foreach ($user->clientConsignees as $clientConsignee)
        {
            // ExtUserConsigneeDeleteJob::dispatch($clientConsignee->id);
            
            $clientConsignee->delete();
        }

        return [
            'user_id' => $params['user_id']
        ];
    }

    /**
     * Создание грузополучателя.
     * 
     * @param array $params
     * @return array
     */
    private function createUserConsignee($params)
    {
        $validator = Validator::make(
            $params,
            UserConsignee::$apiRules['create'],
            UserConsignee::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userConsignee = UserConsignee::oneByExtId($params['consignee_id']);

        if ($userConsignee)
        {
            $this->errors['not_created']['error_string']['consignee_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $user = User::oneByExtId($params['user_id']);

        if (!$user)
        {
            $this->errors['not_created']['error_string']['user_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_created'];
        }

        $userConsignee = new UserConsignee();

        $userConsignee->ext_id = $params['consignee_id'];
        $userConsignee->user_id = $user->id;
        $userConsignee->title = $params['title'];
        $userConsignee->inn = $params['inn'];
        $userConsignee->ogrn = $params['ogrn'];

        $userConsignee->save();

        return [
            'consignee_id' => $params['consignee_id']
        ];
    }

    /**
     * Обновление грузополучателя.
     * 
     * @param array $params
     * @return array
     */
    private function updateUserConsignee($params)
    {
        $validator = Validator::make(
            $params,
            UserConsignee::$apiRules['update'],
            UserConsignee::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $userConsignee = UserConsignee::oneByExtId($params['consignee_id']);

        if (!$userConsignee)
        {
            $this->errors['not_updated']['error_string']['consignee_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('user_id', $params))
        {
            $user = User::oneByExtId($params['user_id']);

            if (!$user)
            {
                $this->errors['not_updated']['error_string']['user_id'] = $this->errorsMessages['not_found'];
                return $this->errors['not_updated'];
            }

            $userConsignee->user_id = $user->id;
        }

        if (array_key_exists('title', $params)) $userConsignee->title = $params['title'];
        if (array_key_exists('inn', $params)) $userConsignee->inn = $params['inn'];
        if (array_key_exists('ogrn', $params)) $userConsignee->ogrn = $params['ogrn'];

        $userConsignee->save();

        // ExtUserConsigneeUpdateJob::dispatch($userConsignee->id);

        return [
            'consignee_id' => $params['consignee_id']
        ];
    }

    /**
     * Удаление грузополучателя.
     * 
     * @param array $params
     * @return array
     */
    private function deleteUserConsignee($params)
    {
        $validator = Validator::make(
            $params,
            UserConsignee::$apiRules['delete'],
            UserConsignee::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }
        
        $userConsignee = UserConsignee::oneByExtId($params['consignee_id']);

        if (!$userConsignee)
        {
            $this->errors['not_deleted']['error_string'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        // ExtUserConsigneeDeleteJob::dispatch($userConsignee->id);

        $userConsignee->delete();

        return [
            'consignee_id' => $params['consignee_id']
        ];
    }
}