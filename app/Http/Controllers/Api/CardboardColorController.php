<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController;
use App\Models\CardboardColor;

class CardboardColorController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->logUrl .= '/cardboard_colors';
    }

    /**
     * Метод запроса.
     * Получение цвета картона.
     * 
     * @param array $params
     * @return array
     */
    public function get($params)
    {
        $validator = Validator::make(
            $params,
            CardboardColor::$apiRules['get'],
            CardboardColor::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color_id']);

        if (!$cardboardColor)
        {
            $this->errors['not_found']['error_string'] = $this->errorsMessages['not_found'];
            return $this->errors['not_found'];
        }

        return $cardboardColor->serrializeForApi(); 
    }

    /**
     * Метод запроса.
     * Создание с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function create($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->createCardboardColor($params[$k]);
            }
        }
        else
        {
            $result = $this->createCardboardColor($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Обновление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function update($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->updateCardboardColor($params[$k]);
            }
        }
        else
        {
            $result = $this->updateCardboardColor($params);
        }

        return $result;
    }

    /**
     * Метод запроса.
     * Удаление с поддержкой нескольких объектов.
     * 
     * @param array $params
     * @return array
     */
    public function delete($params)
    {
        $result = [];

        if (is_multi_array($params))
        {
            foreach ($params as $k => $v)
            {
                $result[$k] = $this->deleteCardboardColor($params[$k]);
            }
        }
        else
        {
            $result = $this->deleteCardboardColor($params);
        }

        return $result;
    }

    /**
     * Создание цвета картона.
     * 
     * @param array $params
     * @return array
     */
    private function createCardboardColor($params)
    {
        $validator = Validator::make(
            $params,
            CardboardColor::$apiRules['create'],
            CardboardColor::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color_id']);

        if ($cardboardColor)
        {
            $this->errors['not_created']['error_string']['cardboard_color_id'] = $this->errorsMessages['exist'];
            return $this->errors['not_created'];
        }

        $cardboardColor = new CardboardColor();

        $cardboardColor->ext_id = $params['cardboard_color_id'];
        $cardboardColor->title = $params['title'];

        $cardboardColor->save();

        return [
            'cardboard_color_id' => $params['cardboard_color_id']
        ];
    }

    /**
     * Обновление цвета картона.
     * 
     * @param array $params
     * @return array
     */
    private function updateCardboardColor($params)
    {
        $validator = Validator::make(
            $params,
            CardboardColor::$apiRules['update'],
            CardboardColor::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color_id']);

        if (!$cardboardColor)
        {
            $this->errors['not_updated']['error_string']['cardboard_color_id'] = $this->errorsMessages['not_found'];
            return $this->errors['not_updated'];
        }

        if (array_key_exists('title', $params)) $cardboardColor->title = $params['title'];

        $cardboardColor->save();

        return [
            'cardboard_color_id' => $params['cardboard_color_id']
        ];
    }

    /**
     * Удаление цвета картона.
     * 
     * @param array $params
     * @return array
     */
    private function deleteCardboardColor($params)
    {
        $validator = Validator::make(
            $params,
            CardboardColor::$apiRules['delete'],
            CardboardColor::$apiRulesMessages
        );

        if ($validator->fails())
        {
            $this->errors['schema_error']['error_string'] = $validator->errors()->getMessages();
            return $this->errors['schema_error'];
        }

        $cardboardColor = CardboardColor::oneByExtId($params['cardboard_color_id']);

        if (!$cardboardColor)
        {
            $this->errors['not_deleted']['error_string']['cardboard_color_id'] = $this->errorsMessages['not_deleted'];
            return $this->errors['not_deleted'];
        }

        $cardboardColor->delete();

        return [
            'cardboard_color_id' => $params['cardboard_color_id']
        ];
    }
}