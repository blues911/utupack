<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Traits\SessionObjectTrait;
use App\Http\Traits\ChatTrait;
use App\Models\User;
use App\Models\Claim;
use App\Models\ClaimItem;
use App\Models\Order;
use App\Models\UserNomenclature;
use App\Models\File;
use App\Jobs\ExtClaimCreateJob;
use App\Jobs\ExtClaimUpdateJob;
use App\Jobs\ExtClaimDeleteJob;

class ClaimController extends Controller
{
    use SessionObjectTrait,
        ChatTrait;

    /**
     * Лимит на кол-во претензий в запросе.
     * 
     * @var int
     */
    protected $claimsLimit = 20;

    /**
     * Список претензий.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->forgetSessionObject('claim_client');
        
        $params = [];
        $params['clients'] = User::allClients();
        $params['status_types'] = Claim::$statusTypes;

        $params['filters'] = [];
        $params['filters']['offset'] = 0;
        $params['filters']['limit'] = $this->claimsLimit;
        $params['filters']['date_range'] = '';
        $params['filters']['status'] = '';
        $params['filters']['client']= '';

        if (!empty($request->query()))
        {
            $params['filters']['date_range'] = $request->query('date_range');
            $params['filters']['status'] = $request->query('status');

            if (user_account_type(['manager', 'admin']))
                $params['filters']['client']= $request->query('client');
        }

        $claims = Claim::allForAccountType($params['filters']);
        $claimsTotal = Claim::countForAccountType($params['filters']);

        return view('claim.index', [
            'params' => $params,
            'claims' => $claims,
            'claims_total' => $claimsTotal
        ]);
    }

    /**
     * Форма создания претензии.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = Auth::user();

        if (user_account_type(['manager', 'admin']))
            $client = User::findOrFail($this->getSessionObject('claim_client'));

        $params = [];
        $params['client'] = $client;
        $params['status_types'] = Claim::$statusTypes;
        $params['orders'] = Order::allForClaim($client->id);

        return view('claim.form', [
            'params' => $params,
            'claim' => false
        ]);
    }

    /**
     * Создать претензию.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxStore(Request $request)
    {
        $claim = new Claim();

        $claim->order_id = $request->order_id;
        $claim->common_claim = $request->common_claim;

        if ($request->has('common_decision'))
            $claim->common_decision = $request->common_decision;

        if ($request->has('status'))
            $claim->status = $request->status;

        $claim->date = db_datetime();

        $claim->save();

        if ($request->has('claim_items'))
        {
            foreach ($request->claim_items as $requestItem)
            {
                $claimItem = new ClaimItem();
    
                $claimItem->claim_id = $claim->id;
                $claimItem->nomenclature_id = $requestItem['nomenclature_id'];
                $claimItem->claim = $requestItem['claim'];
    
                if (array_key_exists('decision', $requestItem))
                    $claimItem->decision = $requestItem['decision'];
    
                $claimItem->cnt = $requestItem['cnt'];
    
                $claimItem->save();
    
                if (isset($requestItem['files']) && !empty($requestItem['files']))
                {
                    foreach ($requestItem['files'] as $claimItemFile)
                    {
                        $file = new File();
    
                        $file->object_id = $claimItem->id;
                        $file->object_type = 'claim_item';
                        $file->file = file_get_contents($claimItemFile);
                        $file->name = $claimItemFile->getClientOriginalName();
                        $file->mime_type = $claimItemFile->getClientMimeType();
                        $file->hash = random_hash(32);

                        $file->save();
                    }
                }
            }
        }

        ExtClaimCreateJob::dispatch($claim->id);

        $this->forgetSessionObject('claim_client');

        return response()->json([
            'id' => $claim->id
        ], 200);
    }

    /**
     * Форма редактирования претензии.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $claim = Claim::oneById($id);

        if (!$claim
            || $claim && user_account_type('client') && $claim->order->user_id != auth()->user()->id
            || $claim && user_account_type('manager') && $claim->order->client->manager_id != auth()->user()->id
            || $claim && $claim->status != 'new' && user_account_type('client'))
        {
            abort(404);
        }

        if ($claim->ext_id == null)
            return redirect('claim/show/' . $claim->id);

        $this->readChat('claim', $id);

        $params = [];
        $params['client'] = $claim->order->client;
        $params['status_types'] = Claim::$statusTypes;
        $params['orders'] = Order::allForClaim($claim->order->user_id);

        return view('claim.form', [
            'params' => $params,
            'claim' => $claim
        ]);
    }

    /**
     * Обновить претензию.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return json
     */
    public function ajaxUpdate(Request $request, $id)
    {
        $claim = Claim::find($id);

        $claim->order_id = $request->order_id;
        $claim->common_claim = $request->common_claim;

        if ($request->has('common_decision'))
            $claim->common_decision = $request->common_decision;

        if ($request->has('status'))
            $claim->status = $request->status;

        $claim->save();

        if ($request->has('claim_items'))
        {
            foreach ($request->claim_items as $key => $requestItem)
            {
                $claimItem = ClaimItem::find($key);
    
                if ($claimItem)
                {
                    $claimItem->claim_id = $claim->id;
                    $claimItem->nomenclature_id = $requestItem['nomenclature_id'];
                    $claimItem->claim = $requestItem['claim'];
    
                    if (array_key_exists('decision', $requestItem))
                        $claimItem->decision = $requestItem['decision'];
    
                    $claimItem->cnt = $requestItem['cnt'];
                }
                else
                {
                    $claimItem = new ClaimItem();
    
                    $claimItem->claim_id = $claim->id;
                    $claimItem->nomenclature_id = $requestItem['nomenclature_id'];
                    $claimItem->claim = $requestItem['claim'];
    
                    if (array_key_exists('decision', $requestItem))
                        $claimItem->decision = $requestItem['decision'];
    
                    $claimItem->cnt = $requestItem['cnt'];
                }
    
                $claimItem->save();

                $deleteFilesHash = json_decode($requestItem['delete_files'], true);

                if (!empty($deleteFilesHash))
                {
                    foreach ($deleteFilesHash as $deleteFileHash)
                    {
                        $file = File::oneByHash($deleteFileHash);

                        if ($file) $file->delete();
                    }
                }

                if (isset($requestItem['files']) && !empty($requestItem['files']))
                {
                    foreach ($requestItem['files'] as $claimItemFile)
                    {
                        $file = new File();
    
                        $file->object_id = $claimItem->id;
                        $file->object_type = 'claim_item';
                        $file->file = file_get_contents($claimItemFile);
                        $file->name = $claimItemFile->getClientOriginalName();
                        $file->mime_type = $claimItemFile->getClientMimeType();
                        $file->hash = random_hash(32);
                        
                        $file->save();
                    }
                }
            }
        }

        $itemsDeletedIds = json_decode($request->claim_items_deleted, true);

        if (!empty($itemsDeletedIds))
        {
            foreach ($itemsDeletedIds as $itemDeletedId)
            {
                $claimItem = ClaimItem::oneById($itemDeletedId);

                $claimItem->files()->delete();
                $claimItem->delete();
            }
        }

        // TODO: открыть когда будет готова сторона 1С
        // ExtClaimUpdateJob::dispatch($claim->id);

        return response()->json([
            'id' => $claim->id
        ], 200);
    }

    /**
     * Просмотр претензии.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $claim = Claim::oneById($id);

        if (!$claim
            || $claim && user_account_type('client') && $claim->order->user_id != auth()->user()->id
            || $claim && user_account_type('manager') && $claim->order->client->manager_id != auth()->user()->id)
        {
            abort(404);
        }

        $this->readChat('claim', $id);

        return view('claim.view', [
            'claim' => $claim
        ]);
    }

    /**
     * Удалить претензию.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $claim = Claim::oneById($id);

        foreach ($claim->chat as $claimChat)
        {
            $claimChat->files()->delete();
            $claimChat->delete();
        }

        foreach ($claim->items as $claimItem)
        {
            $claimItem->files()->delete();
            $claimItem->delete();
        }

        // TODO: открыть когда будет готова сторона 1С
        // ExtClaimDeleteJob::dispatch($claim->id);

        $claim->delete();

        return redirect('claim/list');
    }

    /**
     * Запомнить user_id для новой претензии.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxRememberClient(Request $request)
    {
        $params = $request->except('_token');
        $this->putSessionObject('claim_client', $params['client_id']);
 
        return response()->json([], 200);
    }

    /**
     * Валидация претензии и ее позиций.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxValidateClaim(Request $request)
    {
        $errors = [];
        $claimInput = [];
        $claimItemsInput = [];

        $claimInput = $request->except(['_token']);

        if (!$request->has('claim_items'))
            $claimInput['claim_items'] = null;
        else
            $claimItemsInput = $request->except(['_token', 'user_id', 'order_id', 'common_claim']);

        $claimValidator = Validator::make(
            $claimInput,
            Claim::$rules,
            Claim::$rulesMessages
        );
        
        if ($claimValidator->fails())
            $errors['claim'] = $claimValidator->errors()->getMessages();
        
        if (!empty($claimItemsInput))
        {
            foreach ($claimItemsInput['claim_items'] as $k => $v)
            {
                $claimItemsValidator = Validator::make(
                    $v,
                    ClaimItem::$rules,
                    ClaimItem::$rulesMessages
                );

                if ($claimItemsValidator->fails())
                    $errors['claim_items'][$k] = $claimItemsValidator->errors()->getMessages();
            }
        }

        if (!empty($errors))
        {
            if (isset($errors['claim_items']))
                $errors['claim']['common'][] = 'Ошибки в полях позиций.';

            return response()->json([
                'errors' => $errors
            ], 422);
        }

        return response()->json([], 200);
    }

    /**
     * Подгрузка списка претензий.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function ajaxLoadMore(Request $request)
    {
        $params = $request->except('_token');
        $params['limit'] = $this->claimsLimit;

        $claims = Claim::allForAccountType($params);
        $claimsTotal = Claim::countForAccountType($params);

        return response()->json([
            'html' => view('claim.inc._claim_items', [
                'claims' => $claims
            ])->render(),
            'claims_total' => $claimsTotal
        ], 200);
    }
}