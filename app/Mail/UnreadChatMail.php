<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnreadChatMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new message instance.
     *
     * @param array $chat
     * @return void
     */
    public function __construct($chat)
    {
        $this->queue = 'mail';
        $this->queueData['chat'] = $chat;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_FROM'))
            ->subject('Новые сообщения')
            ->view('email.unread_chat')
            ->text('email.unread_chat_plain')
            ->with([
                'items' => $this->queueData['chat']
            ]);
    }
}
