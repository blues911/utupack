<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewClaimMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new message instance.
     *
     * @param \App\Models\Claim $claim
     * @return void
     */
    public function __construct($claim)
    {
        $this->queue = 'mail';
        $this->queueData['claim'] = $claim;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_FROM'))
            ->subject('Новая претензия от клиента ' . $this->queueData['claim']->order->client->name)
            ->view('email.new_claim')
            ->text('email.new_claim_plain')
            ->with([
                'claim' => $this->queueData['claim'],
            ]);
    }
}
