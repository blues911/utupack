<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewSampleMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new message instance.
     *
     * @param \App\Models\Sample $sample
     * @return void
     */
    public function __construct($sample)
    {
        $this->queue = 'mail';
        $this->queueData['sample'] = $sample;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_FROM'))
            ->subject('Новый образец от клиента ' . $this->queueData['sample']->client->name)
            ->view('email.new_sample')
            ->text('email.new_sample_plain')
            ->with([
                'sample' => $this->queueData['sample']
            ]);
    }
}
