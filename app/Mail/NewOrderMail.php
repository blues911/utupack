<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new message instance.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function __construct($order)
    {
        $this->queue = 'mail';
        $this->queueData['order'] = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(env('MAIL_FROM'))
            ->subject('Новая заявка от клиента ' . $this->queueData['order']->client->name)
            ->view('email.new_order')
            ->text('email.new_order_plain')
            ->with([
                'order' => $this->queueData['order']
            ]);
    }
}
