<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client as HttpClient;
use App\Models\Sample;
use App\Models\ApiLog;
use App\Models\User;
use App\Mail\NewSampleMail;

class ExtSampleCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $sample = Sample::oneById($id);
          
        $this->queue = 'ext_request';

        $this->queueData['object_type'] = 'sample';
        $this->queueData['object_id'] = $sample->id;
        $this->queueData['request']['method'] = 'create';
        $this->queueData['request']['params'] = $sample->serrializeForApi();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // TODO: открыть когда будет готова сторона 1С

        // $httpClient = new HttpClient(config('app.http_client'));

        // $request = null;
        // $response = null;
        // $exception = null;

        // $timeStart = microtime(true);

        // try
        // {
        //     $res = $httpClient->post('samples', ['json' => $this->queueData['request']]);
        //     $response = json_decode($res->getBody()->getContents(), true);

        //     if (isset($response['sample_id']) && isset($response['product_code']))
        //     {
        //         $sample = Sample::oneById($this->queueData['object_id']);
        //         $sample->ext_id = $response['sample_id'];
        //         $sample->product_code = $response['product_code'];
        //         $sample->save();

        //         $user = User::oneById($sample->user_id);
        //         if ($user->clientManager && $user->clientManager->email)
        //         {
        //             Mail::to($user->clientManager->email)
        //                 ->queue(new NewSampleMail($sample));
        //         }
        //     }
        // }
        // catch (\Exception $error)
        // {
        //     $exception = $error;
        // }

        // $timeEnd = microtime(true);
        // $timeElapsed = $timeEnd - $timeStart;

        // $request = json_encode($this->queueData['request']);
        // $response = ($response) ? json_encode($response, JSON_UNESCAPED_UNICODE) : $response;

        // ApiLog::create([
        //     'type' => 'out',
        //     'url' => env('EXT_API_URL') . 'samples',
        //     'request' => $request,
        //     'response' => $response,
        //     'exception' => $exception,
        //     'time_elapsed' => $timeElapsed,
        //     'date' => db_datetime()
        // ]);

        // if ($exception)
        // {
        //     $this->fail($exception);
        // }


        // TODO: убрать когда будет готова сторона 1С
        $sample = Sample::oneById($this->queueData['object_id']);
        $sample->ext_id = $sample->id;
        $sample->product_code = $sample->id;
        $sample->save();

        $user = User::oneById($sample->user_id);
        if ($user->clientManager && $user->clientManager->email)
        {
            Mail::to($user->clientManager->email)
                ->queue(new NewSampleMail($sample));
        }
    }
}
