<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client as HttpClient;
use App\Models\Claim;
use App\Models\ApiLog;

class ExtClaimDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $claim = Claim::oneById($id);
        
        $this->queue = 'ext_request';

        $this->queueData['object_type'] = 'claim';
        $this->queueData['object_id'] = $claim->id;
        $this->queueData['request']['method'] = 'delete';
        $this->queueData['request']['params'] = ['claim_id' => $claim->ext_id];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $httpClient = new HttpClient(config('app.http_client'));

        $request = null;
        $response = null;
        $exception = null;

        $timeStart = microtime(true);

        try
        {
            $res = $httpClient->post('claims', ['json' => $this->queueData['request']]);
            $response = $res->getBody()->getContents();
        }
        catch (\Exception $error)
        {
            $exception = $error;
        }

        $timeEnd = microtime(true);
        $timeElapsed = $timeEnd - $timeStart;

        $request = json_encode($this->queueData['request'], JSON_UNESCAPED_UNICODE);

        ApiLog::create([
            'type' => 'out',
            'url' => env('EXT_API_URL') . 'claims',
            'request' => $request,
            'response' => $response,
            'exception' => $exception,
            'time_elapsed' => $timeElapsed,
            'date' => db_datetime()
        ]);

        if ($exception)
        {
            $this->fail($exception);
        }
    }
}
