<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client as HttpClient;
use App\Models\Chat;
use App\Models\ApiLog;

class ExtChatAddJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $chat = Chat::oneById($id);

        $this->queue = 'ext_request';

        $this->queueData['object_type'] = 'chat';
        $this->queueData['object_id'] = $chat->id;
        $this->queueData['request']['method'] = 'add';
        $this->queueData['request']['params'] = $chat->serrializeForApi();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $httpClient = new HttpClient(config('app.http_client'));

        $request = null;
        $response = null;
        $exception = null;

        $timeStart = microtime(true);

        try
        {
            $res = $httpClient->post('chats', ['json' => $this->queueData['request']]);
            $response = json_decode($res->getBody()->getContents(), true);

            if (isset($response['message_id']))
            {
                $chat = Chat::oneById($this->queueData['object_id']);
                $chat->ext_id = $response['message_id'];
                $chat->save();
            }
        }
        catch (\Exception $error)
        {
            $exception = $error;
        }

        $timeEnd = microtime(true);
        $timeElapsed = $timeEnd - $timeStart;

        $request = json_encode($this->queueData['request']);
        $response = ($response) ? json_encode($response, JSON_UNESCAPED_UNICODE) : $response;

        ApiLog::create([
            'type' => 'out',
            'url' => env('EXT_API_URL') . 'chats',
            'request' => $request,
            'response' => $response,
            'exception' => $exception,
            'time_elapsed' => $timeElapsed,
            'date' => db_datetime()
        ]);

        if ($exception)
        {
            $this->fail($exception);
        }
    }
}
