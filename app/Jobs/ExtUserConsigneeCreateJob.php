<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client as HttpClient;
use App\Models\UserConsignee;
use App\Models\ApiLog;

class ExtUserConsigneeCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $userConsignee = UserConsignee::find($id);

        $this->queue = 'ext_request';

        $this->queueData['object_type'] = 'user_consignee';
        $this->queueData['object_id'] = $userConsignee->id;
        $this->queueData['request']['method'] = 'create';
        $this->queueData['request']['params'] = $userConsignee->serrializeForApi();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $httpClient = new HttpClient(config('app.http_client'));

        $request = null;
        $response = null;
        $exception = null;

        $timeStart = microtime(true);

        try
        {
            $res = $httpClient->post('consignees', ['json' => $this->queueData['request']]);
            $response = json_decode($res->getBody()->getContents(), true);

            if (isset($response['consignee_id']))
            {
                $userConsignee = UserConsignee::find($this->queueData['object_id']);
                $userConsignee->ext_id = $response['consignee_id'];
                $userConsignee->save();
            }
        }
        catch (\Exception $error)
        {
            $exception = $error;
        }

        $timeEnd = microtime(true);
        $timeElapsed = $timeEnd - $timeStart;

        $request = json_encode($this->queueData['request']);
        $response = ($response) ? json_encode($response, JSON_UNESCAPED_UNICODE) : $response;

        ApiLog::create([
            'type' => 'out',
            'url' => env('EXT_API_URL') . 'consignees',
            'request' => $request,
            'response' => $response,
            'exception' => $exception,
            'time_elapsed' => $timeElapsed,
            'date' => db_datetime()
        ]);

        if ($exception)
        {
            $this->fail($exception);
        }
    }
}
