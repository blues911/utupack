<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client as HttpClient;
use App\Models\Claim;
use App\Models\ApiLog;
use App\Models\User;
use App\Mail\NewClaimMail;

class ExtClaimCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queueData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $claim = Claim::oneById($id);
        
        $this->queue = 'ext_request';

        $this->queueData['object_type'] = 'claim';
        $this->queueData['object_id'] = $claim->id;
        $this->queueData['request']['method'] = 'create';
        $this->queueData['request']['params'] = $claim->serrializeForApi();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // TODO: открыть когда будет готова сторона 1С

        // $httpClient = new HttpClient(config('app.http_client'));

        // $request = null;
        // $response = null;
        // $exception = null;

        // $timeStart = microtime(true);

        // try
        // {
        //     $res = $httpClient->post('claims', ['json' => $this->queueData['request']]);
        //     $response = json_decode($res->getBody()->getContents(), true);

        //     if (isset($response['claim_id']) && isset($response['product_code']))
        //     {
        //         $claim = Claim::oneById($this->queueData['object_id']);
        //         $claim->ext_id = $response['claim_id'];
        //         $claim->product_code = $response['product_code'];
        //         $claim->save();

        //         $user = User::oneById($claim->order->user_id);
        //         if ($user->clientManager && $user->clientManager->email)
        //         {
        //             Mail::to($user->clientManager->email)
        //                 ->queue(new NewClaimMail($claim));
        //         }
        //     }
        // }
        // catch (\Exception $error)
        // {
        //     $exception = $error;
        // }

        // $timeEnd = microtime(true);
        // $timeElapsed = $timeEnd - $timeStart;

        // $request = json_encode($this->queueData['request']);
        // $response = ($response) ? json_encode($response, JSON_UNESCAPED_UNICODE) : $response;

        // ApiLog::create([
        //     'type' => 'out',
        //     'url' => env('EXT_API_URL') . 'claims',
        //     'request' => $request,
        //     'response' => $response,
        //     'exception' => $exception,
        //     'time_elapsed' => $timeElapsed,
        //     'date' => db_datetime()
        // ]);

        // if ($exception)
        // {
        //     $this->fail($exception);
        // }

        // TODO: убрать когда будет готова сторона 1С
        $claim = Claim::oneById($this->queueData['object_id']);
        $claim->ext_id = $claim->id;
        $claim->product_code = $claim->id;
        $claim->save();
        $user = User::oneById($claim->order->user_id);
        if ($user->clientManager && $user->clientManager->email)
        {
            Mail::to($user->clientManager->email)
                ->queue(new NewClaimMail($claim));
        }
    }
}
