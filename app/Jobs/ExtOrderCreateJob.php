<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client as HttpClient;
use App\Models\Order;
use App\Models\ApiLog;
use App\Models\User;
use App\Mail\NewOrderMail;

class ExtOrderCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queueData = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $order = Order::oneById($id);
        
        $this->queue = 'ext_request';

        $this->queueData['object_type'] = 'order';
        $this->queueData['object_id'] = $order->id;
        $this->queueData['request']['method'] = 'create';
        $this->queueData['request']['params'] = $order->serrializeForApi();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $httpClient = new HttpClient(config('app.http_client'));

        $request = null;
        $response = null;
        $exception = null;

        $timeStart = microtime(true);

        try
        {
            $res = $httpClient->post('orders', ['json' => $this->queueData['request']]);
            $response = json_decode($res->getBody()->getContents(), true);

            if (isset($response['order_id']) && isset($response['product_code']))
            {
                $order = Order::oneById($this->queueData['object_id']);
                $order->ext_id = $response['order_id'];
                $order->product_code = $response['product_code'];
                $order->save();

                $user = User::oneById($order->user_id);
                if ($user->clientManager && $user->clientManager->email)
                {
                    Mail::to($user->clientManager->email)
                        ->queue(new NewOrderMail($order));
                }
            }
        }
        catch (\Exception $error)
        {
            $exception = $error;
        }

        $timeEnd = microtime(true);
        $timeElapsed = $timeEnd - $timeStart;

        $request = json_encode($this->queueData['request']);
        $response = ($response) ? json_encode($response, JSON_UNESCAPED_UNICODE) : $response;

        ApiLog::create([
            'type' => 'out',
            'url' => env('EXT_API_URL') . 'orders',
            'request' => $request,
            'response' => $response,
            'exception' => $exception,
            'time_elapsed' => $timeElapsed,
            'date' => db_datetime()
        ]);

        if ($exception)
        {
            $this->fail($exception);
        }
    }
}
