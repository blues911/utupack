<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
      $urlParams = explode('/', url()->current());

      if ($this->isHttpException($exception) && $exception->getStatusCode() == 404)
      {
          return response()->view('error', [
              'code' => 404,
              'message' => $exception->getMessage() ?: 'Not found.'
          ], 404);
      }

      if ($this->isHttpException($exception) && $exception->getStatusCode() == 412)
      {
          return response()->view('error', [
              'code' => 412,
              'message' => $exception->getMessage() ?: 'Page expired.'
          ], 412);
      }

      if ($this->isHttpException($exception) && $exception->getStatusCode() == 500)
      {
          return response()->view('error', [
              'code' => 500,
              'message' => $exception->getMessage() ?: 'Server error.'
          ], 500);
      }

      return parent::render($request, $exception);
    }
}
