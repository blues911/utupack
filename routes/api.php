<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('users', 'Api\UserController@handle');
Route::post('agreements', 'Api\UserAgreementController@handle');
Route::post('consignees', 'Api\UserConsigneeController@handle');
Route::post('nomenclatures', 'Api\UserNomenclatureController@handle');
Route::post('settings', 'Api\SettingsController@handle');
Route::post('orders', 'Api\OrderController@handle');
Route::post('samples', 'Api\SampleController@handle');
Route::post('claims', 'Api\ClaimController@handle');
Route::post('chats', 'Api\ChatController@handle');
Route::post('nomenclature_types', 'Api\NomenclatureTypeController@handle');
Route::post('cardboard_types', 'Api\CardboardTypeController@handle');
Route::post('cardboard_profiles', 'Api\CardboardProfileController@handle');
Route::post('cardboard_colors', 'Api\CardboardColorController@handle');