<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('', 'order/list');
Route::get('shared/read-file/{hash}', 'SharedController@readFile');
Route::get('shared/terms-of-use', 'SharedController@termsOfuse');

// TODO: удалить shared/fake-api
Route::post('shared/fake-api/orders', 'SharedController@fakeApi');
Route::post('shared/fake-api/samples', 'SharedController@fakeApi');
Route::post('shared/fake-api/claims', 'SharedController@fakeApi');
Route::post('shared/fake-api/chats', 'SharedController@fakeApi');
Route::post('shared/fake-api/consignees', 'SharedController@fakeApi');

// Авторизация
Route::get('login', 'AuthController@showLoginForm')->name('login');
Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');

// Личный кабинет
Route::group(['middleware' => 'auth'], function() {

    // Заявки
    Route::group(['prefix' => 'order'], function() {
        Route::redirect('', 'order/list');
        Route::get('list', 'OrderController@index');
        Route::get('create', 'OrderController@create');
        Route::get('edit/{id}', 'OrderController@edit');
        Route::post('copy/{id}', 'OrderController@copy');
        Route::get('show/{id}', 'OrderController@show');
        Route::post('delete/{id}', 'OrderController@delete');
        // ajax
        Route::post('ajax-store', 'OrderController@ajaxStore');
        Route::post('ajax-update/{id}', 'OrderController@ajaxUpdate');
        Route::post('ajax-remember-client', 'OrderController@ajaxRememberClient');
        Route::post('ajax-filter-nomenclatures', 'OrderController@ajaxFilterNomenclatures');
        Route::post('ajax-validate-order', 'OrderController@ajaxValidateOrder');
        Route::post('ajax-validate-order-item', 'OrderController@ajaxValidateOrderItem');
        Route::post('ajax-load-more', 'OrderController@ajaxLoadMore');
    });

    // Образцы
    Route::group(['prefix' => 'sample'], function() {
        Route::redirect('', 'sample/list');
        Route::get('list', 'SampleController@index');
        Route::get('create', 'SampleController@create');
        Route::get('edit/{id}', 'SampleController@edit');
        Route::post('copy/{id}', 'SampleController@copy');
        Route::get('show/{id}', 'SampleController@show');
        Route::post('delete/{id}', 'SampleController@delete');
        // ajax
        Route::post('ajax-store', 'SampleController@ajaxStore');
        Route::post('ajax-update/{id}', 'SampleController@ajaxUpdate');
        Route::post('ajax-remember-client', 'SampleController@ajaxRememberClient');
        Route::post('ajax-validate-sample', 'SampleController@ajaxValidateSample');
        Route::post('ajax-load-more', 'SampleController@ajaxLoadMore');
    });

    // Претензии
    Route::group(['prefix' => 'claim'], function() {
        Route::redirect('', 'claim/list');
        Route::get('list', 'ClaimController@index');
        Route::get('create', 'ClaimController@create');
        Route::get('edit/{id}', 'ClaimController@edit');
        Route::get('show/{id}', 'ClaimController@show');
        Route::post('delete/{id}', 'ClaimController@delete');
        // ajax
        Route::post('ajax-store', 'ClaimController@ajaxStore');
        Route::post('ajax-update/{id}', 'ClaimController@ajaxUpdate');
        Route::post('ajax-remember-client', 'ClaimController@ajaxRememberClient');
        Route::post('ajax-validate-claim', 'ClaimController@ajaxValidateClaim');
        Route::post('ajax-load-more', 'ClaimController@ajaxLoadMore');
    });

    // Профиль
    Route::group(['prefix' => 'profile', 'middleware' => 'client'], function() {
        Route::redirect('', 'profile/info');
        Route::get('info', 'ProfileController@info');
        Route::get('agreements', 'ProfileController@agreements');
        Route::get('consignees', 'ProfileController@consignees');
        Route::get('nomenclatures', 'ProfileController@nomenclatures');
        // ajax
        Route::post('ajax-validate-consignee', 'ProfileController@ajaxValidateConsignee');
        Route::post('ajax-handle-consignee', 'ProfileController@ajaxHandleConsignee');
        Route::post('ajax-check-consignee-extid', 'ProfileController@ajaxCheckConsigneeExtId');
    });

    // Чат
    Route::group(['prefix' => 'chat'], function() {
        // ajax
        Route::post('ajax-store', 'ChatController@ajaxStore');
        Route::post('ajax-get-unread', 'ChatController@ajaxGetUnread');
    });
});
