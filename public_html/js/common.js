var csrf = $("meta[name=csrf_token]").attr('content');
var fileExt = ['jpg', 'jpeg', 'png', 'tiff', 'bmp', 'doc', 'docx', 'xls', 'xlsx', 'pdf', 'zip', 'rar'];

function plural(num, words) {
    var wforms = words.split('|');
    if (wforms.length == 3) {
        var plural = ((num % 10 == 1 && num % 100 != 11) ? 0 : ((num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)) ? 1 : 2));
        return wforms[plural];
    }
    else return '';
}

function to_agreement_date(date)
{
    var date = new Date(date);
    var monthNames = {
        '0': 'января',
        '1': 'февраля',
        '2': 'марта',
        '3': 'апреля',
        '4': 'мая',
        '5': 'июня',
        '6': 'июля',
        '7': 'августа',
        '8': 'сентября',
        '9': 'октября',
        '10': 'ноября',
        '11': 'декабря'
    };

    var Y,m,d;
    Y = date.getFullYear();
    m = date.getMonth();
    d = date.getDate();

    return d + ' ' + monthNames[m] + ' ' + Y + ' года';
}

function textarea_resize(event, line_height, min_line_count) {
    var min_line_height = min_line_count * line_height;
    var obj = event.target;
    var div = document.getElementById(obj.id + '_div');
    div.innerHTML = obj.value;
    var obj_height = div.offsetHeight;
    if (event.keyCode == 13)
        obj_height += line_height;
    else if (obj_height < min_line_height)
        obj_height = min_line_height;
    obj.style.height = obj_height + 5 + 'px';
}

$("select.styled").selectBoxIt({});

$('[data-toggle=\'tooltip\']').tooltip({container: 'body',trigger: 'hover'});

$('body').on('keyup','.textarea-magic-height', function(e) {
    textarea_resize(event, 20, 1);
});

$('body').on('focusout','textarea, input', function(e) {
    e.preventDefault();
    if ($(this).val() !='') {
        $(this).addClass('not-empty');
    } else {
        $(this).removeClass('not-empty');
    }        
});

// Всплывающая менюшка у элементов
$('body').on('click','.js-show-popup-menu', function(e) {
    e.preventDefault();
    if ($(this).parent().hasClass('active')) {
        $(this).parent().removeClass('active');
    } else {
        $('.popup-menu-container').removeClass('active');
        $(this).parent().addClass('active');
    }        
});

$(document).on('click', function(e) {
    if (!$(e.target).closest(".popup-menu-container").length) {
        $('.popup-menu-container').removeClass('active');
    }
    if (!$(e.target).closest(".order-positions-cell").length) {
        $('.order-positions-cell').removeClass('active');
    }
    if ((!$(e.target).closest(".date-container").length) && (!$(e.target).closest(".ui-corner-all").length)) {
        $('.date-container').removeClass('active');
    }     
    e.stopPropagation();
});

$('body').on('click', '.date-result', function(e) {
    e.preventDefault();
    if ($(this).parent().hasClass('active')){
        $(this).parent().removeClass('active');
        $('.datepicker-input').trigger('change');
    } else {
        $('.date-container').removeClass('active');
        $(this).parent().addClass('active');
    }        
});

// Диапазон дат
$('.datepicker-range').datepicker({
    dateFormat: 'dd.mm.yy',
    range: 'period',
    onSelect: function(dateText, inst, extensionRange) {
        var value = extensionRange.startDateText + ' – ' + extensionRange.endDateText;
        $('.date-result').removeClass('empty');
        $('.date-result').html(value);
        $('.datepicker-input').val(value);
        if (extensionRange.startDateText != extensionRange.endDateText) {
            $('.date-container').removeClass('active');
            $('input[name="filter[date_range]"]').trigger('change');
        }
    }
});

// Одна дата
$('.datepicker').datepicker({
    dateFormat: 'dd.mm.yy',
    onSelect: function() {
        $(this).parents('.date-popup').siblings('.date-result').html($(this).val());
        $(this).parents('.date-popup').siblings('.date-result').removeClass('empty');
        $(this).parent().find('.datepicker-input').val($(this).val());
        $('.date-container').removeClass('active');
    }
});

// Сброс даты
$(document).on('click', '.date-reset', function(e){
    e.preventDefault();
    if ($(this).next('.datepicker-range').length != 0) {
        $(this).parents('.date-container').find('.datepicker-input').val('');
        $(this).parents('.date-container').find('.date-result').addClass('empty');
        $(this).parents('.date-container').find('.date-result').text('выберите период');
        $('input[name="filter[date_range]"]').trigger('change');
    } else {
        $(this).parents('.date-container').find('.datepicker-input').val('');
        $(this).parents('.date-container').find('.date-result').addClass('empty');
        $(this).parents('.date-container').find('.date-result').text('——.——.————');
        $('.date-container').removeClass('active');
    }
});

// Подстановка одной даты
$('.datepicker').each(function(){
    var date = $(this).siblings('.datepicker-input').val();
    if (date != '') {
        $(this).datepicker('setDate', date);
    }
});

// Подстановка диапазона дат
$('.datepicker-range').each(function(){
    var date = $(this).siblings('.datepicker-input').val();
    if (date != '') {
        date = date.replace(/\s(.*)\s/g, ' ').split(' ');
        $(this).datepicker('setDate', [date[0], date[1]]);
    }
});

// Выход из ЛК
$('#logout').on('click', function(e){
    e.preventDefault();
    $(this).next('form').submit();
});