<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserNomenclatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'ext_id' => '1000000000',
                'user_id' => 3,
                'title' => strtoupper('nomenclature_01'),
                'sku' => 'BKGLOIUGJ',
                'print' => 1,
                'area' => '10.2',
                'nomenclature_type_id' => 1,
                'cardboard_type_id' => 2,
                'cardboard_profile_id' => 1,
                'cardboard_color_id' => 3,
            ],
            [
                'id' => 2,
                'ext_id' => '2000000000',
                'user_id' => 3,
                'title' => strtoupper('nomenclature_02'),
                'sku' => '48FK124536',
                'print' => 0,
                'area' => '4.2',
                'nomenclature_type_id' => 2,
                'cardboard_type_id' => 1,
                'cardboard_profile_id' => 3,
                'cardboard_color_id' => 2,
            ],
        ];

        foreach ($items as $item)
        {
            DB::table('user_nomenclature')->insert($item);
        }
    }
}
