<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserConsigneeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'ext_id' => '1000000000',
                'user_id' => 3,
                'title' => strtoupper('consignee_01'),
                'inn' => '0215478125',
                'ogrn' => '2150215486'
            ],
            [
                'id' => 2,
                'ext_id' => '2000000000',
                'user_id' => 3,
                'title' => strtoupper('consignee_02'),
                'inn' => '5102365489',
                'ogrn' => '0214589632'
            ],
        ];

        foreach ($items as $item)
        {
            DB::table('user_consignee')->insert($item);
        }
    }
}
