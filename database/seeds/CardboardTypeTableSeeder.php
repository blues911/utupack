<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CardboardTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'ext_id' => '1000000000',
                'title' => strtoupper('cardboard_type_01'),
            ],
            [
                'id' => 2,
                'ext_id' => '2000000000',
                'title' => strtoupper('cardboard_type_02'),
            ],
            [
                'id' => 3,
                'ext_id' => '3000000000',
                'title' => strtoupper('cardboard_type_03'),
            ],
        ];

        foreach ($items as $item)
        {
            DB::table('cardboard_type')->insert($item);
        }
    }
}
