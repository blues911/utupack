<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserAgreementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'ext_id' => '1000000000',
                'user_id' => 3,
                'buyer_ext_id' => '1000000000',
                'buyer_title' => strtoupper('buyer_01'),
                'number' => '05J9JY8UR7',
                'date' => db_datetime(),
                'min_execution' => 0,
                'info' => 'Agreement info',
            ],
            [
                'id' => 2,
                'ext_id' => '2000000000',
                'user_id' => 3,
                'buyer_ext_id' => '2000000000',
                'buyer_title' => strtoupper('buyer_02'),
                'number' => 'K8UY09KG00',
                'date' => db_datetime(),
                'min_execution' => 2,
                'info' => 'Agreement info',
            ],
        ];

        foreach ($items as $item)
        {
            DB::table('user_agreement')->insert($item);
        }
    }
}
