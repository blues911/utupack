<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'id' => 1,
                'ext_id' => '1000000000',
                'login' => 'admin',
                'password' => Hash::make('admin'),
                'account_type' => 'admin',
                'name' => 'admin',
                'status' => 1,
                'manager_id' => null,
                'phone' => null,
                'email' => null
            ],
            [
                'id' => 2,
                'ext_id' => '2000000000',
                'login' => 'manager',
                'password' => Hash::make('manager'),
                'account_type' => 'manager',
                'name' => 'manager',
                'status' => 1,
                'manager_id' => null,
                'phone' => '77777777777',
                'email' => 'manager@gmail.com'
            ],
            [
                'id' => 3,
                'ext_id' => '3000000000',
                'login' => 'client',
                'password' => Hash::make('client'),
                'account_type' => 'client',
                'name' => 'client',
                'status' => 1,
                'manager_id' => 2,
                'phone' => null,
                'email' => null
            ],
        ];

        foreach ($items as $item)
        {
            DB::table('user')->insert($item);
        }
    }
}
