<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('claim_id')->unsigned()->nullable();
            $table->bigInteger('nomenclature_id')->unsigned()->nullable();
            $table->text('claim');
            $table->integer('cnt')->unsigned()->default(0);
            $table->text('decision')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_item');
    }
}
