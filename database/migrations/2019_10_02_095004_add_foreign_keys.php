<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function(Blueprint $table) {
            $table->foreign('manager_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('user_agreement', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('user_consignee', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('user_nomenclature', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('nomenclature_type_id')
                ->references('id')
                ->on('nomenclature_type')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('cardboard_type_id')
                ->references('id')
                ->on('cardboard_type')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('cardboard_profile_id')
                ->references('id')
                ->on('cardboard_profile')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('cardboard_color_id')
                ->references('id')
                ->on('cardboard_color')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('order', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('consignee_id')
                ->references('id')
                ->on('user_consignee')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('agreement_id')
                ->references('id')
                ->on('user_agreement')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('order_item', function(Blueprint $table) {
            $table->foreign('order_id')
                ->references('id')
                ->on('order')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('nomenclature_id')
                ->references('id')
                ->on('user_nomenclature')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('claim', function(Blueprint $table) {
            $table->foreign('order_id')
                ->references('id')
                ->on('order')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('claim_item', function(Blueprint $table) {
            $table->foreign('claim_id')
                ->references('id')
                ->on('claim')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('nomenclature_id')
                ->references('id')
                ->on('user_nomenclature')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('sample', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('consignee_id')
                ->references('id')
                ->on('user_consignee')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('agreement_id')
                ->references('id')
                ->on('user_agreement')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('nomenclature_type_id')
                ->references('id')
                ->on('nomenclature_type')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('cardboard_type_id')
                ->references('id')
                ->on('cardboard_type')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('cardboard_profile_id')
                ->references('id')
                ->on('cardboard_profile')
                ->onUpdate('cascade')
                ->onDelete('set null');
            $table->foreign('cardboard_color_id')
                ->references('id')
                ->on('cardboard_color')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });

        Schema::table('chat', function(Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function(Blueprint $table) {
            $table->dropForeign('user_manager_id_foreign');
        });

        Schema::table('user_agreement', function(Blueprint $table) {
            $table->dropForeign('user_agreement_user_id_foreign');
        });

        Schema::table('user_consignee', function(Blueprint $table) {
            $table->dropForeign('user_consignee_user_id_foreign');
        });

        Schema::table('user_nomenclature', function(Blueprint $table) {
            $table->dropForeign('user_nomenclature_user_id_foreign');
            $table->dropForeign('user_nomenclature_nomenclature_type_id_foreign');
            $table->dropForeign('user_nomenclature_cardboard_type_id_foreign');
            $table->dropForeign('user_nomenclature_cardboard_profile_id_foreign');
            $table->dropForeign('user_nomenclature_cardboard_color_id_foreign');
        });

        Schema::table('order', function(Blueprint $table) {
            $table->dropForeign('order_user_id_foreign');
            $table->dropForeign('order_consignee_id_foreign');
            $table->dropForeign('order_agreement_id_foreign');
        });

        Schema::table('order_item', function(Blueprint $table) {
            $table->dropForeign('order_item_order_id_foreign');
            $table->dropForeign('order_item_nomenclature_id_foreign');
        });

        Schema::table('claim', function(Blueprint $table) {
            $table->dropForeign('claim_order_id_foreign');
        });

        Schema::table('claim_item', function(Blueprint $table) {
            $table->dropForeign('claim_item_claim_id_foreign');
            $table->dropForeign('claim_item_nomenclature_id_foreign');
        });

        Schema::table('sample', function(Blueprint $table) {
            $table->dropForeign('sample_user_id_foreign');
            $table->dropForeign('sample_consignee_id_foreign');
            $table->dropForeign('sample_agreement_id_foreign');
            $table->dropForeign('sample_nomenclature_type_id_foreign');
            $table->dropForeign('sample_cardboard_type_id_foreign');
            $table->dropForeign('sample_cardboard_profile_id_foreign');
            $table->dropForeign('sample_cardboard_color_id_foreign');
        });

        Schema::table('chat', function(Blueprint $table) {
            $table->dropForeign('chat_user_id_foreign');
        });
    }
}
