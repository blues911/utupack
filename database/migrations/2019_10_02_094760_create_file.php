<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('object_id')->unsigned()->nullable();
            $table->enum('object_type', [
                    'order',
                    'sample',
                    'claim_item',
                    'chat',
                    'user'
                ])
                ->default('order');
            // $table->binary('file');   
            $table->string('name');
        });

        DB::statement("ALTER TABLE file ADD file LONGBLOB AFTER object_type");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
