<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->nullable();
            $table->enum('object_type', [
                    'order',
                    'sample',
                    'claim'
                ])
                ->default('order');
            $table->bigInteger('object_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->text('message');
            $table->dateTime('date');
            $table->tinyInteger('read_by_client')->unsigned()->default(0);
            $table->tinyInteger('read_by_manager')->unsigned()->default(0);
            $table->tinyInteger('read_by_admin')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat');
    }
}
