<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->string('product_code')->nullable();
        });

        Schema::table('sample', function (Blueprint $table) {
            $table->string('product_code')->nullable();
        });

        Schema::table('claim', function (Blueprint $table) {
            $table->string('product_code')->nullable();
        });

        Schema::table('user_nomenclature', function (Blueprint $table) {
            $table->string('product_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('product_code');
        });

        Schema::table('sample', function (Blueprint $table) {
            $table->dropColumn('product_code');
        });

        Schema::table('claim', function (Blueprint $table) {
            $table->dropColumn('product_code');
        });

        Schema::table('user_nomenclature', function (Blueprint $table) {
            $table->dropColumn('product_code');
        });
    }
}
