<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserConsignee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_consignee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('inn');
            $table->string('ogrn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_consignee');
    }
}
