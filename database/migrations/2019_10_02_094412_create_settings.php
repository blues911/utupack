<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('var')->primary();
            $table->text('value');
        });

        $items = [
            [
                'var' => 'default_execution_time',
                'value' => 0
            ],
            [
                'var' => 'min_production_volume',
                'value' => 0
            ]
        ];
        
        foreach ($items as $item)
        {
            DB::table('settings')->insert($item);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
