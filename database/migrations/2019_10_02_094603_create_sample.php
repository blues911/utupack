<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSample extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sample', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('buyer_ext_id', 16)->nullable();
            $table->bigInteger('consignee_id')->unsigned()->nullable();
            $table->bigInteger('agreement_id')->unsigned()->nullable();
            $table->string('request_number')->nullable();
            $table->enum('delivery_type', [
                    'delivery',
                    'pickup'
                ])
                ->default('delivery');
            $table->enum('making', [
                    'line',
                    'plotter'
                ])
                ->default('line');
            $table->json('sizes');
            $table->integer('cnt')->unsigned()->default(0);
            $table->text('comment')->nullable();
            $table->enum('status', [
                    'new',
                    'confirmed',
                    'production',
                    'ready',
                    'done',
                    'canceled'
                ])
                ->default('new');
            $table->dateTime('date');
            $table->bigInteger('nomenclature_type_id')->unsigned()->nullable();
            $table->bigInteger('cardboard_type_id')->unsigned()->nullable();
            $table->bigInteger('cardboard_profile_id')->unsigned()->nullable();
            $table->bigInteger('cardboard_color_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sample');
    }
}
