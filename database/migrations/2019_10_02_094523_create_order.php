<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('buyer_ext_id')->nullable();
            $table->bigInteger('consignee_id')->unsigned()->nullable();
            $table->bigInteger('agreement_id')->unsigned()->nullable();
            $table->string('request_number')->nullable();
            $table->enum('delivery_type', [
                    'delivery',
                    'pickup'
                ])
                ->default('delivery');
            $table->dateTime('desired_date')->nullable();
            $table->dateTime('ready_date')->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->text('comment')->nullable();
            $table->enum('status', [
                    'new',
                    'confirmed',
                    'production',
                    'ready',
                    'done',
                    'canceled'
                ])
                ->default('new');
            $table->dateTime('date');
            $table->decimal('amount', 12, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
