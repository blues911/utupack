<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNomenclature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_nomenclature', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('sku')->nullable();
            $table->tinyInteger('print')->unsigned()->default(0);
            $table->decimal('area', 10, 6);
            $table->bigInteger('nomenclature_type_id')->unsigned()->nullable();
            $table->bigInteger('cardboard_type_id')->unsigned()->nullable();
            $table->bigInteger('cardboard_profile_id')->unsigned()->nullable();
            $table->bigInteger('cardboard_color_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_nomenclature');
    }
}
