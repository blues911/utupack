<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned()->nullable();
            $table->bigInteger('nomenclature_id')->unsigned()->nullable();
            $table->string('address')->nullable();
            $table->integer('address_number')->unsigned()->nullable();
            $table->integer('cnt')->unsigned()->default(0);
            $table->integer('cnt_rule')->unsigned()->default(0);
            $table->integer('produced')->unsigned()->default(0);
            $table->text('comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
