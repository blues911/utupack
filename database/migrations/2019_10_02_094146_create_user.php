<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ext_id')->nullable();
            $table->string('login');
            $table->string('password'); 
            $table->string('remember_token')->nullable();
            $table->enum('account_type', [
                    'client',
                    'manager',
                    'admin'
                ])
                ->default('client');
            $table->string('name');
            $table->tinyInteger('status')->default(1);
            $table->bigInteger('manager_id')->unsigned()->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
