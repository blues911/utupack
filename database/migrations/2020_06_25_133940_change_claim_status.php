<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeClaimStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claim', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('claim', function (Blueprint $table) {
            $table->enum('status', [
                    'new',
                    'review',
                    'solved',
                    'canceled'
                ])
                ->default('new');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claim', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('claim', function (Blueprint $table) {
            $table->enum('status', [
                    'new',
                    'review',
                    'solved'
                ])
                ->default('new');
        });
    }
}
