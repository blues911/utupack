// Грузополучатели
if ($('#tpl-profile-consignees').length == 1) {
    // Закрытие модального окна
    $('#modal-consignee').on('hidden.bs.modal', function() {
        $('#modal-consignee').find('.modal-content').html('');
    });

    // Модальное окно создания грузополучателя
    $('#add-consignee').on('click', function(e) {
        e.preventDefault();
        var tpl = _.template($('#js-modal-consignee-content').html());

        $('#modal-consignee').find('.modal-content').html(tpl({
            title: 'Добавить грузополучателя',
            input: { id: '', user_id: $(this).data('user-id'), title: '', inn: '', ogrn: '' }
        }));

        $('#modal-consignee').modal('show');
    });

    // Модальное окно редатирования грузополучателя
    $(document).on('click', '.edit-consignee', function(e) {
        e.preventDefault();
        var item = $(this).data('item');
        var tpl = _.template($('#js-modal-consignee-content').html());

        $('#modal-consignee').find('.modal-content').html(tpl({
            title: 'Редактировать грузополучателя',
            input: item
        }));

        $.ajax({
            url: '/profile/ajax-check-consignee-extid',
            type: 'POST',
            data: { _token: csrf, consignee_id: item.id },
            dataType: 'JSON',
            success: function (result) {
                if (result.ext_id == null || result.ext_id == '') {
                    $('#modal-consignee').find('.modal-body').addClass('disabled-form');
                    $('#modal-consignee').find('.modal-body .errors').html(`
                        <div class="alert alert-warning">
                            Режим редактирования недоступен. Отсутствует № грузополучателя.
                        </div>
                    `);
                }
            
                $('#modal-consignee').modal('show');
            }
        });
    });

    // Добавление/редактирование грузополучателя
    $(document).on('click', '#save-consignee', function(e) {
        e.preventDefault();
        var errorsBlock = $(document).find('#modal-consignee .errors');
        var form = {
            _token: csrf,
            id: $(document).find('input[name="id"]').val(),
            user_id: $(document).find('input[name="user_id"]').val(),
            title: $(document).find('input[name="title"]').val(),
            inn: $(document).find('input[name="inn"]').val(),
            ogrn: $(document).find('input[name="ogrn"]').val()
        };

        form.action = (form.id != '') ? 'update' : 'store';

        errorsBlock.html('');

        $.ajax({
            url: '/profile/ajax-validate-consignee',
            type: 'POST',
            data: form,
            dataType: 'JSON',
            success: function (result) {
                $.ajax({
                    url: '/profile/ajax-handle-consignee',
                    type: 'POST',
                    data: form,
                    dataType: 'JSON',
                    success: function (result) {
                        var tpl = _.template($('#js-consignee-item').html());
            
                        if (form.id != '')
                            $(document).find('div[data-id="' + result.id + '"] .consignee-item_name').text(result.title);
                        else
                            $('.consignees').append(tpl({ item: result }));
            
                        $('#modal-consignee').modal('hide');
                    }
                });
            },
            error: function (err) {
                var tpl = _.template($('#js-validation-errors').html());

                if (err.status == 422)
                    errorsBlock.html(tpl({ items: err.responseJSON.errors}));
                else
                    errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
            }
        });
    });

    // Удаление грузополучателя
    $(document).on('click', '.remove-consignee', function(e) {
        e.preventDefault();
        var item = $(this).data('item');

        $.ajax({
            url: '/profile/ajax-handle-consignee',
            type: 'POST',
            data: {
                _token: csrf,
                action: 'delete',
                id: item.id
            },
            dataType: 'JSON',
            success: function (result) {
                $(document).find('.consignees div[data-id="' + item.id + '"]').remove();
            }
        });
    });
}