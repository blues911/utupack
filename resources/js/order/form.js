// Форма заявки
if ($('#tpl-order-form').length == 1) {
    var orderFile = {};

    function modaFilterNomenclatures() {
        var data = {
            _token: csrf,
            cardboard_type_id: $('select[name="filter[cardboard_type]"]').val(),
            cardboard_profile_id: $('select[name="filter[cardboard_profile]"]').val(),
            cardboard_color_id: $('select[name="filter[cardboard_color]"]').val(),
            nomenclature_print: $('select[name="filter[nomenclature_print]"]').val(),
            nomenclature_search: $('input[name="filter[nomenclature_search]"]').val(),
            user_id: $('input[name="user_id"]').val()
        };
    
        $.ajax({
            url: '/order/ajax-filter-nomenclatures',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (result) {
                var tpl = _.template($('#js-modal-nomenclatures').html());
                var selectedIds = [];
                var orderPositions = $(document).find('.order-position-item');
                var positionsContainer = $('.order-positions-popup-container');
    
                if (orderPositions.length > 0) {
                    orderPositions.each(function() {
                        selectedIds.push($(this).data('selected-nomenclature-id'));
                    });
                }
    
                positionsContainer.find('table').removeClass('invis');
                positionsContainer.find('p').addClass('invis');
                positionsContainer.find('p').text('');
    
                if (result.length > 0) {
                    positionsContainer.find('tbody').html(tpl({
                        items: result,
                        selected_ids: selectedIds
                    }));
                } else {
                    positionsContainer.find('table').addClass('invis');
                    if (data.cardboard_type_id == ''
                        && data.cardboard_profile_id == ''
                        && data.cardboard_color_id == ''
                        && data.nomenclature_print == ''
                        && data.nomenclature_search == '')
                    {
                        positionsContainer.find('p').text('Нет позиций');
                    } else {
                        positionsContainer.find('p').text('С учетом фильтрации нужных позиций не найдено');
                    }
                    positionsContainer.find('p').removeClass('invis');
                }
    
                modalClearOrderItem();
            }
        }); 
    }
    
    function modalClearOrderItem() {
        $('input[name="order_item[id]"]').val('');
        $('input[name="order_item[nomenclature]"]').val('');
        $('input[name="order_item[address_number]"]').val(0);
        $('select[name="order_item[cnt_rule]"] option:first').prop('selected', true);
        $('select[name="order_item[cnt_rule]"]').data('selectBox-selectBoxIt').refresh();
        $('input[name="order_item[cnt]"]').val(0);
        $('textarea[name="order_item[comment]"]').val('');
        $('input[name="order_item[address]"]').val('');
        $('select[name="order_item[address]"] option:first').prop('selected', true);
        $('select[name="order_item[address]"]').data('selectBox-selectBoxIt').refresh();
    
        $('.modal-address-input').addClass('invis');
        $('.modal-address-select').removeClass('invis');
        $('a.modal-address-new').html('Добавить адрес').removeClass('active');
        $('.order-positions-panel').removeClass('invis');
    
        $('#modal-position .errors').html('');
    }
    
    function resetDatepickerParams() {
        $('.desired-date .date-result').text('——.——.————');
        $('.desired-date .date-result').addClass('empty');
        $('.desired-date .datepicker').datepicker('option', 'minDate', null);
        $('.desired-date .datepicker').datepicker('setDate', new Date());
        $('.desired-date .date-help').text('');
        $('.desired-date .date-help').addClass('invis');
    }
    
    // Автовыбор: Покупатель, Грузополучатель, Договор (если в списке только 1 параметр)
    $(document).ready(function() {
        if ($('#order-form').attr('action') == '/order/ajax-store') {
            // Выбор покупателя
            if ($('select[name="buyer_ext_id"] option').length == 2) {
                var agreements = [];
                var tpl = _.template($('#js-buyer-agreements').html());
    
                $('select[name="buyer_ext_id"]').val($('select[name="buyer_ext_id"] option:eq(1)').val());
                $('select[name="buyer_ext_id"]').data('selectBox-selectBoxIt').refresh();
    
                agreements = $('select[name="buyer_ext_id"] option:eq(1)').data('agreements');
                $('select[name="agreement_id"]').html(tpl({ items: agreements }));
                $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
    
                // Выбор договора
                if ($('select[name="agreement_id"] option').length == 2) {
                    $('select[name="agreement_id"]').val($('select[name="agreement_id"] option:eq(1)').val());
                    $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
                    $('select[name="agreement_id"]').trigger('change');
                }
            }
    
            // Выбор грузополучателя
            if ($('select[name="consignee_id"] option').length == 2) {
                $('select[name="consignee_id"]').val($('select[name="consignee_id"] option:eq(1)').val());
                $('select[name="consignee_id"]').data('selectBox-selectBoxIt').refresh();
            }
        }
    });
    
    // Подгрузка договоров для выбранного покупателя
    $(document).ready(function() {
        if ($('#order-form').attr('action') != '/order/ajax-store') {
            var agreements = [];
            var select = $('select[name="buyer_ext_id"]');
            var tpl = _.template($('#js-buyer-agreements').html());
    
            if (select.val() != '') {
                agreements = select.find(':selected').data('agreements');
                $('select[name="agreement_id"]').html(tpl({ items: agreements }));
                $('select[name="agreement_id"]').val(select.data('buyer-agreement-id'));
                $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
    
                // Установка даты согласно мин. срока исполнения (из договора или настроек)
                var agreement = $('select[name="agreement_id"]').find(':selected').data('agreement');
                var defaultExecutionTime = Number($('#order-form').data('default-execution-time'));
                var minExecutionTime = Number(agreement.min_execution);
                var minDateHelp = '';
                var setDate = $('input[name="desired_date"]').data('date');
                setDate = new Date(setDate);
            
                if (minExecutionTime > 0) {
                    setDate.setDate(setDate.getDate() + minExecutionTime);
                    minDateHelp = 'Минимальный срок от даты заказа - ' + minExecutionTime + ' ' + plural(minExecutionTime, 'день|дня|дней');
                    $('.desired-date').find('.datepicker').datepicker('option', 'minDate', setDate);
                } else if (defaultExecutionTime > 0) {
                    setDate.setDate(setDate.getDate() + defaultExecutionTime);
                    minDateHelp = 'Минимальный срок от даты заказа - ' + defaultExecutionTime + ' ' + plural(defaultExecutionTime, 'день|дня|дней');
                    $('.desired-date').find('.datepicker').datepicker('option', 'minDate', setDate);
                } else {
                    $('.desired-date .datepicker').datepicker('option', 'minDate', null);
                    $('.desired-date .datepicker').datepicker('setDate', setDate);
                }
            
                if (minDateHelp != '') {
                    $('.desired-date .date-help').text(minDateHelp);
                    $('.desired-date .date-help').removeClass('invis');
                } else {
                    $('.desired-date .date-help').text('');
                    $('.desired-date .date-help').addClass('invis');
                }
            }
        }
    });
    $('select[name="buyer_ext_id"]').on('change', function() {
        var agreements = [];
        var tpl = _.template($('#js-buyer-agreements').html());
    
        if ($(this).val() != '')
            agreements = $(this).find(':selected').data('agreements');
    
        $('select[name="agreement_id"]').html(tpl({ items: agreements }));
        $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
    
        resetDatepickerParams();
    
        // Автовыбор договора (если 1 в списке)
        if ($('select[name="agreement_id"] option').length == 2) {
            $('select[name="agreement_id"]').val($('select[name="agreement_id"] option:eq(1)').val());
            $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
            $('select[name="agreement_id"]').trigger('change');
        }
    });
    
    // Установка даты согласно мин. срока исполнения (из договора или настроек)
    $('select[name="agreement_id"]').on('change', function() {
        resetDatepickerParams();
    
        if ($(this).val() != '') {
            var agreement = $(this).find(':selected').data('agreement');
            var defaultExecutionTime = Number($('#order-form').data('default-execution-time'));
            var minExecutionTime = Number(agreement.min_execution);
            var setDate = new Date();
            var minDateHelp = '';
    
            if (minExecutionTime > 0) {
                setDate.setDate(setDate.getDate() + minExecutionTime);
                minDateHelp = 'Минимальный срок от даты заказа - ' + minExecutionTime + ' ' + plural(minExecutionTime, 'день|дня|дней');
                $('.desired-date .datepicker').datepicker('option', 'minDate', setDate);
                $('.desired-date .date-help').text(minDateHelp);
                $('.desired-date .date-help').removeClass('invis');
            } else if (defaultExecutionTime > 0) {
                setDate.setDate(setDate.getDate() + defaultExecutionTime);
                minDateHelp = 'Минимальный срок от даты заказа - ' + defaultExecutionTime + ' ' + plural(defaultExecutionTime, 'день|дня|дней');
                $('.desired-date .datepicker').datepicker('option', 'minDate', setDate);
                $('.desired-date .date-help').text(minDateHelp);
                $('.desired-date .date-help').removeClass('invis');
            }
        }
    });
    
    // Смена названия даты откгрузк/доставки согласно типу доставки
    $('select[name="delivery_type"]').on('change', function() {
        if ($(this).val() == 'pickup')
            $('.date-container.desired-date').prev('.panel-item-title').text('Дата отгрузки:');
        if ($(this).val() == 'delivery')
            $('.date-container.desired-date').prev('.panel-item-title').text('Дата доставки:');
    });
    
    // Открыть модальное окно позиций
    $('#add-position').on('click', function(e) {
        e.preventDefault();
    
        if ($('select[name="order_item[address]"] option:first').val() != '') {
            $('select[name="order_item[address]"]').prepend('<option value="">Выбрать адрес</option>');
            $('select[name="order_item[address]"]').data('selectBox-selectBoxIt').refresh();
        }
    
        $('#modal-position .modal-header h2').text($('#modal-position .modal-header h2').data('title1'));
        $('#modal-position').modal('show');
        $('.order-positions-panel').find('.filter-by').val('');
    
        modaFilterNomenclatures();
    });
    
    // Закрытие модального окна позиций
    $('#modal-position').on('hidden.bs.modal', function() {
        $('#modal-position .modal-header h2').text('');
        $('.order-positions-popup-container tbody').html('');
        modalClearOrderItem();
    });
    
    // Фильтры позиций в модальном окне
    $('.filter-by-cardboard-type, .filter-by-cardboard-profile, .filter-by-cardboard-color, .filter-by-nomenclature-print').on('change', function() {
        modaFilterNomenclatures();
    });
    $('.filter-by-nomenclature-search').on('keyup', function() {
        modaFilterNomenclatures();
    });
    
    // Номер точки выгрузки
    $(document).on('click', '.count-plus', function(e) {
        e.preventDefault();
        var value = parseInt($(this).parent().find('input').val());
        $(this).parent().find('input').val(value + 1);
    });
    $(document).on('click', '.count-minus', function(e) {
        e.preventDefault();
        $this = $(this);
        var value = parseInt($(this).parent().find('input').val());
        if (value > 0)
            $this.parent().find('input').val(value - 1);    
    });
    
    // Смена поля адреса
    $(document).on('click','.modal-address-new', function(e) {
        e.preventDefault();
        $this = $(this);
        if ($this.hasClass('active')) {
            $this.parent().find('.modal-address-input').addClass('invis');
            $this.parent().find('.modal-address-select').removeClass('invis');
            $this.html('Добавить адрес').removeClass('active');
        } else {
            $this.parent().find('.modal-address-input').removeClass('invis');
            $this.parent().find('.modal-address-select').addClass('invis');
            $this.html('Вернуться к выбору').addClass('active');
        }
    });
    
    // Добавить адрес
    $(document).on('click','#js-add-address', function(e) {
        e.preventDefault();
        var address = $(this).prev().find('input').val();
        address = address.trim();
        if (address != '') {
            if ($('select[name="order_item[address]"] option[value="'+address+'"]').length == 0) {
                $('.modal-address-select select').append('<option value="'+address+'">'+address+'</option>');
            }
            $('select[name="order_item[address]"] option[value="'+address+'"]').prop('selected', true);
            $('select[name="order_item[address]"]').data('selectBox-selectBoxIt').refresh();
            $('.modal-address-input').addClass('invis');
            $('.modal-address-select').removeClass('invis');
            $('.modal-address-new').html('Добавить адрес').removeClass('active');
            $(this).prev().find('input').val('');
        } else {
            $('.modal-address-input').addClass('invis');
            $('.modal-address-select').removeClass('invis');
            $('.modal-address-new').html('Добавить адрес').removeClass('active');
        }
    });
    
    // Выбор товарной номенклатуры
    $(document).on('click','.modal tr', function(e) {
        $('.modal tr').removeClass('selected');
        $(this).addClass('selected');
    
        if ($(document).find('.order-position-item[data-selected-nomenclature-id="'+ $(this).data('nomenclature').id +'"]').length == 0) {
            $('input[name="order_item[nomenclature]"]').val(JSON.stringify($(this).data('nomenclature')));
        } else {
            $('input[name="order_item[nomenclature]"]').val('');
        }
    });
    
    // Валидация и сохранение позиции
    $('#js-save-position').on('click', function() {
        var orderItem = {};
        var orderItemNomenclature = [];
        var errorsBlock = $('#modal-position .errors');
    
        if ($('input[name="order_item[nomenclature]"]').val() != '')
            orderItemNomenclature = JSON.parse($('input[name="order_item[nomenclature]"]').val())
    
        orderItem = {
            id: $('input[name="order_item[id]"]').val(),
            nomenclature_id: orderItemNomenclature['id'],
            address: $('.modal-address-new').hasClass('active') ? $('input[name="order_item[address]"]').val() : $('select[name="order_item[address]"]').val(),
            address_number: $('input[name="order_item[address_number]"]').val(),
            cnt: $('input[name="order_item[cnt]"]').val(),
            cnt_rule: $('select[name="order_item[cnt_rule]"]').val(),
            comment: $('textarea[name="order_item[comment]"]').val(),
            nomenclature: orderItemNomenclature
        };
    
        if (orderItem['id'] == '') {
            var rand = Math.random().toString(36).substr(2, 10);
            orderItem['id'] = 'NEW_' + rand.toUpperCase();
        }
    
        errorsBlock.html('');
    
        var ajaxData = orderItem;
        ajaxData._token = csrf;
    
        $.ajax({
            url: '/order/ajax-validate-order-item',
            type: 'POST',
            data: ajaxData,
            dataType: 'JSON',
            success: function (result) {
                var tpl = _.template($('#js-order-position').html());
                var itemsList = JSON.parse($('input[name="items"]').val());
                var item = {
                    id: orderItem['id'],
                    address: orderItem['address'],
                    address_number: orderItem['address_number'],
                    title: orderItem['nomenclature']['title'],
                    sku: orderItem['nomenclature']['sku'],
                    cnt: orderItem['cnt'],
                    comment: orderItem['comment'],
                    area: orderItem['nomenclature']['area'],
                    nomenclature: orderItem['nomenclature']
                };
    
                if (itemsList.length > 0) {
                    var isModified = false;
    
                    for (var i = 0; i < itemsList.length; i++) {
                        if (itemsList[i].id == orderItem['id']) {
                            itemsList[i] = orderItem;
                            isModified = true;
                            break;
                        }
                    }
            
                    if (!isModified)
                        itemsList.push(orderItem);
            
                    $('input[name="items"]').val(JSON.stringify(itemsList));
                } else {
                    itemsList.push(orderItem);
                    $('input[name="items"]').val(JSON.stringify(itemsList));
                }
    
                // Добавить/обновить позицию
                if ($(document).find('.order-position-item[data-id="'+orderItem['id']+'"]').length)
                    $(document).find('.order-position-item[data-id="'+orderItem['id']+'"]').replaceWith(tpl({ item: item }));
                else
                    $('.order-position-list').append(tpl({ item: item }));
            
                $('#modal-position').modal('hide');
                $('[data-toggle=\'tooltip\']').tooltip({container: 'body',trigger: 'hover'});
            },
            error: function (err) {
                var tpl = _.template($('#js-validation-errors').html());
    
                if (err.status == 422)
                    errorsBlock.html(tpl({ items: err.responseJSON.errors}));
                else
                    errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
            }
        });
    });
    
    // Редактировать позицию
    $(document).on('click', '.edit-position', function(e) {
        e.preventDefault();
        var orderItem = {};
        var itemsList = JSON.parse($('input[name="items"]').val());
        var tpl = _.template($('#js-modal-nomenclatures').html());
    
        for (var i = 0; i <= itemsList.length; i++) {
            if (itemsList[i].id == $(this).data('id')) {
                orderItem = itemsList[i];
                break;
            }
        }
    
        $('.order-positions-popup-container tbody').html(tpl({
            items: [orderItem['nomenclature']],
            selected_ids: []
        }));
        $('.order-positions-popup-container tbody tr:first').addClass('selected');
    
        $('input[name="order_item[id]"]').val(orderItem['id']);
        $('input[name="order_item[nomenclature]"]').val(JSON.stringify(orderItem['nomenclature']));
        $('textarea[name="order_item[comment]"]').val(orderItem['comment']);
    
        if (orderItem['comment'] != '')
            $('textarea[name="order_item[comment]"]').addClass('not-empty');
    
        if ($('select[name="order_item[address]"] option:first').val() == '')
            $('select[name="order_item[address]"] option[value=""]').remove();
    
        if ($('select[name="order_item[address]"] option[value="' + orderItem['address'] + '"]').length == 0) {
            $('input[name="order_item[address]"]').val(orderItem['address']);
            $('input[name="order_item[address]"]').addClass('not-empty');
            $('.modal-address-input').removeClass('invis');
            $('.modal-address-select').addClass('invis');
            $('.modal-address-new').html('Вернуться к выбору').addClass('active');
        } else {
            $('select[name="order_item[address]"]').val(orderItem['address']);
            $('select[name="order_item[address]"]').data('selectBox-selectBoxIt').refresh();
            $('.modal-address-input').addClass('invis');
            $('.modal-address-select').removeClass('invis');
            $('.modal-address-new').html('Добавить адрес').removeClass('active');
        }
    
        $('input[name="order_item[address_number]"]').val(orderItem['address_number']);
        $('input[name="order_item[cnt]"]').val(orderItem['cnt']);
        $('select[name="order_item[cnt_rule]"]').val(orderItem['cnt_rule']);
        $('select[name="order_item[cnt_rule]"]').data('selectBox-selectBoxIt').refresh();
    
        $('.order-positions-panel').addClass('invis');
    
        $('#modal-position .modal-header h2').text($('#modal-position .modal-header h2').data('title2'));
        $('#modal-position').modal('show');
    
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body',trigger: 'hover'});
    });
    
    // Удалить позицию
    $(document).on('click', '.delete-position', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var itemsList = JSON.parse($('input[name="items"]').val());
        var itemsDeletedList = JSON.parse($('input[name="items_deleted"]').val());
    
        for (let i = itemsList.length - 1; i >= 0; i--) {
            if (itemsList[i].id == id) {
                if (String(itemsList[i].id).match(/^NEW_/g) == null) {
                    itemsDeletedList.push(itemsList[i].id);
                }
    
                itemsList.splice(i, 1);
                break;
            }
        }
    
        $(document).find('.order-position-item[data-id="' + id + '"]').remove();
        $('input[name="items"]').val(JSON.stringify(itemsList));
        $('input[name="items_deleted"]').val(JSON.stringify(itemsDeletedList));
    });
    
    // Валидация и отправка заявки
    $('#js-save-order').on('click', function(e){
        e.preventDefault();
        var $self = $(this);
        var form = $('#order-form');
        var formData = new FormData(form[0]);
        var errorsBlock = $('#order-form .errors');
    
        errorsBlock.html('');
    
        if (!$.isEmptyObject(orderFile))
            formData.append('file', orderFile);
    
        $self.addClass('loading');
    
        $.ajax({
            url: '/order/ajax-validate-order',
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            success: function (result) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: formData,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        $self.removeClass('loading');
                        window.location.href = window.location.origin + '/order/list';
                    },
                    error: function (err) {
                        var tpl = _.template($('#js-validation-errors').html());
                        $self.removeClass('loading');
                        errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
                    }
                });
            },
            error: function (err) {
                var tpl = _.template($('#js-validation-errors').html());
    
                $self.removeClass('loading');
                
                if (err.status == 422)
                    errorsBlock.html(tpl({ items: err.responseJSON.errors}));
                else
                    errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
            }
        });
    });
    
    // Поиск позиций
    $('.search-by').on('keyup', function(){
        var sku = $('input[name="search[nomenclature_sku]"]').val();
        var title = $('input[name="search[nomenclature_title]"]').val();
        
        sku = sku.toLowerCase();
        title = title.toLowerCase();
    
        $(document).find('.order-position-item').addClass('invis');
    
        if (sku != '' && title != '') {
            var search = $(document).find('.order-position-item[data-search-nomenclature-sku^="'+sku+'"][data-search-nomenclature-title^="'+title+'"]');
            if (search.length > 0)
                search.removeClass('invis');
        } else if (sku != '' && title == '') {
            var search = $(document).find('.order-position-item[data-search-nomenclature-sku^="'+sku+'"]');
            if (search.length > 0)
                search.removeClass('invis');
        } else if (sku == '' && title != '') {
            var search = $(document).find('.order-position-item[data-search-nomenclature-title^="'+title+'"]');
            if (search.length > 0)
                search.removeClass('invis');
        } else {
            $(document).find('.order-position-item').removeClass('invis');
        }
    });
    
    // Добавление фала
    $('a.add-file-link').click(function(e){
        e.preventDefault();
        $('.add-file input[type=file]').trigger('click');
    });
    $('.add-file input[type=file]').change(function() {
        if ($(this).get(0).files.length == 0)
            return false;
    
        var errorsBlock = $('#order-form .errors');
        var tpl = _.template($('#js-validation-errors').html());
        
        errorsBlock.html('');
        if (($(this).get(0).files[0].size / 1024) > 10000) {
            errorsBlock.html(tpl({ items: {file:['Максимальный объем файла 10Mb.']} }));
            return false;
        }
        if (!fileExt.includes($(this).get(0).files[0].name.split('.').pop())) {
            errorsBlock.html(tpl({ items: {file:['Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar']} }));
            return false;
        }
    
        var filename = $(this).val().replace(/.*\\/, '');
        var name = '';
    
        orderFile = $(this).get(0).files[0];
        name = '<span>'+$(this).get(0).files[0].name+'<span class="remove-file" data-hash=""></span></span>';
    
        if (filename !='') {
            $('.add-file .add-file-name').html(name);
            $('.add-file .add-file-name').addClass('havefile');   
        }  else {
            $('.add-file .add-file-name').text($('.add-file-name').data('text'));
            $('.add-file .add-file-name').removeClass('havefile');
        }
    
        $(this).val('');
    });
    
    // Удаление фалйа
    $(document).on('click', '.remove-file', function(){
        orderFile = {};
    
        if ($(this).data('hash') != '')
            $('<input type="hidden" name="delete_file" value="'+ $(this).data('hash') +'">').insertAfter('input[type=file]');
    
        $('.add-file input[type=file]').val('');
        $('.add-file .add-file-name').text($('.add-file-name').data('text'));
        $('.add-file .add-file-name').removeClass('havefile');
    });
}