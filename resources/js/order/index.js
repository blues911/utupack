// Список заявок
if ($('#tpl-order-index').length == 1) {
    var accountType = $('meta[name="account_type"]').attr("content");

    function filterOrders() {
        var params  = '?';
        params = params + 'date_range=' + $('input[name="filter[date_range]"]').val();
        params = params + '&status=' + $('select[name="filter[status]"]').val();
        params = params + '&by_date=' + $('select[name="filter[by_date]"]').val();
        if (accountType == 'admin' || accountType == 'manager') {
            params = params + '&client=' + $('select[name="filter[client]"]').val();
        }
        
        window.location.href = window.location.origin + window.location.pathname + params;
    }

    // Фильтры
    $('input[name="filter[date_range]"], select[name="filter[status]"], select[name="filter[by_date]"], select[name="filter[client]"]').on('change', function(){
        filterOrders();
    });

    // Всплывающая таблица с позициями
    $('.js-show-positions').on('mouseover', function(e) {
        e.preventDefault();
        $(this).parent().addClass('active');
    });
    $('.js-show-positions').on('mouseleave', function(e) {
        e.preventDefault();
        $('.order-positions-cell').removeClass('active');
    });


    // Выбор клиента в модальном окне
    $('#js-confirm-client').on('click', function() {
        var clientId = $('#modal-clients').find('select[name="client_id"]').val();
        var errorsBlock = $('#modal-clients .errors');

        errorsBlock.html('');

        if (clientId != '') {
            $.ajax({
                url: '/order/ajax-remember-client',
                type: 'POST',
                data: {
                    _token: csrf,
                    client_id: clientId
                },
                dataType: 'JSON',
                success: function (result) {
                    window.location.href = window.location.origin + '/order/create';
                }
            });
        } else {
            var tpl = _.template($('#js-validation-errors').html());
            errorsBlock.html(tpl({ items: {user:['Не выбран клиент.']} }));
        }
    });

    // Открыть заявку
    $(document).on('click', '.order-item', function(e){
        e.preventDefault();
        var $link = $(this).data('link');
        if (e.target !== '.popup-menu-container'){
            window.location.href = $link;
        }
    }).on('click', '.order-item .popup-menu-container, .order-item a, .order-positions-popup', function(e) {
        e.stopPropagation();
    });

    // Копировать заявку
    $(document).on('click', '.js-copy-order', function(e){
        e.preventDefault();
        $(this).next('form').submit();
    });

    // Удалить заявку
    $(document).on('click', '.js-delete-order', function(e){
        e.preventDefault();
        $(this).next('form').submit();
    });

    // Подгрузка заявок
    $('.js-load-more').on('click', function(e){
        e.preventDefault();
        var $self = $(this);
        
        $self.addClass('loading');

        $.ajax({
            url: '/order/ajax-load-more',
            type: 'POST',
            data: {
                _token: csrf,
                offset: $(document).find('.order-item').length,
                date_range: $('input[name="filter[date_range]"]').val(),
                status: $('select[name="filter[status]"]').val(),
                by_date: $('select[name="filter[by_date]"]').val(),
                client: (accountType == 'admin' || accountType == 'manager') ? $('select[name="filter[client]"]').val() : ''
            },
            dataType: 'JSON',
            success: function (result) {
                setTimeout(function() {
                    $('.orders').append(result.html);
                    $self.removeClass('loading');
                    if ($(document).find('.order-item').length == result.orders_total)
                        $('.orders').next().remove();
                }, 1000);
            }
        });
    });
}