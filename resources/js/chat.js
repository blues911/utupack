// Чат
if ($('#tpl-chat').length == 1) {
    var chatFiles = {};

    function clearUnreadMessagesStyles() {
        if ($('.unread-messages').length > 0) {
            $('.message-list .message').removeClass('message-unread');
            $('.unread-messages').parent('.text-center').remove();
        }
    }
    
    function сountMessages() {
        var total = $(document).find('.message-list .message').length;
        $('#chat-messages-total').text(total);
    }
    
    // Добавление файлов в форму чата
    $('.message-file input[type=file]').change(function(){
        if ($(this).get(0).files.length == 0)
            return false;
    
        var errorsBlock = $('.message-list').prev();
        var tpl = _.template($('#js-validation-errors').html());
        
        errorsBlock.html('');
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            if (($(this).get(0).files[i].size / 1024) > 10000) {
                errorsBlock.html(tpl({ items: {file:['Максимальный объем файла 10Mb.']} }));
                return false;
            }
            if (!fileExt.includes($(this).get(0).files[i].name.split('.').pop())) {
                errorsBlock.html(tpl({ items: {file:['Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar']} }));
                return false;
            }
        }
    
        var filename = $(this).val().replace(/.*\\/, '');
        var names = [];
    
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var file = $(this).get(0).files[i];
            if (chatFiles[file.name] == undefined) {
                chatFiles[file.name] = file;
                names.push('<span>'+file.name+'<span class="remove-file" data-name="'+file.name+'"></span></span>');
            }
        }
    
        if (filename !='') {
            $('.message-file-list').append(names);
            $('.message-file-list').addClass('havefile');   
        } else {
            $('.message-file-list').html();
            $('.message-file-list').removeClass('havefile');
        }
    
        $(this).val('');
    });
    
    // Удаление файла
    $(document).on('click', '.remove-file', function(){
        var fileName = $(this).data('name');
        if (chatFiles[fileName] != undefined)
            delete chatFiles[fileName];
        $(this).parent('span').remove();
    });
    
    // Подсветка новых сообщений
    $(document).ready(function() {
        if ($('.message-unread').length > 0) {
            var tpl = _.template($('#js-unread-messages').html());
            $(tpl({})).insertAfter('.message-unread:last');
        }
    });
    
    // Отправка сообщения
    $('#js-send-message').on('click', function(e){
        e.preventDefault();
        var form = $('#chat-form');
        var formData = new FormData(form[0]);
        var errorsBlock = $('.message-list').prev();
    
        errorsBlock.html('');
        clearUnreadMessagesStyles();
    
        if (!$.isEmptyObject(chatFiles)) {
            for (var key in chatFiles) {
                formData.append('files[]', chatFiles[key]);
            }
        }
    
        $.ajax({
            url: '/chat/ajax-store',
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            success: function (result) {
                $('.message-list').prepend(result.html);
                $('#chat-form')[0].reset();
                $('#chat-form').find('.message-file-list').empty().removeClass('havefile');
                $('#chat-form').find('textarea').removeClass('not-empty');
    
                сountMessages();
                chatFiles = {};
            },
            error: function (err) {
                var tpl = _.template($('#js-validation-errors').html());
    
                if (err.status == 422)
                    errorsBlock.html(tpl({ items: err.responseJSON.errors}));
                else
                    errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
            }
        });
    });
    
    // Получить новые сообщения
    var messagesInterval;
    if ($('#chat-messages-total').length == 1 && !messagesInterval) {
        messagesInterval = setInterval(function(){
            $.ajax({
                url: '/chat/ajax-get-unread',
                type: 'POST',
                data: {
                    _token: csrf,
                    object_type: $('#chat-form input[name="object_type"]').val(),
                    object_id: $('#chat-form input[name="object_id"]').val()
                },
                dataType: 'JSON',
                success: function (result) {
    
                    if (result.html != '') {
                        $('.message-list').prepend(result.html);
                    }
    
                    сountMessages();
    
                    // Убрать стили новых сообщений
                    setTimeout(function(){
                        $('.message-list .message').removeClass('message-new');
                    }, 2000);
                }
            });
        }, 4000);
    }
    
    // Убрать стили непрочитанных сообщений
    setTimeout(function(){
        clearUnreadMessagesStyles();
    }, 5000);
}