// Список образцов
if ($('#tpl-sample-index').length == 1) {
    var accountType = $('meta[name="account_type"]').attr("content");

    function filterSamples() {
        var params  = '?';
        params = params + 'date_range=' + $('input[name="filter[date_range]"]').val();
        params = params + '&status=' + $('select[name="filter[status]"]').val();
        if (accountType == 'admin' || accountType == 'manager') {
            params = params + '&client=' + $('select[name="filter[client]"]').val();
        }

        window.location.href = window.location.origin + window.location.pathname + params;
    }

    // Фильтры
    $('input[name="filter[date_range]"], select[name="filter[status]"], select[name="filter[client]"]').on('change', function(){
        filterSamples();
    });

    // Выбор клиента в модальном окне
    $('#js-confirm-client').on('click', function() {
        var clientId = $('#modal-clients').find('select[name="client_id"]').val();
        var errorsBlock = $('#modal-clients .errors');

        errorsBlock.html('');

        if (clientId != '') {
            $.ajax({
                url: '/sample/ajax-remember-client',
                type: 'POST',
                data: {
                    _token: csrf,
                    client_id: clientId
                },
                dataType: 'JSON',
                success: function (result) {
                    window.location.href = window.location.origin + '/sample/create';
                }
            });
        } else {
            var tpl = _.template($('#js-validation-errors').html());
            errorsBlock.html(tpl({ items: {user:['Не выбран клиент.']} }));
        }
    });

    // Открыть претензию
    $(document).on('click', '.sample-item', function(e){
        e.preventDefault();
        var $link = $(this).data('link');
        if (e.target !== '.popup-menu-container'){
            window.location.href = $link;
        }
    }).on('click', '.sample-item .popup-menu-container, .sample-item a, .sample-positions-popup', function(e) {
        e.stopPropagation();
    });

    // Копировать образец
    $(document).on('click', '.js-copy-sample', function(e){
        e.preventDefault();
        $(this).next('form').submit();
    });

    // Удалить образец
    $(document).on('click', '.js-delete-sample', function(e){
        e.preventDefault();
        $(this).next('form').submit();
    });

    // Подгрузка образцов
    $('.js-load-more').on('click', function(e){
        e.preventDefault();
        var $self = $(this);

        $self.addClass('loading');

        $.ajax({
            url: '/sample/ajax-load-more',
            type: 'POST',
            data: {
                _token: csrf,
                offset: $(document).find('.sample-item').length,
                date_range: $('input[name="filter[date_range]"]').val(),
                status: $('select[name="filter[status]"]').val(),
                client: (accountType == 'admin' || accountType == 'manager') ? $('select[name="filter[client]"]').val() : ''
            },
            dataType: 'JSON',
            success: function (result) {
                setTimeout(function() {
                    $('.samples').append(result.html);
                    $self.removeClass('loading');
                    if ($(document).find('.sample-item').length == result.samples_total)
                        $('.samples').next().remove();
                }, 1000);
            }
        });
    });
}