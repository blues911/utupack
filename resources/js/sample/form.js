// Форма образца
if ($('#tpl-sample-form').length == 1) {
    var sampleFile = {};

    // Автовыбор: Покупатель, Грузополучатель, Договор (если в списке только 1 параметр)
    $(document).ready(function() {
        if ($('#sample-form').attr('action') == '/sample/ajax-store') {
            // Выбор покупателя
            if ($('select[name="buyer_ext_id"] option').length == 2) {
                var agreements = [];
                var tpl = _.template($('#js-buyer-agreements').html());

                $('select[name="buyer_ext_id"]').val($('select[name="buyer_ext_id"] option:eq(1)').val());
                $('select[name="buyer_ext_id"]').data('selectBox-selectBoxIt').refresh();

                agreements = $('select[name="buyer_ext_id"] option:eq(1)').data('agreements');
                $('select[name="agreement_id"]').html(tpl({ items: agreements }));
                $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();

                // Выбор договора
                if ($('select[name="agreement_id"] option').length == 2) {
                    $('select[name="agreement_id"]').val($('select[name="agreement_id"] option:eq(1)').val());
                    $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
                }
            }

            // Выбор грузополучателя
            if ($('select[name="consignee_id"] option').length == 2) {
                $('select[name="consignee_id"]').val($('select[name="consignee_id"] option:eq(1)').val());
                $('select[name="consignee_id"]').data('selectBox-selectBoxIt').refresh();
            }
        }
    });

    // Подгрузка договоров для выбранного покупателя
    $(document).ready(function(){
        if ($('#sample-form').attr('action') != '/sample/ajax-store') {
            var select = $('select[name="buyer_ext_id"]');
            var tpl = _.template($('#js-buyer-agreements').html());
            
            if (select.val() != '') {
                agreements = select.find(':selected').data('agreements');
                $('select[name="agreement_id"]').html(tpl({ items: agreements }));
                $('select[name="agreement_id"]').val(select.data('buyer-agreement-id'));
                $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
            }
        }
    });
    $('select[name="buyer_ext_id"]').on('change', function() {
        var agreements = [];
        var tpl = _.template($('#js-buyer-agreements').html());

        if ($(this).val() != '')
            agreements = $(this).find(':selected').data('agreements');

        $('select[name="agreement_id"]').html(tpl({ items: agreements }));
        $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();

        // Автовыбор договора (если 1 в списке)
        if ($('select[name="agreement_id"] option').length == 2) {
            $('select[name="agreement_id"]').val($('select[name="agreement_id"] option:eq(1)').val());
            $('select[name="agreement_id"]').data('selectBox-selectBoxIt').refresh();
            $('select[name="agreement_id"]').trigger('change');
        }
    });

    // Валидация и отправка заявки
    $('#js-save-sample').on('click', function(e){
        e.preventDefault();
        var $self = $(this);
        var form = $('#sample-form');
        var formData = new FormData(form[0]);
        var errorsBlock = $('#sample-form .errors');

        errorsBlock.html('');

        if (!$.isEmptyObject(sampleFile))
            formData.append('file', sampleFile);

        $self.addClass('loading');

        $.ajax({
            url: '/sample/ajax-validate-sample',
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            success: function (result) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: formData,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        $self.removeClass('loading');
                        window.location.href = window.location.origin + '/sample/list/';
                    },
                    error: function (err) {
                        var tpl = _.template($('#js-validation-errors').html());
                        $self.removeClass('loading');
                        errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
                    }
                });
            },
            error: function (err) {
                var tpl = _.template($('#js-validation-errors').html());

                $self.removeClass('loading');
                
                if (err.status == 422)
                    errorsBlock.html(tpl({ items: err.responseJSON.errors}));
                else
                    errorsBlock.html(tpl({ items: {server:['Ошибка сервера.']} }));
            }
        });
    });

    // Добавление фала
    $('a.add-file-link').click(function(e){
        e.preventDefault();
        $('.add-file input[type=file]').trigger('click');
    });
    $('.add-file input[type=file]').change(function() {
        if ($(this).get(0).files.length == 0)
            return false;

        var errorsBlock = $('#sample-form .errors');
        var tpl = _.template($('#js-validation-errors').html());

        errorsBlock.html('');
        if (($(this).get(0).files[0].size / 1024) > 10000) {
            errorsBlock.html(tpl({ items: {file:['Максимальный объем файла 10Mb.']} }));
            return false;
        }
        if (!fileExt.includes($(this).get(0).files[0].name.split('.').pop())) {
            errorsBlock.html(tpl({ items: {file:['Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar']} }));
            return false;
        }

        var filename = $(this).val().replace(/.*\\/, '');
        var name = '';

        sampleFile = $(this).get(0).files[0];
        name = '<span>'+$(this).get(0).files[0].name+'<span class="remove-file" data-hash=""></span></span>';

        if (filename !='') {
            $('.add-file .add-file-name').html(name);
            $('.add-file .add-file-name').addClass('havefile');   
        }  else {
            $('.add-file .add-file-name').text($('.add-file-name').data('text'));
            $('.add-file .add-file-name').removeClass('havefile');
        }

        $(this).val('');
    });

    // Удаление фала
    $(document).on('click', '.remove-file', function(){
        sampleFile = {};

        if ($(this).data('hash') != '')
            $('<input type="hidden" name="delete_file" value="'+ $(this).data('hash') +'">').insertAfter('input[type=file]');

        $('.add-file input[type=file]').val('');
        $('.add-file .add-file-name').text($('.add-file-name').data('text'));
        $('.add-file .add-file-name').removeClass('havefile');
    });
}