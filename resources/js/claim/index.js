// Список претензий
if ($('#tpl-claim-index').length == 1) {
    var accountType = $('meta[name="account_type"]').attr("content");

    function filterSamples() {
        var params  = '?';
        params = params + 'date_range=' + $('input[name="filter[date_range]"]').val();
        params = params + '&status=' + $('select[name="filter[status]"]').val();
        if (accountType == 'admin' || accountType == 'manager') {
            params = params + '&client=' + $('select[name="filter[client]"]').val();
        }
    
        window.location.href = window.location.origin + window.location.pathname + params;
    }
    
    // Фильтры
    $('input[name="filter[date_range]"], select[name="filter[status]"], select[name="filter[client]"]').on('change', function(){
        filterSamples();
    });
    
    // Выбор клиента в модальном окне
    $('#js-confirm-client').on('click', function() {
        var clientId = $('#modal-clients').find('select[name="client_id"]').val();
        var errorsBlock = $('#modal-clients .errors');
    
        errorsBlock.html('');
    
        if (clientId != '') {
            $.ajax({
                url: '/claim/ajax-remember-client',
                type: 'POST',
                data: {
                    _token: csrf,
                    client_id: clientId
                },
                dataType: 'JSON',
                success: function (result) {
                    window.location.href = window.location.origin + '/claim/create';
                }
            });
        } else {
            var tpl = _.template($('#js-validation-errors').html());
            errorsBlock.html(tpl({ items: {user:['Не выбран клиент.']} }));
        }
    });
    
    // Открыть претензию
    $(document).on('click', '.claim-item', function(e){
        e.preventDefault();
        var $link = $(this).data('link');
        if (e.target !== '.popup-menu-container'){
            window.location.href = $link;
        }
    }).on('click', '.claim-item .popup-menu-container, .claim-item a, .claim-positions-popup', function(e) {
        e.stopPropagation();
    });
    
    // Удалить претензию
    $(document).on('click', '.js-delete-claim', function(e){
        e.preventDefault();
        $(this).next('form').submit();
    });
    
    // Подгрузка заявок
    $('.js-load-more').on('click', function(e){
        e.preventDefault();
        var $self = $(this);
    
        $self.addClass('loading');
    
        $.ajax({
            url: '/claim/ajax-load-more',
            type: 'POST',
            data: {
                _token: csrf,
                offset: $(document).find('.claim-item').length,
                date_range: $('input[name="filter[date_range]"]').val(),
                status: $('select[name="filter[status]"]').val(),
                client: (accountType == 'admin' || accountType == 'manager') ? $('select[name="filter[client]"]').val() : ''
            },
            dataType: 'JSON',
            success: function (result) {
                setTimeout(function() {
                    $('.claims').append(result.html);
                    $self.removeClass('loading');
                    if ($(document).find('.claim-item').length == result.claims_total)
                        $('.claims').next().remove();
                }, 1000);
            }
        });
    });
}