// Форма претензии
if ($('#tpl-claim-form').length == 1) {
    var claimFiles = {};

    $(".claim-list select.claimselect").selectBoxIt();
    
    // Добавление позиции
    $('.js-add-claim').on('click', function(e) {
        e.preventDefault();
        var tpl1 = _.template($('#js-claim').html());
        var tpl2 = _.template($('#js-nomenclatures').html());
        var rand = Math.random().toString(36).substr(2, 10);
    
        id = 'NEW_' + rand.toUpperCase();
    
        $('#claim-form .errors').html('');
    
        // Запрет на добавление пустых форм
        // Должно быть выбрано изделие в форме в форме
        if ($('.claim-list .white:first select').val() == '') {
            var tpl3 = _.template($('#js-validation-errors').html());
            $('#js-save-claim').prev('.errors').html(tpl3({ items: {items:['Не выбрано "Изделие" в новой позиции.']} }));
            return false;
        }
    
        $('.claim-list').prepend(tpl1({})).find('.claim-hidden').slideDown(200).removeClass('claim-hidden');
        $('.claim-list select.claimselect').selectBoxIt();
    
        if ($('select[name="order_id"]').val() != '') {
            var items = $('select[name="order_id"]').find(':selected').data('items');
            var selectedIds = [];
    
            $('select.claimselect').each(function() {
                if ($(this).val() != '')
                    selectedIds.push(Number($(this).val()));
            });
    
            $('.claim-list .white:first select.claimselect').html(tpl2({ id: id, items: items, selected_ids: selectedIds }));
            $('.claim-list .white:first select.claimselect').data('selectBox-selectBoxIt').refresh();
            $('.claim-list .white:first').next().find('div.label').addClass('disabled-form');
        }
    });
    
    // Удаление позиции
    $(document).on('click', '.remove-claim', function(e) {
        e.preventDefault();
    
        // Запрет на удаление последней формы
        // if ($('.claim-list .white').length == 1)
        //     return false;
    
        var itemsDeleted = $('input[name="claim_items_deleted"]');
        var id = $(this).parent().data('id');
    
        if (itemsDeleted.length == 1 && String(id).match(/^NEW_/g) == null) {
            var deleted = JSON.parse(itemsDeleted.val());
            deleted.push(id);
            itemsDeleted.val(JSON.stringify(deleted));
        }
    
        $(this).parent().remove();
    });
    
    // Подгрузка товарной номенклатуры в позиции при выборе заявке
    $('select[name="order_id"]').on('change', function() {
        var tpl = _.template($('#js-nomenclatures').html());
        var items = [];
    
        $('#js-save-claim').prev('.errors').html('');
    
        if ($(this).val() != '') {
            items = $(this).find(':selected').data('items');
            $('select.claimselect').each(function(){
                $(this).html(tpl({ items: items, selected_ids: [] }));
                $(this).data('selectBox-selectBoxIt').refresh();
            });
            $('.js-add-claim').removeClass('invis');
            $('.claim-flex-title').css({'margin-top': '-10px'});
            $('.js-add-claim').trigger('click');
            $('#js-save-claim').prev('.errors').html('');
        } else {
            $('select.claimselect').each(function(){
                $(this).html(tpl({ items: items, selected_ids: [] }));
                $(this).data('selectBox-selectBoxIt').refresh();
            });
            $('.js-add-claim').addClass('invis');
            $('.claim-flex-title').css({'margin-top': '0px'});
            $('.claim-list').empty();
        }
    });
    
    // Подгрузка товарной номенклатуры и фиксация активной
    $(document).ready(function() {
        var select = $('select[name="order_id"]');
        var tpl = _.template($('#js-nomenclatures').html());
        
        if (select.val() != '') {
            var items = select.find(':selected').data('items');
            $('select.claimselect').each(function() {
                $(this).html(tpl({ items: items, selected_ids: [] }));
                $(this).val($(this).data('nomenclature-id'));
                $(this).data('selectBox-selectBoxIt').refresh();
            });
            $('.js-add-claim').removeClass('invis');
            $('.claim-flex-title').css({'margin-top': '-10px'});
        } else {
            $('.claim-flex-title').css({'margin-top': '0px'});
        }
    });
    
    // Сохранить претензию
    $('#js-save-claim').on('click', function(e){
        e.preventDefault();
        var $self = $(this);
        var formData = new FormData($('#claim-form')[0]);
        formData.append('_token', csrf);
        $('#claim-form .errors').html('');
    
        if (!$.isEmptyObject(claimFiles)) {
            for (var key in claimFiles) {
                if (!$.isEmptyObject(claimFiles[key])) {
                    for (var k in claimFiles[key]) {
                        formData.append('claim_items['+key+'][files][]', claimFiles[key][k]);
                    }
                }
            }
        }
    
        $self.addClass('loading');
    
        $.ajax({
            url: '/claim/ajax-validate-claim',
            type: 'POST',
            data: formData,
            dataType: 'JSON',
            processData: false,
            contentType: false,
            success: function (result) {
                $.ajax({
                    url: $('#claim-form').attr('action'),
                    type: 'POST',
                    data: formData,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        $self.removeClass('loading');
                        window.location.href = window.location.origin + '/claim/list/';
                    },
                    error: function (err) {
                        var tpl = _.template($('#js-validation-errors').html());
                        $self.removeClass('loading');
                        $('#js-save-claim').prev('.errors').html(tpl({ items: {server:['Ошибка сервера.']} }));
                    }
                });
            },
            error: function (err) {
                var tpl = _.template($('#js-validation-errors').html());
                var errors = err.responseJSON.errors;
    
                $self.removeClass('loading');
    
                if (err.status == 422)
                {
                    if (errors.claim !== undefined) {
                        $('#js-save-claim').prev('.errors').html(tpl({ items: errors.claim }));
                    }
                    
                    if (errors.claim_items !== undefined) {
                        _.forEach(errors.claim_items, function(items, key) {
                            $(document).find('.claim-list .white[data-id="' + key + '"] .errors').html(tpl({ items: items }));
                        });
                    }
                }
                else
                {
                    $('#js-save-claim').prev('.errors').html(tpl({ items: {server:['Ошибка сервера.']} }));
                }
    
            }
        });
    });
    
    // Добавить файлы
    $(document).on('click', '.add-file-link', function(){
        $(this).prev('.file-type-3 input[type=file]').trigger('click');
    });
    $(document).on('change', '.file-type-3 input[type=file]', function() {
        if ($(this).get(0).files.length == 0)
            return false;
    
        var errorsBlock = $(this).parents('.white').find('.errors');
        var tpl = _.template($('#js-validation-errors').html());
    
        errorsBlock.html('');
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            if (($(this).get(0).files[i].size / 1024) > 10000) {
                errorsBlock.html(tpl({ items: {file:['Максимальный объем файла 10Mb.']} }));
                return false;
            }
            if (!fileExt.includes($(this).get(0).files[i].name.split('.').pop())) {
                errorsBlock.html(tpl({ items: {file:['Формат файла должен быть: jpg, jpeg, png, tiff, bmp, doc, docx, xls, xlsx, pdf, zip, rar']} }));
                return false;
            }
        }
    
        var filename = $(this).val().replace(/.*\\/, '');
        var formId = $(this).parents('.white').data('id');
        var names = [];
        $this = $(this);
    
        if (claimFiles[formId] == undefined)
            claimFiles[formId] = {};
    
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var file = $(this).get(0).files[i];
            if (claimFiles[formId][file.name] == undefined) {
                claimFiles[formId][file.name] = file;
                names.push('<span>'+file.name+'<span class="remove-file" data-name="'+file.name+'" data-hash="" data-form-id="'+formId+'"></span></span>');
            }
        }
    
        if (filename != '') {
            $this.parents('.file-type-3').find('.file-list-3').append(names);
            $this.parents('.file-type-3').find('.file-list-3').addClass('havefile');   
        } else {
            $this.parents('.file-type-3').find('.file-list-3').html();
            $this.parents('.file-type-3').find('.file-list-3').removeClass('havefile');
        }
    
        $this.val('');
    });
    
    // Удаление файла
    $(document).on('click', '.remove-file', function(){
        var formId = $(this).data('form-id');
        if ($(this).data('name') != '' && $(this).data('hash') == '') {
            // Новый
            var fileName = $(this).data('name');
            if (claimFiles[formId][fileName] != undefined)
                delete claimFiles[formId][fileName];
            $(this).parent('span').remove();
        } else if ($(this).data('name') == '' && $(this).data('hash') != '') {
            // Старый
            var fileHash = $(this).data('hash');
            var deleteFiles = JSON.parse($('input[name="claim_items['+formId+'][delete_files]"]').val());
            deleteFiles.push(fileHash);
            $('input[name="claim_items['+formId+'][delete_files]"]').val(JSON.stringify(deleteFiles));
            $(this).parent('span').remove();
        }
    });
}