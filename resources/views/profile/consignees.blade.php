@extends('layout.user')

@section('title')

    Грузополучатели

@endsection

@section('content')

<div class="container" id="tpl-profile-consignees">

    @include('profile.inc._nav', ['nav' => $nav])

    <div class="consignees-top">
        <div class="consignees-text">Ниже вы можете ознакомится с доступным списком грузополучателей</div>
        <a href="#" id="add-consignee" class="button" data-user-id="{{ auth()->user()->id }}">Добавить</a>
    </div>

    <div class="consignees">

        @if (!empty($consignees))
            @foreach ($consignees as $consignee)
                <div data-id="{{ $consignee['id'] }}">
                    <div class="consignee-item">
                        <div class="consignee-item_name">
                            <ttag title="Код в 1С: {{ $consignee['ext_id'] }}">{{ $consignee['title'] }}</ttag>
                        </div>
                        <div class="popup-menu-container">
                            <div class="icon-more js-show-popup-menu"></div>
                            <div class="popup-menu">
                                <ul>
                                    <li>
                                        <a href="#" class="edit-consignee" data-item="{{ $consignee->toJson() }}">Изменить</a>
                                    </li>
                                    <li>
                                        <a href="#" class="remove-consignee" data-item="{{ $consignee->toJson() }}">Удалить</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

    </div>

    <div class="modal modal-v-center fade modal-cons" id="modal-consignee">
        <div class="modal-dialog modal-dialog-mini">
            <div class="modal-content modal-content-mini">
            </div>
        </div>
    </div>

    <!-- js templates-->
    @include('profile.inc._js_consignee')

</div>

@endsection