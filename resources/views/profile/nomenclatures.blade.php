@extends('layout.user')

@section('title')

    Товарная номенклатура

@endsection

@section('content')

<div class="container" id="tpl-profile-nomenclatures">

    @include('profile.inc._nav', ['nav' => $nav])
    
    @if (!empty($nomenclatures))

        <div class="nom js-ajax-list">
            @foreach ($nomenclatures as $nomenclature)
                <div class="nom-item {{ ($nomenclature->status == 0) ? 'disabled' : ''}}">
                    <div>
                        <ttag title="Код в 1С: {{ $nomenclature->ext_id }}">{{ $nomenclature->title }}</ttag>
                        <span>
                            @if($nomenclature->status == 0)
                                <i class="archive-lable">архивная</i>
                            @endif
                        </span>
                    </div>
                    <div>
                        {{ ($nomenclature['product_code']) ? $nomenclature['product_code'] : '—' }}
                        <span>Код изделия</span>
                    </div>
                    <div>
                        @php
                            $cardboard = '';
                            if ($nomenclature->cardboardType) $cardboard .= $nomenclature->cardboardType->title .', ';
                            if ($nomenclature->cardboardColor) $cardboard .= $nomenclature->cardboardColor->title .', ';
                            if ($nomenclature->cardboardProfile) $cardboard .= $nomenclature->cardboardProfile->title;
                            $cardboard = rtrim(trim($cardboard), ',');
                        @endphp
 
                        {{ $cardboard }}
                        <span>Картон</span>
                    </div>
                    <div>
                        {{ ($nomenclature['print'] == 1) ? 'Да' : 'Нет' }}
                        <span>Печать</span>
                    </div>
                    <div>
                        {{ snumb_format($nomenclature['area']) }}
                        <span>Площадь, м²</span>
                    </div>
                </div>
            @endforeach
        </div>

        {{-- <div class="text-center">
            <div class="button button-more js-load-more">Показать ещё</div>		
        </div> --}}

    @endif
      
</div>

@endsection