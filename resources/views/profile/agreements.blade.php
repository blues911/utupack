@extends('layout.user')

@section('title')

    Юридические лица и договора 

@endsection

@section('content')

<div class="container">

    @include('profile.inc._nav', ['nav' => $nav])

    <div class="contracts" id="tpl-profile-agreements">

        @if (!empty($agreements))

            @foreach ($agreements as $agreement)

                <div class="contract-item {{ ($agreement['status'] == 0) ? 'disabled' : ''}}">
                    <span class="contact-item_main">
                        <ttag title="Код в 1С: {{ $agreement['ext_id'] }}">Договор № {{ $agreement['number'] }} от {{ to_agreement_date($agreement['date']) }}</ttag>
                        <span>
                            {{ $agreement['buyer_title'] }}
                            @if($agreement['status'] == 0)
                                <i class="archive-lable">архивный</i>
                            @endif
                        </span>
                    </span>
                    <span class="contract-item_addon">
                        @if ($agreement['info'])
                            {{ $agreement['info'] }}<br>
                        @endif
                        @if ($agreement['min_execution'] > 0)
                            Срок исполнения - от {{ $agreement['min_execution'] }} {{ plural($agreement['min_execution'], 'дня', 'дней', 'дней') }}
                        @endif
                    </span>
                </div>

            @endforeach

        @endif

    </div>

</div>

@endsection