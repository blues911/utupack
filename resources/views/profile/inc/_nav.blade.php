<ul class="tabs">
		<li class="{{ ($nav == 'info') ? 'active' : '' }}">
				<a href="{{ url('profile/info') }}">Профиль</a>
		</li>
		<li class="{{ ($nav == 'agreements') ? 'active' : '' }}">
				<a href="{{ url('profile/agreements') }}">Юридические лица и договора</a>
		</li>
		<li class="{{ ($nav == 'consignees') ? 'active' : '' }}">
				<a href="{{ url('profile/consignees') }}">Грузополучатели</a>
		</li>
		<li class="{{ ($nav == 'nomenclatures') ? 'active' : '' }}">
				<a href="{{ url('profile/nomenclatures') }}">Товарная номенклатура</a>
		</li>
</ul>