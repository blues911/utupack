<script id="js-modal-consignee-content" type="text/template">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"></button>
        <h2><%= title %></h2>
    </div>
    <div class="modal-body">
        <input type="hidden" name="id" value="<%= input.id %>">
        <input type="hidden" name="user_id" value="<%= input.user_id %>">
        <div class="cons-input">
            <div class="input-container">
                <input type="text" class="form-control <%= input.id ? 'not-empty' : '' %>" name="title" value="<%= input.title %>">
                <span class="placeholder">Наименование юридического лица</span>
            </div>
        </div>
        <div class="cons-input">
            <div class="input-container">
                <input type="text" class="form-control <%= input.id ? 'not-empty' : '' %>" name="inn" value="<%= input.inn %>">
                <span class="placeholder">ИНН</span>
            </div>
        </div>
        <div class="cons-input">
            <div class="input-container">
                <input type="text" class="form-control <%= input.id ? 'not-empty' : '' %>" name="ogrn" value="<%= input.ogrn %>">
                <span class="placeholder">ОГРН/ОГРНИП</span>
            </div>
        </div>
        <div class="errors"></div>
        <div class="text-center">
            <a href="#" id="save-consignee" class="button">Сохранить</a>
        </div>
    </div>
</script>

<script id="js-validation-errors" type="text/template">
    <div class="alert alert-danger">
        <ul>
            <% _.each(items, function (error) { %>
                <li><%= error[0] %></li>
            <% }); %>
        </ul>
    </div>
</script>

<script id="js-consignee-item" type="text/template">
    <div data-id="<%= item.id %>">
        <div class="consignee-item">
            <div class="consignee-item_name">
                <%= item.title %>
            </div>
            <div class="popup-menu-container">
                <div class="icon-more js-show-popup-menu"></div>
                <div class="popup-menu">
                    <ul>
                        <li>
                            <a href="#" class="edit-consignee" data-item='<%= JSON.stringify(item) %>'>Изменить</a>
                        </li>
                        <li>
                            <a href="#" class="remove-consignee" data-item='<%= JSON.stringify(item) %>'>Удалить</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</script>