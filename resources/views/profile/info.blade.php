@extends('layout.user')

@section('title')

    Профиль

@endsection

@section('content')

<div class="container" id="tpl-profile-info">

  @include('profile.inc._nav', ['nav' => $nav])

  <div class="profile-info">
      <div>
          <ttag title="Код в 1С: {{ $info->ext_id }}">{{ ($info->name) ? $info->name : '—' }}</ttag>
          <span>Наименование организации</span>
      </div>
      <div>
          {{ ($info->email) ? $info->email : '—' }}
          <span>E-mail</span>
      </div>
      <div>
          {{ ($info->phone) ? $info->phone : '—' }}
          <span>Телефон</span>
      </div>
      <div>
          {{ ($info->login) ? $info->login : '—' }}
          <span>Логин</span>
      </div>
  </div>

</div>

@endsection