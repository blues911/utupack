<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Новая претензия от {{ $claim->order->client->name }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:regular,medium,semi-bold" rel="stylesheet" type="text/css">
</head>
<body style="font-family: Montserrat, Arial, sans-serif; font-size: 14px; font-weight: 400;">

    <img src="{{ asset('img/logo.png') }}" alt="logo">

    <h3 style="color: #212842;">Новая претензия от клиента {{ $claim->order->client->name }}</h3>

    <div style="background: #f2f2f2; padding: 15px 15px 1px 15px;">

        <a href="{{ url('claim/edit/'. $claim->id) }}" style="display: inline-block; padding-bottom: 15px; font-size: 18px; font-weight: bold; color: #0083fb;">Претензия № {{ fix_product_code($claim->product_code) }}</a>

        @foreach ($claim->items as $item)
            <div style="display: block; background: #fff; padding: 15px; margin-bottom: 15px;">
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="display: inline-block; color: #9197ae; width: 30%; font-weight: 500;">Изделие:</div>
                    <div style="display: inline-block; color: #212842; width: 70%; font-weight: 600;">{{ $item->nomenclature->title }}</div>
                </div>
                <div style="display: flex; margin-bottom: 15px; line-height: 1.2">
                    <div style="display: inline-block; color: #9197ae; width: 30%; font-weight: 500;">Претензия:<br/><span style="font-size: 12px;">или проблема с изделием</span></div>
                    <div style="display: inline-block; color: #212842; width: 70%; font-weight: 600;">{{ $item->claim }}</div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="display: inline-block; color: #9197ae; width: 30%; font-weight: 500;">Кол-во проблемных позиций:</div>
                    <div style="display: inline-block; color: #212842; width: 70%; font-weight: 600;">{{ $item->cnt }} шт.</div>
                </div>
            </div>
        @endforeach

    </div>

</body>
</html>