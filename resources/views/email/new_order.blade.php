<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Новая заявка от клиента {{ $order->client->name }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:regular,medium,semi-bold" rel="stylesheet" type="text/css">
</head>
<body style="font-family: Montserrat, Arial, sans-serif; font-size: 14px; font-weight: 400;">

    <img src="{{ asset('img/logo.png') }}" alt="logo">

    <h3 style="color: #212842;">Новая заявка от клиента {{ $order->client->name }}</h3>

    <div style="background: #f2f2f2; padding: 15px 15px 1px 15px;">

        <a href="{{ url('order/edit/'. $order->id) }}" style="display: inline-block; padding-bottom: 15px; font-size: 18px; font-weight: 600; color: #0083fb;">Заявка № {{ fix_product_code($order->product_code) }}</a>

        <table style="background: #fff; padding: 15px; margin-bottom: 15px;">
            <thead style="color: #9197ae; text-align: left;">
                <tr>
                    <th style="width: 10%; padding: 12px 20px 12px 0; font-weight: 500;">Код товара</th>
                    <th style="width: 20%; padding: 12px 20px 12px 0; font-weight: 500;">Наименование</th>
                    <th style="width: 10%; padding: 12px 20px 12px 0; font-weight: 500;">Итоговая площадь,&nbsp;м²</th>
                    <th style="width: 15%; padding: 12px 20px 12px 0; font-weight: 500;">Кол-во</th>
                </tr>
            </thead>
            <tbody style="color: #212842; text-align: left;">
                @foreach ($order->items as $item)
                    <tr>
                        <td style="border-top: 2px solid #ebedf3; padding: 12px 20px 12px 0; font-weight: 600;">{{ ($item->nomenclature->sku) ? $item->nomenclature->sku : '—' }}</td>
                        <td style="border-top: 2px solid #ebedf3; padding: 12px 20px 12px 0; font-weight: 600;">{{ $item->nomenclature->title }}</td>
                        @php
                            $area = ($item->cnt > 1) ? $item->cnt * $item->nomenclature->area : $item->nomenclature->area;
                        @endphp
                        <td style="border-top: 2px solid #ebedf3; padding: 12px 20px 12px 0; font-weight: 600;">{{ $area }}</td>
                        <td style="border-top: 2px solid #ebedf3; padding: 12px 20px 12px 0; font-weight: 600;">{{ snumb_format($item->cnt) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

</body>
</html>