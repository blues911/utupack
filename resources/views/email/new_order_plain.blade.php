Новая заявка от клиента {{ $order->client->name }}

Заявка № {{ fix_product_code($order->product_code) }}
@foreach ($order->items as $item)

Код товара: {{ ($item->nomenclature->sku) ? $item->nomenclature->sku : '—' }}
Наименование: {{ $item->nomenclature->title }}</td>
@php
$area = ($item->cnt > 1) ? $item->cnt * $item->nomenclature->area : $item->nomenclature->area;
@endphp
Итоговая площадь, м²: {{ $area }}
Кол-во: {{ snumb_format($item->cnt) }}
@endforeach