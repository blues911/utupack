Новая претензия от клиента {{ $claim->order->client->name }}

Претензия № {{ fix_product_code($claim->product_code) }}
@foreach ($claim->items as $item)

Изделие: {{ $item->nomenclature->title }}
Претензия: {{ $item->claim }}
Кол-во проблемных позиций: {{ $item->cnt }} шт.
@endforeach