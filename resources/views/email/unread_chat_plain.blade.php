@if (isset($items['orders_chat']) && !empty($items['orders_chat']))
Новые сообщения по заявкам

@foreach($items['orders_chat'] as $item)
Заявка № {{ fix_product_code($item['order_number']) }}
@foreach($item['chat'] as $chat)

{{ $chat['message'] }}
{{ $chat['author'] }}, {{ to_chat_date($chat['date']) }}
@endforeach
@endforeach

@endif
@if (isset($items['samples_chat']) && !empty($items['samples_chat']))
Новые сообщения по образцам

@foreach($items['samples_chat'] as $item)
Образец № {{ fix_product_code($item['sample_number']) }}
@foreach($item['chat'] as $chat)

{{ $chat['message'] }}
{{ $chat['author'] }}, {{ to_chat_date($chat['date']) }}
@endforeach
@endforeach

@endif
@if (isset($items['claims_chat']) && !empty($items['claims_chat']))
Новые сообщения по претензиям

@foreach($items['claims_chat'] as $item)
Претензия № {{ fix_product_code($item['claim_number']) }}
@foreach($item['chat'] as $chat)

{{ $chat['message'] }}
{{ $chat['author'] }}, {{ to_chat_date($chat['date']) }}
@endforeach
@endforeach
@endif