<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Новый образец от клиента {{ $sample->client->name }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:regular,medium,semi-bold" rel="stylesheet" type="text/css">
</head>
<body style="font-family: Montserrat, Arial, sans-serif; font-size: 14px; font-weight: 400;">

    <img src="{{ asset('img/logo.png') }}" alt="logo">

    <h3 style="color: #212842;">Новый образец от клиента {{ $sample->client->name }}</h3>

    <div style="background: #f2f2f2; padding: 15px 15px 1px 15px;">

        <a href="{{ url('sample/edit/'. $sample->id) }}" style="display: inline-block; padding-bottom: 15px; font-size: 18px; font-weight: bold; color: #0083fb;">Образец № {{ fix_product_code($sample->product_code) }}</a>

        <div style="display: block; background: #fff; padding: 15px; margin-bottom: 15px;">
            <div style="display: flex; margin-bottom: 15px;">
                <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Вид изделия:</div>
                <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sample->nomenclatureType->title }}</div>
            </div>
            @php
                $sizes = json_decode($sample->sizes, true);
            @endphp
            <div style="display: flex; margin-bottom: 15px;">
                <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Длинна:</div>
                <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sizes['length'] }}</div>
            </div>
            <div style="display: flex; margin-bottom: 15px;">
                <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Ширина:</div>
                <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sizes['width'] }}</div>
            </div>
            @if (isset($sizes['height']))
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Высота:</div>
                    <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sizes['height'] }}</div>
                </div>
            @endif
            <div style="display: flex; margin-bottom: 15px;">
                <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Марка картона:</div>
                <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sample->cardboardType->title }}</div>
            </div>
            <div style="display: flex; margin-bottom: 15px;">
                <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Профиль:</div>
                <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sample->cardboardProfile->title }}</div>
            </div>
            <div style="display: flex; margin-bottom: 15px;">
                <div style="display: inline-block; color: #9197ae; width: 20%; font-weight: 500;">Цвет:</div>
                <div style="display: inline-block; color: #212842; width: 80%; font-weight: 600;">{{ $sample->cardboardColor->title }}</div>
            </div>
        </div>

    </div>

</body>
</html>