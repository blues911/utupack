<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Новые сообщения</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:regular,medium,semi-bold" rel="stylesheet" type="text/css">
</head>
<body style="font-family: Montserrat, Arial, sans-serif; font-size: 14px; font-weight: 400;">

    <img src="{{ asset('img/logo.png') }}" alt="logo">

    @if (isset($items['orders_chat']) && !empty($items['orders_chat']))
        <h3 style="color: #212842;">Новые сообщения по заявкам</h3>
        @foreach($items['orders_chat'] as $item)
            <div style="background: #f2f2f2; padding: 15px 15px 1px 15px;">
                <a href="{{ url('order/edit/'. $item['order_id']) }}" style="display: inline-block; padding-bottom: 15px; font-size: 18px; font-weight: 600; color: #0083fb;">Заявка № {{ fix_product_code($item['order_number']) }}</a>
                @foreach($item['chat'] as $chat)
                    <div style="background: #fff; padding: 15px; margin-bottom: 15px; font-weight: 500;">
                        <p style="margin: 0; padding: 0; color: #212842;">{!! nl2br($chat['message']) !!}</p>
                        <span style="display: block; margin-top: 10px; text-align: right; font-size: 12px;">
                            <a href="javascript:void(0);" style="text-decoration: none; color: #0083fb;">{{ $chat['author'] }}</a><span style="color: #9197ae;">, {{ to_chat_date($chat['date']) }}</span>
                        </span>
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    @if (isset($items['samples_chat']) && !empty($items['samples_chat']))
        <h3 style="color: #212842;">Новые сообщения по образцам</h3>
        @foreach($items['samples_chat'] as $item)
            <div style="background: #f2f2f2; padding: 15px 15px 1px 15px;">
                <a href="{{ url('sample/edit/'. $item['sample_id']) }}" style="display: inline-block; padding-bottom: 15px; font-size: 18px; font-weight: 600; color: #0083fb;">Образец № {{ fix_product_code($item['sample_number']) }}</a>
                @foreach($item['chat'] as $chat)
                    <div style="background: #fff; padding: 15px; margin-bottom: 15px; font-weight: 500;">
                        <p style="margin: 0; padding: 0; color: #212842;">{!! nl2br($chat['message']) !!}</p>
                        <span style="display: block; margin-top: 10px; text-align: right; font-size: 12px;">
                            <a href="javascript:void(0);" style="text-decoration: none; color: #0083fb;">{{ $chat['author'] }}</a><span style="color: #9197ae;">, {{ to_chat_date($chat['date']) }}</span>
                        </span>
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

    @if (isset($items['claims_chat']) && !empty($items['claims_chat']))
        <h3 style="color: #212842;">Новые сообщения по претензиям</h3>
        @foreach($items['claims_chat'] as $item)
            <div style="background: #f2f2f2; padding: 15px 15px 1px 15px;">
                <a href="{{ url('claim/edit/'. $item['claim_id']) }}" style="display: inline-block; padding-bottom: 15px; font-size: 18px; font-weight: 600; color: #0083fb;">Претензия № {{ fix_product_code($item['claim_number']) }}</a>
                @foreach($item['chat'] as $chat)
                    <div style="background: #fff; padding: 15px; margin-bottom: 15px; font-weight: 500;">
                        <p style="margin: 0; padding: 0; color: #212842;">{!! nl2br($chat['message']) !!}</p>
                        <span style="display: block; margin-top: 10px; text-align: right; font-size: 12px;">
                            <a href="javascript:void(0);" style="text-decoration: none; color: #0083fb;">{{ $chat['author'] }}</a><span style="color: #9197ae;">, {{ to_chat_date($chat['date']) }}</span>
                        </span>
                    </div>
                @endforeach
            </div>
        @endforeach
    @endif

</body>
</html>