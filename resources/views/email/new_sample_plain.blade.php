Новый образец от клиента {{ $sample->client->name }}

Образец № {{ fix_product_code($sample->product_code) }}

Вид изделия: {{ $sample->nomenclatureType->title }}
@php
$sizes = json_decode($sample->sizes, true);
@endphp
Длинна: {{ $sizes['length'] }}
Ширина: {{ $sizes['width'] }}
@if (isset($sizes['height']))
Высота: {{ $sizes['height'] }}
@endif
Марка картона: {{ $sample->cardboardType->title }}
Профиль: {{ $sample->cardboardProfile->title }}
Цвет: {{ $sample->cardboardColor->title }}