@extends('layout.user')

@section('title')

    @php
        $claimNumber = '';

        if ($claim)
        {
            $claimNumber = ($claim->product_code != null) ? '№ ' . fix_product_code($claim->product_code) : '№ ...';
        }

        $claimDate = ($claim) ? to_calendar_date($claim->date) : date('d.m.Y');
    @endphp

    Претензия {{ $claimNumber }} от {{ $claimDate }}

@endsection

@section('content')

<div class="container" id="tpl-claim-form">

    <h1>
        Претензия {{ $claimNumber }}
        <span class="date">от {{ $claimDate }}</span>
        @if ($claim)
            <span class="claim-title-status claim-title-status-{{ $claim->status }}">{{ claim_status_name($claim->status) }}</span>
        @endif
    </h1>

    @if (user_account_type(['manager', 'admin']))
        <div class="content-title-addon">
            Клиент: <a href="{{ url('claim/list?date_range=&status=&client='.$params['client']->id) }}" class="client-filter-link u u-dotted">{{ $params['client']->name }}</a>
        </div>
    @endif

    <form id="claim-form" action="{{ ($claim) ? url('claim/ajax-update/' . $claim->id) : url('claim/ajax-store') }}" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}

        @if ($claim)
            <input type="hidden" name="id" value="{{ $claim->id }}">
        @endif

        @if ($claim)
            <input type="hidden" name="claim_items_deleted" value="[]">
        @endif

        <div class="claim-body">
            <div class="row">

                <div class="col-4">
                    <div class="white white-p-mini">
                        @if (user_account_type(['manager', 'admin']))
                            <div class="label">
                                <div class="label-title">Статус</div>
                                <select name="status" class="styled">
                                    @foreach ($params['status_types'] as $key => $name)
                                        <option value="{{ $key }}" {{ ($claim && $claim->status == $key) ? 'selected disabled' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="label">
                            <div class="label-title">Связанная заявка на производство</div>
                            <select name="order_id" class="styled">
                                <option value="">Выбрать заявку</option>
                                @foreach ($params['orders'] as $order)
                                    <option value="{{ $order->id }}" data-items="{{ $order->items->toJson() }}" {{ ($claim && $claim->order_id == $order->id) ? 'selected' : '' }}>Заявка № {{ fix_product_code($order->product_code) }} от {{ to_calendar_date($order->date) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="label">
                            <div class="label-title">Претензия</div>
                            <textarea name="common_claim" class="form-control">{{ ($claim && $claim->common_claim != '') ? $claim->common_claim  : '' }}</textarea>
                        </div>
                        @if (user_account_type(['manager', 'admin']))
                            <div class="label">
                                <div class="label-title">Решение</div>
                                <textarea name="common_decision" class="form-control">{{ ($claim && $claim->common_decision != '') ? $claim->common_decision  : '' }}</textarea>
                            </div>
                        @endif
                    </div>
                    <div class="errors"></div>
                    <a href="#" id="js-save-claim" class="button btn-block button-lg btn-submit">Сохранить</a>
                </div>

                <div class="col-8">
                    <div class="claim-flex-title">
                        <h2>Позиции из заявки</h2>
                        <a href="#" class="button js-add-claim invis">Добавить</a> 
                    </div>
                    <div class="claim-list">

                        @if ($claim && !empty($claim->items))

                            @foreach ($claim->items as $item)
                                <div class="white" data-id="{{ $item->id }}">
                                    <div class="remove-claim"></div>
                                    <div class="label disabled-form">
                                        <div class="label-title">Изделие</div>
                                        <select name="claim_items[{{ $item->id }}][nomenclature_id]" data-nomenclature-id="{{ $item->nomenclature_id }}" class="claimselect">
                                            <option value="">Выбрать изделие</option>
                                        </select>
                                    </div>
                                    <div class="label">
                                        <div class="label-title">Претензия или проблема с изделием</div>
                                        <textarea class="form-control" name="claim_items[{{ $item->id }}][claim]" placeholder="Опишите проблему">{{ $item->claim }}</textarea>
                                    </div>
                                    @if (user_account_type(['manager', 'admin']))
                                        <div class="label">
                                            <div class="label-title">Решение по претензии</div>
                                            <textarea class="form-control" name="claim_items[{{ $item->id }}][decision]" placeholder="Напишите решение">{{ $item->decision }}</textarea>
                                        </div>
                                    @endif
                                    <div class="claim-bottom">
                                        <label class="input-container">
                                            <input type="number" class="form-control not-empty" name="claim_items[{{ $item->id }}][cnt]" value="{{ $item->cnt }}">
                                            <span class="placeholder">Кол-во проблемных позиций, шт</span>
                                        </label>
                                        <div class="claim-files file-type-3">
                                            @if (isset($item->files) && !empty($item->files))
                                                <div class="claim-files-list file-list-3 havefile">
                                                    @foreach($item->files as $itemFile)
                                                        <span>
                                                            <a href="{{ url('shared/read-file/' . $itemFile->hash) }}" target="_blank">{{ $itemFile->name }}</a>
                                                            <span class="remove-file" data-name="" data-hash="{{ $itemFile->hash }}" data-form-id="{{ $item->id }}"></span>
                                                        </span>
                                                    @endforeach
                                                </div>
                                            @else
                                                <div class="claim-files-list file-list-3"></div>
                                            @endif
                                            <input type="hidden" name="claim_items[{{ $item->id }}][delete_files]" value="[]">
                                            <input type="file" name="claim_items[{{ $item->id }}][files][]" multiple accept=".jpg, .jpeg, .png, .tiff, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar">
                                            <span class="add-file-link u u-dotted">Загрузить файлы</span>
                                        </div>
                                    </div>
                                    <div class="errors"></div>
                                </div>
                            @endforeach

                        @endif
                        
                    </div>
                </div>

            </div>
        </div>
    </form>

    {{-- Чат --}}

    @if ($claim && $claim->ext_id != null)
        
        @include('_chat', [
            'h2_title' => 'Сообщения по претензии',
            'chat_object_type' => 'claim',
            'chat_object' => $claim,
            'chat_items' => $claim->chat
        ])

    @endif

    {{-- js templates --}}

    @include('claim.inc._js_form')

</div>

@endsection