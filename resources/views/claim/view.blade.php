@extends('layout.user')

@section('title')

    @php
        $claimNumber = ($claim->product_code != null) ? '№ ' . fix_product_code($claim->product_code) : '№ ...';
        $claimDate = to_calendar_date($claim->date);
    @endphp

    Претензия {{ $claimNumber }} от {{ $claimDate }}

@endsection

@section('content')

<div class="container" id="tpl-claim-view">

    @if ($claim->ext_id == null)
        <div class="alert alert-warning">
            Режим редактирования недоступен. Отсутствует № претензии.
        </div>
    @endif

    <h1>
        Претензия {{ $claimNumber }}
        <span class="date">от {{ $claimDate }}</span>
        <span class="claim-title-status claim-title-status-{{ $claim->status }}">{{ claim_status_name($claim->status) }}</span>
    </h1>

    @if (user_account_type(['manager', 'admin']))
        <div class="content-title-addon">
            Клиент: <strong>{{ $claim->order->client->name }}</strong>
        </div>
    @endif

    <div class="claim-body">
        <div class="row">

            <div class="col-4">
                <h2>Данные</h2>
                <div class="white white-p-mini">
                    <ul class="claim-data">
                        <li>
                            <span>Связанная заявка на производство</span>
                            Заявка №{{ $claim->order->id }} от {{ to_calendar_date($claim->order->date) }}
                        </li>
                        @if ($claim->common_claim)
                            <li>
                                <span>Претензия</span>
                                {{ $claim->common_claim }}
                            </li>
                        @endif
                        @if ($claim->common_decision)
                            <li>
                                <span>Ответ</span>
                                {{ $claim->common_decision }}
                            </li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="col-8">

                <h2>Позиции из заявки</h2>
                
                @foreach ($claim->items as $item)
                    <div class="white">
                        <div class="claim-info">
                            <div class="claim-info-title">
                                Изделие:
                            </div>
                            <div class="claim-info-info">
                                {{ $item->nomenclature->title }}
                            </div>
                        </div>
                        <div class="claim-info">
                            <div class="claim-info-title">
                                Претензия:
                                <span>или проблема с изделием</span>
                            </div>
                            <div class="claim-info-info">
                                {{ $item->claim }}
                            </div>
                        </div>
                        <div class="claim-info">
                            <div class="claim-info-title">
                                Кол-во проблемных позиций:
                            </div>
                            <div class="claim-info-info">
                                {{ $item->cnt }} шт.
                            </div>
                        </div>
                        @if ($item->files()->count() > 0)
                            <div class="claim-info">
                                <div class="claim-info-title">
                                    Файлы:
                                </div>
                                <div class="claim-info-info">
                                    <div class="claim-files">
                                        @foreach ($item->files as $file)
                                            <a href="{{ url('shared/read-file/'. $file->hash) }}" target="_blank">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if ($item->decision)
                            <div class="claim-info">
                                <div class="claim-info-title">
                                    Решение:
                                </div>
                                <div class="claim-info-info">
                                    <div class="claim-result">
                                        {{ $item->decision }}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach

            </div>

        </div>
        
    </div>

    @if ($claim->ext_id != null)

        {{-- Чат --}}

        @include('_chat', [
            'h2_title' => 'Сообщения по претензии',
            'chat_object_type' => 'claim',
            'chat_object' => $claim,
            'chat_items' => $claim->chat
        ])

    @endif

    {{-- js templates --}}

    @include('claim.inc._js_view')

</div>

@endsection