@foreach ($claims as $claim)
    <div class="claim-item" data-link="{{ ($claim->status != 'new' && user_account_type('client')) ? url('claim/show/' . $claim->id) : url('claim/edit/' . $claim->id) }}">
        <div>
            <div class="claim-number"><span class="number">№</span> {{ ($claim->product_code != null) ? fix_product_code($claim->product_code) : '...' }}
                @if ($claim->chat_count > 0)
                    <span class="claim-notification">{{ $claim->chat_count }}</span>
                @endif
            </div>
        </div>
        <div class="claim-title">
            {{ $claim->common_claim }}
            <span>Претензия</span>
        </div>
        @if (user_account_type(['manager', 'admin']))
            <div>
                {{ $claim->order->client->name }}
                <span>Клиент</span>
            </div>
        @endif
        <div class="claim-status-cell">
            <div class="claim-status claim-status-{{ $claim->status }}">{{ claim_status_name($claim->status) }}</div>
            <div class="popup-menu-container">
                <div class="icon-more js-show-popup-menu"></div>
                <div class="popup-menu">
                    <ul>
                        <li>
                            @if ($claim->status != 'new' && user_account_type('client'))
                                <a href="{{ url('claim/show/' . $claim->id) }}">Просмотр</a>
                            @else
                                <a href="{{ url('claim/edit/' . $claim->id) }}">Изменить</a>
                            @endif
                        <li>
                            <a href="#" class="js-delete-claim">Удалить</a>
                            <form action="{{ url('claim/delete/' . $claim->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <span>Дата создания: {{ to_calendar_date($claim->date) }}</span>
        </div>
    </div>
@endforeach 