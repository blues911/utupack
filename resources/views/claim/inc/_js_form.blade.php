<script id="js-claim" type="text/template">
    <div class="white claim-hidden" data-id="<%= id %>">
        <div class="remove-claim"></div>
        <div class="label">
            <div class="label-title">Изделие</div>
            <select name="claim_items[<%= id %>][nomenclature_id]" class="claimselect">
                <option value="">Выбрать изделие</option>
            </select>
        </div>
        <div class="label">
            <div class="label-title">Претензия или проблема с изделием</div>
            <textarea class="form-control" name="claim_items[<%= id %>][claim]" placeholder="Опишите проблему"></textarea>
        </div>
        @if (user_account_type(['manager', 'admin']))
            <div class="label">
                <div class="label-title">Решение по претензии</div>
                <textarea class="form-control" name="claim_items[<%= id %>][decision]" placeholder="Напишите решение"></textarea>
            </div>
        @endif
        <div class="claim-bottom">
            <label class="input-container">
                <input type="number" class="form-control" name="claim_items[<%= id %>][cnt]">
                <span class="placeholder">Кол-во проблемных позиций, шт</span>
            </label>
            <div class="claim-files file-type-3">
                <div class="claim-files-list file-list-3"></div>
                <input type="hidden" name="claim_items[<%= id %>][delete_files]" value="[]">
                <input type="file" name="claim_items[<%= id %>][files][]" multiple accept=".jpg, .jpeg, .png, .tiff, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar">
                <span class="add-file-link u u-dotted">Загрузить файлы</span>
            </div>
        </div>
        <div class="errors"></div>
    </div>
</script>

<script id="js-nomenclatures" type="text/template">
    <option value="">Выбрать изделие</option>
    <% _.each(items, function (item, key) { %>
        <option value="<%= item.nomenclature_id %>" <%= (_.contains(selected_ids, item.nomenclature_id)) ? 'disabled' : '' %> ><%= item.nomenclature.title %></option>
    <% }); %>
</script>

<script id="js-validation-errors" type="text/template">
    <div class="alert alert-danger">
        <ul>
            <% _.each(items, function (error) { %>
                <li><%= error[0] %></li>
            <% }); %>
        </ul>
    </div>
</script>