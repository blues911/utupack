@extends('layout.page')

@section('title')

    {{ $code }}

@endsection

@section('content')
    
    <div class="error-page">

        <h1>{{ $code }}</h1>

        <p>{{ $message }}</p>

    </div>

@endsection