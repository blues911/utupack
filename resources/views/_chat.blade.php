<h2 class="chat-title" id="tpl-chat">{{ $h2_title }} (<span id="chat-messages-total">{{ $chat_items->count() }}</span>)</h2>

<div class="order-message-form">
    <div class="input-container">
        <form id="chat-form" action="{{ url('chat/ajax-send-message') }}" action="POST">
            <div id="text_area_div"></div>
            {{ csrf_field() }}
            <input type="hidden" name="object_type" value="{{ $chat_object_type }}">
            <input type="hidden" name="object_id" value="{{ $chat_object->id }}">
            <textarea name="message" rows="1" class="form-control textarea-magic-height" id="text_area"></textarea>
            <span class="placeholder">Введите сообщение</span>
            <label class="message-file">
                <input type="file" name="files[]" multiple accept=".jpg, .jpeg, .png, .tiff, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar">
                <span></span>
            </label>
            <span class="message-file-list"></span>
        </form>
    </div>
    <a href="#" id="js-send-message" class="button">Отправить</a>
</div>

<div class="errors"></div>

<div class="message-list">

    @include('_chat_messages', [
        'chat_items' => $chat_items,
        'is_ajax' => false
    ])

</div>

<script id="js-unread-messages" type="text/template">
    <div class="text-center">
          <div class="unread-messages">Новые сообщения</div>
    </div>
</script>