@extends('layout.user')

@section('title')

    @php
        $orderNumber = '';

        if ($order && $params['is_order_draft'] == false)
        {
            $orderNumber = ($order->product_code != null) ? '№ ' . fix_product_code($order->product_code) : '№ ...';
        }
        
        $orderDate = ($order && $params['is_order_draft'] == false) ? to_calendar_date($order->date) : date('d.m.Y');
    @endphp

    Заявка № {{ $orderNumber }} от {{ $orderDate }}

@endsection

@section('content')

<div class="container" id="tpl-order-form">

    {{-- Форма заявки --}}

    <div @if($params['view_mode'] == true) class="disabled-form" @endif>

        @if ($order && $order->product_code == null)
            <div class="alert alert-warning">
                Режим редактирования недоступен. Отсутствует № заявки.
            </div>
        @endif

        @if (settings('min_production_volume') > 0)
            <div class="alert alert-info">
                Минимальный объем заявки {{ settings('min_production_volume') }} м².
            </div>
        @endif

        <h1>
            Заявка {{ $orderNumber }}
            <span class="date">от {{ $orderDate }}</span>
            @if ($order)
                @php
                    $orderStatus = ($params['is_order_draft'] == true) ? 'new' : $order->status;
                @endphp
                <span class="order-title-status order-title-status-{{ $orderStatus }}">{{ order_status_name($orderStatus) }}</span>
            @endif
        </h1>

        @if ($order && $params['is_order_draft'] == true)
            <div class="content-title-addon">
                Копия заявки №: <strong>{{ fix_product_code($order->product_code) }}</strong>
            </div>
        @endif

        @if (user_account_type(['manager', 'admin']))
            <div class="content-title-addon">
                Клиент: <a href="{{ url('order/list?date_range=&status=&by_date=&client='.$params['client']->id) }}" class="client-filter-link u u-dotted">{{ $params['client']->name }}</a>
            </div>
        @endif

        <form id="order-form" action="{{ ($order && $params['is_order_draft'] == false) ? '/order/ajax-update/' . $order->id : '/order/ajax-store' }}" method="POST" enctype="multipart/form-data" data-default-execution-time="{{ settings('default_execution_time') }}">

            {{ csrf_field() }}
            
            <input type="hidden" name="user_id" value="{{ $params['client']->id }}">

            @if ($order && $params['is_order_draft'] == true)
                <input type="hidden" name="id" value="{{ $order->id }}">
            @endif

            <div class="order-positions-panel fix-for-order">
                @if (user_account_type(['manager', 'admin']))
                    <div>
                        <span class="panel-item-title">Статус:</span>
                        <select name="status" class="styled">
                            @foreach ($params['status_types'] as $key => $name)
                                <option value="{{ $key }}" {{ ($order && $order->status == $key) ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div>
                    <span class="panel-item-title">Покупатель:</span>
                    <select name="buyer_ext_id" class="styled" data-buyer-agreement-id="{{ ($order) ? $order->agreement_id : '' }}">
                        <option value="">Выбрать покупателя</option>
                        @foreach ($params['buyers'] as $buyer)
                            <option value="{{ $buyer->buyer_ext_id }}" {{ ($order && $order->buyer_ext_id == $buyer->buyer_ext_id) ? 'selected' : '' }} data-agreements="{{ $buyer->buyerAgreements->toJson() }}">{{ $buyer->buyer_title }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <span class="panel-item-title">Грузополучатель: </span>
                    <select name="consignee_id" class="styled">
                        <option value="">Выбрать грузополучателя</option>
                        @foreach ($params['consignees'] as $consignee)
                            <option value="{{ $consignee->id }}" {{ ($order && $order->consignee_id == $consignee->id) ? 'selected' : '' }}>{{ $consignee->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <span class="panel-item-title">Договор:</span>
                    <select name="agreement_id" class="styled">
                        <option value="">Выбрать договор</option>
                    </select>
                </div>
                <div>
                    <span class="panel-item-title">Тип поставки:</span>
                    <select name="delivery_type" class="styled">
                        <option value="delivery" {{ ($order && $order->delivery_type == 'delivery') ? 'selected' : '' }}>Доставка</option>
                        <option value="pickup" {{ ($order && $order->delivery_type == 'pickup') ? 'selected' : '' }}>Самовывоз</option>
                    </select>
                </div>
                <div>
                    @php
                        $desiredDateTitle = 'Дата доставки:';
                        if ($order)
                        {
                            $desiredDateTitle = ($order->delivery_type == 'pickup') ? 'Дата отгрузки:' : 'Дата доставки:';
                        }
                    @endphp
                    <span class="panel-item-title">{{ $desiredDateTitle }}</span>
                    <div class="date-container desired-date">
                        <div class="date-result {{ ($order && !is_null($order->desired_date)) ? $order->desired_date : 'empty' }}">{{ ($order && !is_null($order->desired_date)) ? $order->desired_date : '——.——.————' }}</div>
                        <div class="date-popup">
                            <input type="text" class="form-control datepicker-input" name="desired_date" value="{{ ($order) ? $order->desired_date : '' }}" data-date="{{ ($order) ? date('Y-m-d', strtotime($order->date)) : '' }}">
                            <span class="date-reset"><a href="#">Очистить дату</a></span>
                            <div class="datepicker"></div>
                            <span class="date-help invis"></span>
                        </div>
                    </div>
                </div>
                <div class="number-in">
                    <label class="input-container">
                        <input type="text" class="form-control {{ ($order && $order->request_number != '') ? 'not-empty' : '' }}" name="request_number" value="{{ $order ? $order->request_number : '' }}">
                        <span class="placeholder">Внутренний № заявки</span>
                        <span class="help" data-toggle="tooltip" title="Введите внутренний номер заявки">?</span>
                    </label>
                </div>
            </div>

            <div class="order-positions-title">
        
                <h2>Позиции</h2>
                
                <div class="order-positions-inputs">
                    <label class="input-container">
                        <input type="text" class="form-control search-by" name="search[nomenclature_sku]">
                        <span class="placeholder">Код изделия</span>
                    </label>
                    <label class="input-container">
                        <input type="text" class="form-control search-by" name="search[nomenclature_title]">
                        <span class="placeholder">Наименование</span>
                    </label>
                </div>
            
                <div class="order-positions-btn-container">
                    <a href="#" id="add-position" class="button">Добавить</a>
                </div>
                
            </div>

            <input type="hidden" name="items" value="{{ ($order && $order->items) ? $order->items->toJson() : '[]' }}">
            <input type="hidden" name="items_deleted" value="[]">

            <div class="order-positions-container">

                <div class="order-position-list">
                    @if ($order && $order->items)
                        @foreach ($order->items as $item)
                            <div class="order-position-item" data-id="{{ $item->id }}" data-search-nomenclature-sku="{{ strtolower($item->nomenclature->sku) }}" data-search-nomenclature-title="{{ strtolower($item->nomenclature->title) }}" data-selected-nomenclature-id="{{ $item->nomenclature->id }}" data-nomenclature="{{ $item->nomenclature->toJson() }}">
                                <div>
                                    @if ($item->address != '' && $item->address_number > 0)
                                        <span class="order-position-number">{{ $item->address_number }}</span>
                                    @endif
                                    {{ $item->address }}
                                </div>
                                <div>
                                    @if ($item->nomenclature->sku)
                                        <a href="#" class="order-position-code u u-dotted" @if($item->comment) data-toggle="tooltip" title="{{ $item->comment}}" @endif>{{ $item->nomenclature->sku }}</a>
                                    @else
                                        —
                                    @endif
                                    <span>Код изделия</span>
                                </div>
                                <div>
                                    {{ $item->nomenclature->title }}
                                    <span>{{ $item->nomenclature->print ? 'С печатью' : 'Без печати' }}</span>
                                </div>
                                <div>
                                    <div>
                                        {{ snumb_format($item->cnt) }}
                                        @if ($item->amount > 0 && $params['is_order_draft'] == false)
                                            <span>x {{ snumb_format($item->amount / $item->cnt) }} руб.</span>
                                        @endif
                                    </div>
                                    <span>
                                        @if ($item->amount > 0 && $params['is_order_draft'] == false)
                                            Стоимость: {{ snumb_format($item->amount) }} руб.
                                        @else
                                            Кол-во
                                        @endif
                                    </span>
                                </div>
                                <div>
                                    @php
                                        $area = ($item->cnt > 1) ? $item->cnt * $item->nomenclature->area : $item->nomenclature->area;
                                    @endphp
                                    {{ snumb_format($area) }}
                                    <span>Итоговая площадь,&nbsp;м²</span>
                                    <div class="popup-menu-container">
                                        <div class="icon-more js-show-popup-menu"></div>
                                        <div class="popup-menu">
                                            <ul>
                                                <li><a href="#" class="edit-position" data-id="{{ $item->id }}">Изменить</a></li>
                                                <li><a href="#" class="delete-position" data-id="{{ $item->id }}">Удалить</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                @if ($order && $params['is_order_draft'] == false)
                    @php
                        $amount = 0;
                        foreach ($order->items as $item) {
                            $amount += $item->amount;
                        }
                    @endphp
                    @if ($amount > 0)
                        <div class="order-sum">
                          Общая стоимость: {{ snumb_format($amount) }} руб.
                        </div>
                    @endif
                @endif

            </div>

            <h2>Дополнительная информация</h2>
      
            <div class="oder-add-info">
                <div class="oder-add-comment">
                    <label class="input-container">
                        <textarea name="comment" cols="30" class="form-control {{ ($order && $order->comment != '') ? 'not-empty' : '' }}">{{ $order ? $order->comment : '' }}</textarea>
                        <span class="placeholder">Комментарий к заявке</span>
                    </label>
                </div>
                <div class="add-file">
                    <input type="file" name="file" accept=".jpg, .jpeg, .png, .tiff, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar">
                    <span class="order-add-file">
                        <span class="add-file-name" data-text="Доверенность для водителя">
                            @if ($order && $params['is_order_draft'] == false && $order->file)
                                <span>
                                    <a href="{{ url('shared/read-file/' . $order->file->hash) }}" target="_blank">{{ $order->file->name }}</a>
                                    <span class="remove-file" data-hash="{{ $order->file->hash }}"></span>
                                </span>
                            @else
                                Доверенность для водителя
                            @endif
                        </span>
                        @if ($params['view_mode'] == false)
                            <a href="#" class="add-file-link u u-dotted">Загрузить скан</a>
                        @else
                            <span class="add-file-link u u-dotted disabled">Загрузить скан</span>
                        @endif
                    </span>
                </div>
            </div>

            <div class="errors"></div>
            
            @if ($params['view_mode'] == false)
                <div class="text-center">
                    <a href="#" id="js-save-order" class="button button-lg btn-submit">Сохранить</a>
                </div>
            @endif

        </form>

    </div>

    {{-- Чат --}}

    @if ($order && $order->ext_id != null && $params['is_order_draft'] == false)
        
        @include('_chat', [
            'h2_title' => 'Сообщения по заявке',
            'chat_object_type' => 'order',
            'chat_object' => $order,
            'chat_items' => $order->chat
        ])

    @endif

    {{-- Модальное окно --}}

    <div class="modal modal-v-center fade" id="modal-position">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <h2 data-title1="Добавление позиции" data-title2="Изменение позиции"></h2>
                </div>
                <div class="modal-body">

                    <div class="order-positions-panel">
                        <div>
                            <span class="panel-item-title">Марка:</span>
                            <select name="filter[cardboard_type]" class="styled filter-by filter-by-cardboard-type">
                                <option value="">Все</option>
                                @foreach ($params['cardboard_types'] as $cardboard_type)
                                    <option value="{{ $cardboard_type->id }}">{{ $cardboard_type->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <span class="panel-item-title">Профиль: </span>
                            <select name="filter[cardboard_profile]" class="styled filter-by filter-by-cardboard-profile">
                                <option value="">Все</option>
                                @foreach ($params['cardboard_profiles'] as $cardboard_profile)
                                    <option value="{{ $cardboard_profile->id }}">{{ $cardboard_profile->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <span class="panel-item-title">Цвет:</span>
                            <select name="filter[cardboard_color]" class="styled filter-by filter-by-cardboard-color">
                                <option value="">Все</option>
                                @foreach ($params['cardboard_colors'] as $cardboard_color)
                                    <option value="{{ $cardboard_color->id }}">{{ $cardboard_color->title }}</option>
                                @endforeach
                          </select>
                        </div>
                        <div>
                            <span class="panel-item-title">Печать:</span>
                            <select name="filter[nomenclature_print]" class="styled filter-by filter-by-nomenclature-print">
                                <option value="">Все</option>
                                <option value="0">Без печати</option>
                                <option value="1">С печатью</option>
                            </select>
                        </div>
                        <div class="number-in">
                            <label class="input-container">
                                <input type="text" class="form-control filter-by filter-by-nomenclature-search" name="filter[nomenclature_search]">
                                <span class="placeholder">Поиск по наименованию или коду изделия</span>
                                <span class="help" data-toggle="tooltip" title="Поиск работает по наименованию или коду изделия">?</span>
                            </label>
                        </div>
                    </div>

                    <div class="order-positions-popup-container">
                        <table>
                            <thead>
                                <tr>
                                    <th>Наименование</th>
                                    <th>Код изделия</th>
                                    <th>Картон</th>
                                    <th>Печать</th>
                                    <th>Площадь,&nbsp;м²</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <p class="invis"></p>
                    </div>

                    <input type="hidden" name="order_item[id]">
                    <input type="hidden" name="order_item[nomenclature]">

                    <div class="modal-inputs claim-body">
                        <div class="label modal-address">
                            <div class="label-title">Адрес доставки</div>
                            <div class="modal-address-input invis">
                                <div class="input-container">
                                    <input type="text" class="form-control" name="order_item[address]" value="">
                                    <span class="placeholder">Новый адрес</span>
                                </div>
                                <a href="#" id="js-add-address" class="button">Добавить</a>
                            </div>
                            <div class="modal-address-select">
                                <select name="order_item[address]" class="styled">
                                    @if ($params['addresses'])
                                        @foreach ($params['addresses'] as $address)
                                            <option value="{{ $address }}">{{ $address }}</option> 
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <a href="#" class="modal-address-new u u-dotted">Добавить адрес</a>
                        </div>
                        <div class="label modal-point">
                            <div class="label-title">Номер точки выгрузки</div>
                            <input type="number" class="form-control" name="order_item[address_number]" value="">
                            <span class="count-plus">+</span>
                            <span class="count-minus">–</span>
                        </div>
                        <div class="label modal-kol">
                            <div class="label-title">Количество</div>
                            <select name="order_item[cnt_rule]" class="styled" >
                                <option value="0">Не важно</option>
                                <option value="1">Не больше</option>
                                <option value="2">Не меньше</option>
                                <option value="4">Точно</option>
                            </select>
                        </div>
                        <div class="label modal-point">
                            <div class="label-title">шт.</div>
                            <input type="number" class="form-control" name="order_item[cnt]" value="">
                            <span class="count-plus">+</span>
                            <span class="count-minus">–</span>
                        </div>
                    </div>

                    <div class="modal-comment">
                        <label class="input-container">
                            <textarea name="order_item[comment]" cols="30" class="form-control"></textarea>
                            <span class="placeholder">Комментарии к позиции (порядок загрузки для адреса доставки, особые пожелания)</span>
                        </label>
                    </div>
                    <div class="errors"></div>
                    <div class="text-center">
                        <button class="button" id="js-save-position">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- js templates --}}

    @include('order.inc._js_form')

</div>

@endsection