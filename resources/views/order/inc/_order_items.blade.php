@foreach($orders as $order)
    <div class="order-item" data-link="{{ ($order->status != 'new' && user_account_type('client')) ? url('order/show/' . $order->id) : url('order/edit/' . $order->id) }}">
        <div>
            <div class="order-number">
                <span class="number">№</span> {{ ($order->product_code != null) ? fix_product_code($order->product_code) : '...' }}
                @if ($order->chat_count > 0)
                    <span class="order-notification">{{ $order->chat_count }}</span>
                @endif
            </div>
            <span>{{ (!empty($order->request_number)) ? $order->request_number : '' }}</span>
        </div>
        <div class="order-date-start {{ (!$order->ready_date) ? 'fix-order-date' : '' }}">
            @if ($order->desired_date)
                {{ to_calendar_date($order->desired_date) }}
                <span>Отгрузка</span>
            @else
                &nbsp;
            @endif
        </div>
        <div>
            @if ($order->ready_date)
                {{ to_calendar_date($order->ready_date) }}
                <span>Доставка</span>
            @else
                &nbsp;
            @endif
        </div>
        <div class="{{ !user_account_type(['manager', 'admin']) ? 'fix-for-client' : '' }}">
            {{ order_delivery_name($order->delivery_type) }}
            <span>Тип поставки</span>
        </div>
        @if (user_account_type(['manager', 'admin']))
            <div>
                {{ $order->client->name }}
                <span>Клиент</span>
            </div>
        @endif
        <div class="order-positions-cell {{ !user_account_type(['manager', 'admin']) ? 'fix-for-client' : '' }}">
            <a href="#" class="order-positions u u-dotted js-show-positions">Позиции</a>
            @if (!empty($order->items))
                <div class="order-positions-popup">
                    <div class="order-positions-popup-container">
                        <table>
                            <thead>
                                <tr>
                                    <th>Код товара</th>
                                    <th>Наименование</th>
                                    <th>Итоговая площадь,&nbsp;м²</th>
                                    <th>Кол-во</th>
                                    <th>Стоимость</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($order->items as $item)
                                    <tr>
                                        <td>{{ ($item->nomenclature->sku) ? $item->nomenclature->sku : '—' }}</td>
                                        <td>{{ $item->nomenclature->title }}</td>
                                        @php
                                            $area = ($item->cnt > 1) ? $item->cnt * $item->nomenclature->area : $item->nomenclature->area;
                                        @endphp
                                        <td>{{ snumb_format($area) }}</td>
                                        <td>
                                            {{ snumb_format($item->cnt) }}
                                            @if ($item->amount > 0)
                                                <span>x {{ snumb_format($item->amount / $item->cnt) }} руб.</span>
                                            @endif
                                        </td>
                                        <td>{{ ($item->amount > 0) ? snumb_format($item->amount) .' руб.' : '—'}}</td>
                                    </tr>
                                    @php
                                        $total += ($item->amount > 0) ? $item->amount : 0
                                    @endphp
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Итого</td>
                                    <td>{{ ($total > 0) ? snumb_format($total) .' руб.' : '—' }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @if ($total > 0)
                    <span>{{ snumb_format($total) .' руб.' }}</span>
                @endif
            @endif
        </div>
        <div class="order-status-cell">
            <div class="order-status order-status-{{ $order->status }}">{{ order_status_name($order->status) }}</div>
            <div class="popup-menu-container">
                <div class="icon-more js-show-popup-menu"></div>
                <div class="popup-menu">
                    <ul>
                        <li>
                            @if ($order->status != 'new' && user_account_type('client'))
                                <a href="{{ url('order/show/' . $order->id) }}">Просмотр</a>
                            @else
                                <a href="{{ url('order/edit/' . $order->id) }}">Изменить</a>
                            @endif
                        </li>
                        @if ($order->product_code != null)
                            <li>
                                <a href="#" class="js-copy-order">Копировать</a>
                                <form action="{{ url('order/copy/' . $order->id) }}" method="POST">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endif
                        <li>
                            <a href="#" class="js-delete-order">Удалить</a>
                            <form action="{{ url('order/delete/' . $order->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <span>Дата создания: {{ to_calendar_date($order->date) }}</span>
        </div>
    </div>
@endforeach