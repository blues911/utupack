<script id="js-buyer-agreements" type="text/template">
    <option value="">Выбрать договор</option>
    <% _.each(items, function (agreement, key) { %>
        <option value="<%= agreement.id %>" data-agreement='<%= JSON.stringify(agreement) %>'>№ <%= agreement.number %> от <%= to_agreement_date(agreement.date) %></option>
    <% }); %>
</script>

<script id="js-modal-nomenclatures" type="text/template">
    <% _.each(items, function (nomenclature, key) { %>
        <tr data-nomenclature='<%= JSON.stringify(nomenclature) %>' class="<%= (_.contains(selected_ids, nomenclature.id)) ? 'disabled' : '' %>">
            <td><%= nomenclature.title %></td>
            <td><%= (nomenclature.sku) ? nomenclature.sku : '—' %></td>
            <td><%= nomenclature.cardboard_type.title %></td>
            <td><%= (nomenclature.print == 1) ? 'да' : 'нет' %></td>
            <%
                var area = Number(nomenclature.area).toLocaleString('en-US', {maximumFractionDigits: 2, minimumFractionDigits: 2}).replace(/,/i, ' ');
                if (String(area).search(/\.00/i) > 0) {
                    area = String(area).replace(/\.00/i, '');
                }
            %>
            <td><%= area %></td>
        </tr>
    <% }); %>
</script>

<script id="js-order-position" type="text/template">
    <div class="order-position-item" data-id="<%= item.id %>" data-search-nomenclature-sku="<%= item.nomenclature.sku %>" data-search-nomenclature-title="<%= item.nomenclature.title %>" data-selected-nomenclature-id="<%= item.nomenclature.id %>" data-nomenclature='<%= JSON.stringify(item.nomenclature) %>'>
        <div>
            <% if (item.address != '' && Number(item.address_number) > 0) { %>
                <span class="order-position-number"><%= item.address_number %></span>
            <% } %>
            <%= item.address %>
        </div>
        <div>
            <a href="#" class="order-position-code u u-dotted" data-toggle="tooltip" title="<%= item.comment %>"><%= item.sku %></a>
            <span>Код изделия</span>
        </div>
        <div>
            <%= item.title %>
            <span><%= (item.nomenclature.print == 1) ? 'С печатью' : 'Без печати' %></span>
        </div>
        <div>
            <%= Number(item.cnt).toLocaleString().replace(/,/i, ' ') %>
            <span>Кол-во</span>
        </div>
        <div>
            <%
                var area;
                if (Number(item.cnt) > 1) {
                    area = Number(item.cnt) * Number(item.area);
                    area = Number(area).toLocaleString('en-US', {maximumFractionDigits: 2, minimumFractionDigits: 2}).replace(/,/i, ' ');
                    if (String(area).search(/\.00/i) > 0) {
                        area = String(area).replace(/\.00/i, '');
                    }
                } else {
                    area = Number(item.area).toLocaleString('en-US', {maximumFractionDigits: 2, minimumFractionDigits: 2}).replace(/,/i, ' ');
                    if (String(area).search(/\.00/i) > 0) {
                        area = String(area).replace(/\.00/i, '');
                    }
                }
            %>
            <%= area %>
            <span>Итоговая площадь,&nbsp;м²</span>
            <div class="popup-menu-container">
                <div class="icon-more js-show-popup-menu"></div>
                <div class="popup-menu">
                    <ul>
                        <li><a href="#" class="edit-position" data-id="<%= item.id %>">Изменить</a></li>
                        <li><a href="#" class="delete-position" data-id="<%= item.id %>">Удалить</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="js-validation-errors" type="text/template">
    <div class="alert alert-danger">
        <ul>
            <% _.each(items, function (error) { %>
                <li><%= error[0] %></li>
            <% }); %>
        </ul>
    </div>
</script>