@extends('layout.user')

@section('title')

    Заявки

@endsection

@section('content')

<div class="container" id="tpl-order-index">

    <div class="flex-title">

        <h1>Заявки</h1>

        <div class="orders-filter {{ (user_account_type(['manager', 'admin'])) ? 'fix-for-manager' : '' }}">
          
            <div class="orders-filter-container">
                <div class="panel-item-inline">
                    <span class="panel-item-title">Статус:</span>
                    <select name="filter[status]" class="styled">
                        <option value="">Все</option>
                        @foreach ($params['status_types'] as $key => $name)
                            <option value="{{ $key }}" {{ (!empty($params['filters'] && $params['filters']['status'] == $key)) ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="panel-item-inline">
                    <select name="filter[by_date]" class="styled">
                        <option value="">По дате заявки</option>
                        @foreach($params['date_types'] as $key => $name)
                            <option value="{{ $key }}" {{ (!empty($params['filters'] && $params['filters']['by_date'] == $key)) ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                @if (user_account_type(['manager', 'admin']))
                    <div class="panel-item-inline">
                        <select name="filter[client]" class="styled">
                            <option value="">Все клиенты</option>
                            @foreach ($params['clients'] as $client)
                                <option value="{{ $client->id }}" {{ (!empty($params['filters'] && $params['filters']['client'] == $client->id)) ? 'selected' : '' }}>{{ $client->name }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="panel-item-inline date-range">
                    <div class="date-container">
                        <div class="date-result date-result-range {{ (!empty($params['filters'] && $params['filters']['date_range'] != '')) ? '' : 'empty' }}">{{ (!empty($params['filters'] && $params['filters']['date_range'] != '')) ? $params['filters']['date_range'] : 'выберите период' }}</div>
                        <div class="date-popup">
                            <input type="text" class="form-control datepicker-input" name="filter[date_range]" value="{{ (!empty($params['filters'] && $params['filters']['date_range'] != '')) ? $params['filters']['date_range'] : '' }}">
                            <span class="date-reset"><a href="#">Очистить дату</a></span>
                            <div class="datepicker-range"></div>
                        </div>
                    </div>
                </div>
            </div>

            @if (user_account_type(['manager', 'admin']))

                <a href="#modal-clients" data-toggle="modal" class="button">Добавить</a>

                <div class="modal modal-v-center fade" id="modal-clients">
                    <div class="modal-dialog modal-dialog-mini">
                        <div class="modal-content modal-content-mini">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"></button>
                                <h2>Выбрать клиента для заявки</h2>
                            </div>
                            <div class="modal-body modal-clients">
                                <div class="modal-inputs">
                                    <div class="label clients-select">
                                        <select name="client_id" class="styled">
                                            <option value="">Список клиентов</option>
                                            @foreach ($params['clients'] as $client)
                                                <option value="{{ $client->id }}">{{ $client->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="errors"></div>
                                <div class="text-center">
                                    <button class="button" id="js-confirm-client">Подтвердить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @else
        
                <a href="{{ url('order/create') }}" class="button">Добавить</a>
        
            @endif

        </div>

    </div>

    <div class="orders">

        @if (!$orders->isEmpty())

            @include('order.inc._order_items', ['orders' => $orders])

        @else

            <p>Заявки не найдены</p>

        @endif

    </div>

    @if ($orders_total > $params['filters']['limit'])
        <div class="text-center">
            <div class="button button-more js-load-more">Показать ещё</div>		
        </div>
    @endif

    {{-- js templates --}}

    @include('order.inc._js_index')

</div>

@endsection