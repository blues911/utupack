@extends('layout.auth')

@section('title')

    Вход

@endsection

@section('content')

    <div class="login-form">

        <form action="{{ url('login') }}" method="POST">

            <h2>Личный кабинет клиента</h2>

            {{ csrf_field() }}
            <input type="hidden" name="remember" value="on">

            <div class="input-container">
                <input type="text" class="form-control" name="login">
                <span class="placeholder">Логин</span>
            </div>

            <div class="input-container">
                <input type="password" class="form-control" name="password">
                <span class="placeholder">Пароль</span>
            </div>

            <div class="login-button">
                <button type="submit" class="button">Войти</button>
                @if (isset($errors) && $errors->any())
                    <div class="login-error">Неверный логин или пароль</div>
                @endif
            </div>

            <div class="login-help">
                Если вы забыли логин и пароль от личного кабинета, обратитесь с своему персональному менеджеру
            </div>

        </form>

    </div>

@endsection