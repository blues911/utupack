<footer class="footer">
    <div class="container container--footer">
        <div class="footer-links">
            <div>© {{ date('Y') }} Унитехупак — сохраняя самое ценное</div>
            {{-- <a href="{{ url('shared/terms-of-use') }}">Пользовательское соглашение</a> --}}
        </div>
        <div class="creators">Разработка проекта – <a href="https://cubadesign.ru" target="_blank">CUBA</a></div>
    </div>
</footer>