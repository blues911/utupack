@extends('layout.base')

@section('main')

    <div class="overlay"></div>

    <header class="header">

        <div class="logo">
            <a href="{{ url('') }}">
                <img src="{{ asset('img/logo.png') }}" alt="logo">
            </a>
        </div>

        <div class="container container--header">
            <div class="slogan">Быстрая обработка ваших заказов</div>
        </div>    

    </header>

    <div class="page">

        <div class="content page-content">

            <div>
                <div class="container">

                    @yield('content')

                </div>
            </div>

            @include('layout._footer')

        </div>

    </div>

@endsection