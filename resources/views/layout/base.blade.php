<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" dir="ltr" lang="ru">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=1600">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="account_type" content="{{ (auth()->user()) ? auth()->user()->account_type  : 'guest' }}">

    <title>ЛК клиента УниТехУпак — @yield('title')</title>
    <meta name="description" content="-----" />

    <!-- Теги для соцсеточек и тп -->
    <meta property="og:title" content="Заголовок страницы" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="-----" />
    <meta property="og:image" content="-----" />
    <meta property="og:site_name" content="-----" />
    
    <!-- favicon & manifest -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('favicon/safari-pinned-tab.svg') }}" color="#ff1f30">
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}">
    <meta name="msapplication-TileColor" content="#e10c14">
    <meta name="msapplication-config" content="{{ asset('favicon/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Стили -->
    <link rel="stylesheet" href="{{ asset('css/base.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/select/select.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css?=v16') }}">
</head>

<body>

    @yield('main')

    <!-- Скрипты -->
    <script src="{{ asset('lib/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('lib/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('lib/jquery-ui/jquery.datepicker.extension.range.min.js') }}"></script>
    <script src="{{ asset('lib/jquery-ui/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ asset('lib/underscore-min.js') }}"></script>
    <script src="{{ asset('lib/bootstrap.min.js') }}"></script>
    <script src="{{ asset('lib/select/select.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/app.js?v=2') }}"></script>

</body>
</html>