@extends('layout.base')

@section('main')

    <div class="overlay"></div>

    <header class="header">

        <div class="logo">
            <a href="{{ url('') }}">
                <img src="{{ asset('img/logo.png') }}" alt="logo">
            </a>
        </div>

        <div class="container container--header">

            <div class="slogan">Быстрая обработка ваших заказов</div>

            <div class="user">
                <div class="popup-menu-container">
                    <div class="icon-more js-show-popup-menu"><span class="profile-icon"></span>{{ auth()->user()->name }}</div>
                    <div class="popup-menu">
                        <ul>
                            <li class="profile-li">
                                <a href="{{ url('order/list') }}" class="profile-link"><span class="profile-icon"></span>{{ auth()->user()->name }}</a>
                            </li>
                            @if (user_account_type('client'))
                                <li>
                                    <a href="{{ url('profile') }}">Профиль</a>
                                </li>
                            @endif
                            <li>
                                <a href="#" id="logout">Выход</a>
                                <form action="{{ url('logout') }}" method="POST">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>    

    </header>

    <div class="page">

        <aside class="sidebar">

            <ul class="main-menu">
                @foreach ($menu as $m)
                    <li class="{{ highlight_menu($m['path']) ? 'active' : '' }}">
                        <a href="{{ url($m['path']) }}">
                            <span class="main-menu-icon icon-{{ $m['icon'] }}"></span>
                            @if (array_key_exists('chat_messages', $m) && $m['chat_messages'] > 0)
                                <span class="main-menu-notifications">{{ $m['chat_messages'] }}</span>
                            @endif
                            {{ $m['title'] }}
                        </a>
                    </li>
                @endforeach
            </ul>

            @if (isset($manager))
                <div class="your-manager">
                    @if ($manager->file)
                        <img src="{{ url('shared/read-file/' . $manager->file->hash . '?open=1') }}" alt="Ваш менеджер">
                    @endif
                    <div class="your-manager_name">
                        {{ $manager->name }}
                        @if (user_account_type('client'))
                            <span>Ваш менеджер</span>
                        @endif
                    </div>
                    <div class="your-manager_contacts">
                        <a href="mailto:{{ $manager->email }}" @if(strlen($manager->email) > 16)style="font-size:12px;"@endif>{{ $manager->email }}</a>
                        <a href="tel:{{ $manager->phone }}">{{ $manager->phone }}</a>
                    </div>
                </div>
            @endif
            
        </aside>

        <div class="content">

            <div>
              
                @yield('content')
                    
            </div>

            @include('layout._footer')

        </div>

    </div>

@endsection