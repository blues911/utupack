@if (!empty($chat_items))

    @foreach ($chat_items as $chat_item)

        @php
            $classInOut = (auth()->user()->id == $chat_item->user_id) ? 'out' : 'in';
            $classMessageNew = '';
            $classMessageUnread = '';

            if (!$is_ajax && $classInOut == 'in' && user_account_type('client') && !$chat_item->read_by_client
                || !$is_ajax && $classInOut == 'in' && user_account_type('manager') && !$chat_item->read_by_manager
                || !$is_ajax && $classInOut == 'in' && user_account_type('admin') && !$chat_item->read_by_admin)
            {
                $classMessageUnread = 'message-unread';
            }

            if ($is_ajax)
            {
                $classMessageNew = 'message-new';
            }
        @endphp

        <div class="message message-{{ $classInOut }} {{ $classMessageNew }} {{ $classMessageUnread }}">
            <p>{!! nl2br($chat_item->message) !!}</p>
            <div class="message-bottom">
                @if (!empty($chat_item->files))
                    <div class="message-files">
                        @foreach ($chat_item->files as $file)
                            <a href="{{ url('shared/read-file/' . $file->hash) }}">{{ $file->name }}</a>
                        @endforeach
                    </div>
                @endif
                <div class="message-info">
                    <a href="javascript:void(0);" class="message-author">{{ $chat_item->user->name }}</a>
                    <div class="message-date">, {{ to_chat_date($chat_item->date) }}</div>
                </div>
            </div>
        </div>

    @endforeach

@endif