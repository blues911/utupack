
@foreach ($samples as $sample)
    <div class="sample-item" data-link="{{ ($sample->status != 'new' && user_account_type('client')) ? url('sample/show/' . $sample->id) : url('sample/edit/' . $sample->id) }}">
        <div>
            <div class="sample-number">
                <span class="number">№</span> {{ ($sample->product_code != null) ? fix_product_code($sample->product_code) : '...' }}
                @if ($sample->chat_count > 0)
                    <span class="sample-notification">{{ $sample->chat_count }}</span>
                @endif
            </div>
            <span>{{ (!empty($sample->request_number)) ? $sample->request_number : '' }}</span>
        </div>
        <div class="sample-title">
            {{ $sample->nomenclatureType->title }}
            <span>Вид изделия</span>
        </div>
        <div>
            {{ sample_delivery_name($sample->delivery_type) }}
            <span>Тип поставки</span>
        </div>
        @if (user_account_type(['manager', 'admin']))
            <div>
                {{ $sample->client->name }}
                <span>Клиент</span>
            </div>
        @endif
        <div class="sample-status-cell">
            <div class="sample-status sample-status-{{ $sample->status }}">{{ sample_status_name($sample->status) }}</div>
            <div class="popup-menu-container">
                <div class="icon-more js-show-popup-menu"></div>
                <div class="popup-menu">
                    <ul>
                        <li>
                            @if ($sample->status != 'new' && user_account_type('client'))
                                  <a href="{{ url('sample/show/' . $sample->id) }}">Просмотр</a>
                              @else
                                  <a href="{{ url('sample/edit/' . $sample->id) }}">Изменить</a>
                              @endif
                        </li>
                        <li>
                            <a href="#" class="js-copy-sample">Копировать</a>
                            <form action="{{ url('sample/copy/' . $sample->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>
                            <a href="#" class="js-delete-sample">Удалить</a>
                            <form action="{{ url('sample/delete/' . $sample->id) }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <span>Дата создания: {{ to_calendar_date($sample->date) }}</span>
        </div>
    </div>
@endforeach