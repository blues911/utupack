<script id="js-buyer-agreements" type="text/template">
    <option value="">Выбрать договор</option>
    <% _.each(items, function (agreement, key) { %>
        <option value="<%= agreement.id %>">№ <%= agreement.number %> от <%= to_agreement_date(agreement.date) %></option>
    <% }); %>
</script>

<script id="js-validation-errors" type="text/template">
    <div class="alert alert-danger">
        <ul>
            <% _.each(items, function (error) { %>
                <li><%= error[0] %></li>
            <% }); %>
        </ul>
    </div>
</script>