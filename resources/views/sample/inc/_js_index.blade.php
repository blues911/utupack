<script id="js-validation-errors" type="text/template">
    <div class="alert alert-danger">
        <ul>
            <% _.each(items, function (error) { %>
                <li><%= error[0] %></li>
            <% }); %>
        </ul>
    </div>
</script>