@extends('layout.user')

@section('title')

    @php
        $sampleNumber = '';

        if ($sample && $params['is_sample_draft'] == false)
        {
            $sampleNumber = ($sample->product_code != null) ? '№ ' . fix_product_code($sample->product_code) : '№ ...';
        }
    
        $sampleDate = ($sample) ? to_calendar_date($sample->date) : date('d.m.Y');
    @endphp

    Образец {{ $sampleNumber }} от {{ $sampleDate }}
    
@endsection

@section('content')

<div class="container" id="tpl-sample-form">

    {{-- Форма образца --}}

    <div @if($params['view_mode'] == true) class="disabled-form" @endif>

        @if ($sample && $sample->product_code == null)
            <div class="alert alert-warning">
                Режим редактирования недоступен. Отсутствует № образца.
            </div>
        @endif

        <h1>
            Образец {{ $sampleNumber }}
            <span class="date">от {{ $sampleDate }}</span>
            @if ($sample)
                <span class="sample-title-status sample-title-status-{{ $sample->status }}">{{ sample_status_name($sample->status) }}</span>
            @endif
        </h1>

        @if ($sample && $params['is_sample_draft'] == true)
            <div class="content-title-addon">
                Копия образца №: <strong>{{ fix_product_code($sample->product_code) }}</strong>
            </div>
        @endif

        @if (user_account_type(['manager', 'admin']))
            <div class="content-title-addon">
                Клиент: <a href="{{ url('sample/list?date_range=&status=&client='.$params['client']->id) }}" class="client-filter-link u u-dotted">{{ $params['client']->name }}</a>
            </div>
        @endif

        <form id="sample-form" action="{{ ($sample && $params['is_sample_draft'] == false) ? '/sample/ajax-update/' . $sample->id : '/sample/ajax-store' }}" method="POST" enctype="multipart/form-data">

            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ $params['client']->id }}">

            @if ($sample && $params['is_sample_draft'] == false)
                <input type="hidden" name="id" value="{{ $sample->id }}">
            @endif

            <div class="order-positions-panel">
                @if (user_account_type(['manager', 'admin']))
                    <div>
                        <span class="panel-item-title">Статус:</span>
                        <select name="status" class="styled">
                            @foreach ($params['status_types'] as $key => $name)
                                <option value="{{ $key }}" {{ ($sample && $sample->status == $key) ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div>
                    <span class="panel-item-title">Покупатель:</span>
                    <select name="buyer_ext_id" class="styled" data-buyer-agreement-id="{{ ($sample) ? $sample->agreement_id : '' }}">
                        <option value="">Выбрать покупателя</option>
                        @foreach ($params['buyers'] as $buyer)
                            <option value="{{ $buyer->buyer_ext_id }}" {{ ($sample && $sample->buyer_ext_id == $buyer->buyer_ext_id) ? 'selected' : '' }} data-agreements="{{ $buyer->buyerAgreements->toJson() }}">{{ $buyer->buyer_title }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <span class="panel-item-title">Грузополучатель: </span>
                    <select name="consignee_id" class="styled">
                        <option value="">Выбрать грузополучателя</option>
                        @foreach ($params['consignees'] as $consignee)
                            <option value="{{ $consignee->id }}" {{ ($sample && $sample->consignee_id == $consignee->id) ? 'selected' : '' }}>{{ $consignee->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <span class="panel-item-title">Договор:</span>
                    <select name="agreement_id" class="styled">
                        <option value="">Выбрать договор</option>
                    </select>
                </div>
                <div>
                    <span class="panel-item-title">Тип поставки:</span>
                    <select name="delivery_type" class="styled">
                        @foreach ($params['delivery_types'] as $key => $name)
                            <option value="{{ $key }}" {{ ($sample && $sample->delivery_type == $key) ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="number-in">
                    <label class="input-container">
                        <input type="text" class="form-control {{ ($sample && $sample->request_number != '') ? 'not-empty' : '' }}" name="request_number" value="{{ $sample ? $sample->request_number : '' }}">
                        <span class="placeholder">Внутренний № образца</span>
                        <span class="help" data-toggle="tooltip" title="Введите внутренний номер образца">?</span>
                    </label>
                </div>
            </div>

            <div class="sample-form-title">
                <h2>Основная информация</h2>
            </div>
            
            <div class="claim-body sample-form-body">
                <div class="sample-form">
                    <div class="label">
                        <select name="nomenclature_type_id" class="styled">
                            <option value="">Вид изделия</option>
                            @foreach ($params['nomenclature_types'] as $nomenclatureType)
                                <option value="{{ $nomenclatureType->id }}" {{ ($sample && $sample->nomenclature_type_id == $nomenclatureType->id) ? 'selected' : '' }}>{{ $nomenclatureType->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group">
                        <label class="input-container">
                            <input type="number" class="form-control {{ ($sample && !empty($sample->sizes)) ? 'not-empty' : '' }}" name="sizes[length]" value="{{ ($sample && !empty($sample->sizes)) ? $sample->sizes['length'] : '' }}">
                            <span class="placeholder">Длина</span>
                        </label>
                        <label class="input-container">
                            <input type="number" class="form-control {{ ($sample && !empty($sample->sizes)) ? 'not-empty' : '' }}" name="sizes[width]" value="{{ ($sample && !empty($sample->sizes)) ? $sample->sizes['width'] : '' }}">
                            <span class="placeholder">Ширина</span>
                        </label>
                        <label class="input-container">
                            <input type="number" class="form-control {{ ($sample && !empty($sample->sizes)) ? 'not-empty' : '' }}" name="sizes[height]" value="{{ ($sample && !empty($sample->sizes) && isset($sample->sizes['height']) && !empty($sample->sizes['height'])) ? $sample->sizes['height'] : '' }}">
                            <span class="placeholder">Высота</span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label class="input-container">
                                <input type="number" class="form-control {{ ($sample) ? 'not-empty' : '' }}" name="cnt" value="{{ ($sample) ? $sample->cnt : '' }}">
                                <span class="placeholder">Требуемое кол-во, шт</span>
                            </label>
                        </div>
                        <div class="col-6">
                            <div class="label">
                                <select name="making" class="styled">
                                    <option value="">Изготовление</option>
                                    @foreach($params['making_types'] as $key => $name)
                                        <option value="{{ $key }}" {{ ($sample && $sample->making == $key) ? 'selected' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>		
                        </div>
                    </div>
                    <div class="label">
                        <select name="cardboard_type_id"class="styled">
                            <option value="">Марка картона</option>
                            @foreach ($params['cardboard_types'] as $cardboardType)
                                <option value="{{ $cardboardType->id }}" {{ ($sample && $sample->cardboard_type_id == $cardboardType->id) ? 'selected' : '' }}>{{ $cardboardType->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="label">
                        <select name="cardboard_profile_id" class="styled">
                            <option value="">Профиль</option>
                            @foreach ($params['cardboard_profiles'] as $cardboardProfile)
                                <option value="{{ $cardboardProfile->id }}" {{ ($sample && $sample->cardboard_profile_id == $cardboardProfile->id) ? 'selected' : '' }}>{{ $cardboardProfile->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="label">
                        <select name="cardboard_color_id" class="styled">
                            <option value="">Цвет</option>
                            @foreach ($params['cardboard_colors'] as $cardboardColor)
                                <option value="{{ $cardboardColor->id }}" {{ ($sample && $sample->cardboard_color_id == $cardboardColor->id) ? 'selected' : '' }}>{{ $cardboardColor->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <h2>Дополнительная информация</h2>
            
            <div class="oder-add-info">
                <div class="oder-add-comment">
                    <label class="input-container">
                        <textarea name="comment" cols="30" class="form-control {{ ($sample && $sample->comment != '') ? 'not-empty' : '' }}">{{ $sample ? $sample->comment : '' }}</textarea>
                        <span class="placeholder">Комментарий к заявке</span>
                    </label>
                </div>
                <div class="add-file">
                    <input type="file" name="file" accept=".jpg, .jpeg, .png, .tiff, .bmp, .doc, .docx, .xls, .xlsx, .pdf, .zip, .rar">
                    <span class="order-add-file">
                        <span class="add-file-name" data-text="Прикрепить файл с чертежом">
                            @if ($sample && $params['is_sample_draft'] == false && $sample->file)
                                <span>
                                    <a href="{{ url('shared/read-file/' . $sample->file->hash) }}" target="_blank">{{ $sample->file->name }}</a>
                                    <span class="remove-file" data-hash="{{ $sample->file->hash }}"></span>
                                </span>
                            @else
                                Прикрепить файл с чертежом
                            @endif
                        </span>
                        @if ($params['view_mode'] == false)
                            <a href="#" class="add-file-link u u-dotted">Загрузить файл</a>
                        @else
                            <span class="add-file-link u u-dotted disabled">Загрузить файл</span>
                        @endif
                    </span>
                </div>
            </div>

            <div class="errors"></div>

            @if ($params['view_mode'] == false)
                <div class="text-center">
                    <a href="#" id="js-save-sample" class="button button-lg btn-submit">Сохранить</a>
                </div>
            @endif

        </form>

    </div>

    {{-- Чат --}}

    @if ($sample && $sample->ext_id != null && $params['is_sample_draft'] == false)
        
        @include('_chat', [
            'h2_title' => 'Сообщения по образцу',
            'chat_object_type' => 'sample',
            'chat_object' => $sample,
            'chat_items' => $sample->chat
        ])

    @endif

    {{-- js templates --}}

    @include('sample.inc._js_form')

</div>

@endsection